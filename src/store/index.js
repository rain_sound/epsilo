import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import { rootReducer } from '../reducers'
import { loadState } from './persist'

const initialize = {
    // mPage: { foro_mp: "ALL_ENTRIES", data: null },
    // user: {
    //     userId: '-1',
    //     auth: 'false'
    // },
    posts: 'empty',
}
let state = loadState()
state = (state === undefined) ? initialize : state

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export const store = createStore(rootReducer, state, composeEnhancers(applyMiddleware(thunk)))