import { alertConstants } from '../constants/alerts/actions'

/* Create Alert */

const initialize = {
    toast: []
}

export const alert = (state = initialize , action ) => {
    switch (action.type) {
        case alertConstants.ADD_TOAST: {
            const { payload } = action;
            return {  toast: [ ...state.toast, payload ] }
        }
        case alertConstants.REMOVE_TOAST: {
            const { payload } = action;
            return { toast: state.toast.filter(item => item.id !== payload )}
        }
        case alertConstants.CLEAR_ALL_TOAST: {
            return { toast: [] }
        }
        default:
            return  { ...state }  
    }
}