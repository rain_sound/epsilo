import { clientConstants } from "../constants/client/actions";
import update from 'immutability-helper'

const initialize = {
  list: []
}

export const client = (state = initialize, action) => {
  switch (action.type) {
    case clientConstants.GET_ALL_CLIENTS_REQUEST: {
      return { loading: true };
    }
    case clientConstants.GET_ALL_CLIENTS_FAILED: {
      return { loading: false };
    }
    case clientConstants.GET_ALL_CLIENTS_SUCCESS: {
      const { data } = action.payload
      return { list: data };
    }
    case clientConstants.UPDATE_CLIENT_REQUEST: {
      return { ...state, updating: true };
    }
    case clientConstants.UPDATE_CLIENT_FAILED: {
      const { data } = action.payload
      return { ...state, updating: false, error: data };
    }
    case clientConstants.UPDATE_CLIENT_SUCCESS: {
      const { data } = action.payload
      const index =   [...state.list].findIndex(item => item.id === data.id)
      return { list: update(state.list, {
        [index]: {
          c_name:     { $set: data.c_name },
          c_lastname: { $set: data.c_lastname },
          c_email:    { $set: data.c_email }
        }
        })
      }
    }
    default:
      return state;
  }
};
