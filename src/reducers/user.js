import { userConstants } from "../constants/user/actions";


export const user = (state = {}, action) => {
  switch (action.type) {
    case userConstants.LOGGING: {
      const { name } = action.payload;
      return { ...state, name };
    }
    case userConstants.FAILURE: {
      const { reason } = action.payload;
      return { reason };
    }
    case userConstants.LOGED: {
      const { auth, id, email, imgProfile } = action.payload;
      return { ...state, auth, id, email, profile: { filename: imgProfile } };
    }
    case userConstants.LOGOUT: {
      return { };
    }
    case userConstants.UPDATE_IMAGE_REQUEST: {
      return { ...state, profile: { ...state.profile, loading: false } };
    }
    case userConstants.UPDATE_IMAGE_SUCCESS: {
      const { filename, message } = action.payload;
      return { ...state, profile: { completed: true, filename, message, reason: null } };
    }
    case userConstants.UPDATE_IMAGE_FAILURE: {
      const { reason } = action.payload;
      return { ...state, profile: { ...state.profile, loading: false, message: null, reason } };
    }
    case userConstants.CLEAR_IMAGE: {
      return { ...state, profile: { ...state.profile, message: null, reason: null } }
    }
    default:
      return state;
  }
};

export const getUserId = state => state.id
export const getUsername = state => state.name