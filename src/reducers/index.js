import { combineReducers } from 'redux'
import { user, getUserId as _getUserId, getUsername as _getUsename } from './user'
import { alert } from './alert'
import { client } from './client'
import { userConstants } from '../constants/user/actions'

 const appReducer = combineReducers({
    user,
    alert,
    client
})

export const rootReducer = (state, action) => {
    if (action.type === userConstants.LOGOUT) {
        state = undefined
      }
    
      return appReducer(state, action)
  }
//Selectors
export const getUserId = state => _getUserId(state.user)
export const getUsername = state => _getUsename(state.user)