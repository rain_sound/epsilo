import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import CardInfo from "../../../components/Other/CardInfo";
import { userService } from "../../../services/userServices";

export class PasswordRecovery extends Component {
  constructor() {
    super();
    this.state = {
      submited: false,
      buttonDisabled: false,
      newPassword: "",
      oldPassword: "",
      confirmPassword: ""
    };
  }

  handleChange = e => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  matchSubmit = () => {
    const { newPassword, oldPassword, confirmPassword } = this.state;
    return (
      newPassword &&
      oldPassword &&
      confirmPassword &&
      newPassword === confirmPassword
    );
  };

  handleSubmitUpdatePassword = e => {
    e.preventDefault();
    this.setState({ submited: true });
    const { newPassword, oldPassword } = this.state;
    const { name } = this.props.match.params;
    if (this.matchSubmit()) {
      this.setState({buttonDisabled: true })
      userService.updatePassword(name, newPassword, oldPassword).then(res => this.setState({ buttonDisabled: false }));
    }
  };

  renderUpdatePassword = () => {
    const { submited, newPassword, oldPassword, confirmPassword, buttonDisabled } = this.state;
    return (
      <div className="col-xs-12">
        <div className="card">
          <div className="card-header">
            <strong>Configuración Perfil</strong> Usuario
          </div>
          <div className="card-body">
            <form className="form-horizontal" action="" method="post">
              <div className="form-group row">
                <div className="col-md-12">
                  <div className="input-group">
                    <input
                      className="form-control"
                      onChange={this.handleChange}
                      type="password"
                      name="oldPassword"
                      id="oldPassword"
                      placeholder="Ingrese su contraseña actual"
                    />
                  </div>
                  {submited && !oldPassword && (
                    <small className=" badge badge-danger">
                      Debe ingresar una contrasenia{" "}
                    </small>
                  )}
                </div>
              </div>
              <div className="form-group row">
                <div className="col-md-12">
                  <div className="input-group">
                    <input
                      className="form-control"
                      onChange={this.handleChange}
                      type="password"
                      name="newPassword"
                      id="newPassword"
                      placeholder="Ingrese una nueva contraseña"
                    />
                  </div>
                  {submited && !newPassword && (
                    <small className="badge badge-danger">
                      Debe ingresar una nueva contrasenia{" "}
                    </small>
                  )}
                </div>
              </div>
              <div className="form-group row">
                <div className="col-md-12">
                  <div className="input-group">
                    <input
                      className="form-control"
                      onChange={this.handleChange}
                      type="password"
                      name="confirmPassword"
                      id="confirmPassword"
                      placeholder="Repita la contraseña"
                    />
                  </div>
                  {newPassword !== confirmPassword && (
                    <small className="badge badge-danger">
                      Las contrasenias deben ser iguales{" "}
                    </small>
                  )}
                  {submited && !oldPassword && !this.matchSubmit() && (
                    <small className="badge badge-danger">
                      Debe rellenar todos los campos{" "}
                    </small>
                  )}
                </div>
              </div>
            </form>
          </div>
          <div className="card-footer">
            <button
            disabled = { buttonDisabled }
              onClick={this.handleSubmitUpdatePassword}
              className="btn btn-sm btn-success"
              type="submit"
            >
              <i className="fa fa-check" /> Enviar
            </button>
            <button className="btn btn-sm btn-danger" type="reset">
              <i className="fa fa-ban" /> Reset
            </button>
          </div>
        </div>
      </div>
    );
  };

  render() {
    return (
      <div className="row">
        <CardInfo
          body={this.renderUpdatePassword()}
          title="Mantenimiento de Contraseña"
          width="10"
        />
      </div>
    );
  }
}

const mapStateToProps = ({ user }) => ({ user });

PasswordRecovery.prototypes = {
  user: PropTypes.shape({
    name: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    imgProfile: PropTypes.string.isRequired,
    reason: PropTypes.string,
    message: PropTypes.string
  })
};

export default connect(
  mapStateToProps,
  null
)(PasswordRecovery);
