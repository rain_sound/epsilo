import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import CardInfo from "../../../components/Other/CardInfo";
import CardImage from "../../../components/Other/CardImage";

export class Profile extends Component {
  constructor() {
    super();
    this.state = {
      submited: false,
      newPassword: "",
      oldPassword: "",
      confirmPassword: ""
    };
  }

  handleChange = e => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };


  renderProfile = () => {
    const { submited, username } = this.state;
    return (
      <div className="col-xs-12">
        <div className="card">
          <div className="card-header">
            <strong>Configuración Perfil</strong> Usuario
          </div>
          <div className="row mt-2">
            <div className="col-12">
              <div className="mt-3 card-body">
                <CardImage />
              </div>
            </div>
            <div className="col-12">
              <div className="mt-3 ml-4 card-body">
                <form className="form-horizontal" action="" method="post">
                  <div className="form-group">
                    <div className="input-group">
                      <div className="input-group-prepend">
                        <span className="input-group-text">Código</span>
                      </div>
                      <input
                        type="text"
                        name="id"
                        id="id"
                        disabled
                        className="form-control"
                        value={this.props.user.id}
                      />
                     
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="input-group">
                      <div className="input-group-prepend">
                        <span className="input-group-text">Usuario</span>
                      </div>
                      <input
                        type="text"
                        name="username"
                        id="username"
                        onChange={this.handleChange}
                        className="form-control"
                        value={this.props.user.name}
                      />
                      
                      {submited && !username && (
                        <small className=" badge badge-danger">
                          Debe ingresar una contrasenia{" "}
                        </small>
                      )}
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="input-group">
                      <div className="input-group-prepend">
                        <span className="input-group-text">Correo</span>
                      </div>
                      <input
                        type="email"
                        name="email"
                        id="email"
                        className="form-control"
                      />
                    
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="input-group">
                      <div className="input-group-prepend">
                        <span className="input-group-text">Nacimiento</span>
                      </div>
                      <input
                        type="date"
                        name="username"
                        id="username"
                        onChange={this.handleChange}
                        className="form-control"
                        value={this.props.user.name}
                      />
                      
                      {submited && !username && (
                        <small className=" badge badge-danger">
                          Debe ingresar una contrasenia{" "}
                        </small>
                      )}
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="input-group">
                      <div className="input-group-prepend">
                        <span className="input-group-text">Dirección</span>
                      </div>
                      <input
                        type="text"
                        name="username"
                        id="username"
                        onChange={this.handleChange}
                        className="form-control"
                        value={this.props.user.name}
                      />
                     
                      {submited && !username && (
                        <small className=" badge badge-danger">
                          Debe ingresar una contrasenia{" "}
                        </small>
                      )}
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="input-group">
                      <div className="input-group-prepend">
                        <span className="input-group-text">Celular</span>
                      </div>
                      <input
                        type="text"
                        name="username"
                        id="username"
                        onChange={this.handleChange}
                        className="form-control"
                        value={this.props.user.name}
                      />
                      
                      {submited && !username && (
                        <small className=" badge badge-danger">
                          Debe ingresar una contrasenia{" "}
                        </small>
                      )}
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div className="card-footer">
                <button
                  className="btn btn-sm btn-success"
                  type="submit"
                >
                  <i className="fa fa-check" /> Actualizar
                </button>
                <button className="btn btn-sm btn-danger" type="reset">
                  <i className="fa fa-ban" /> Reset
                </button>
              </div>
        </div>
      </div>
    );
  };
  render() {
    return (
      <div className="row">
        <CardInfo
          body={this.renderProfile()}
          title="Configuración de Perfil"
          width="10"
        />
      </div>
    );
  }
}

const mapStateToProps = ({ user }) => ({ user });

Profile.prototypes = {
  user: PropTypes.shape({
    name: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    imgProfile: PropTypes.string.isRequired,
    reason: PropTypes.string,
    message: PropTypes.string
  })
};

export default connect(mapStateToProps)(Profile);
