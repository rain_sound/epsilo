import React, { Component } from "react";
import "./RegisterService.css";
import { Input } from 'reactstrap'

import logo_otass from '../../../assets/img/logo_otass.png'

import { serviceServices } from '../../../services/serviceServices'
import { clientServices } from '../../../services/clientServices'
import { validation } from '../../../helpers/validation';

class RegisterService extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      name: "",
      address: "",
      lastname: "",
      meter_code: "",
      meter_catas: "",
      selectMulti: [],
      category: "",
      cod_catas: "",
      activity: "",
      lecture_now_date: "",
      lecture_now_value: "",
      meters: [],
      services: [],
      submited: false,
      buttonDisabled: false
    };
  }

  componentDidMount = () => {
    if(this.state.services.length === 0){
      serviceServices.getAll()
        .then(services => {
          this.setState({ services: services.data })
        } )
        
    }
  }

  handleChange = e => {
    const { name, value } = e.target;
    this.setState({
      [name]: value
    });
  };

  handleBlur = () => {
    const { id } = this.state
    if(id){
      clientServices.getClientBill(id)
        .then(({ data: { name, id, lastname, meters }}) => {
          const meter = meters[0]
          const { category } = meter
          this.setState({
            name,
            id,
            lastname,
            meters,
            meter_code: meter.id, 
            meter_catas: meter.m_catastro_code,
            category: category.ca_description,
            activity: category.ca_activity
          })
        })
        
    }
  }

  handleClick = e => {
    e.preventDefault()
    this.setState({ submited: true})
    const { lecture_now_value, lecture_now_date, meter_code, selectMulti } = this.state
    
    if( !validation.isBlank({ lecture_now_value, lecture_now_date, meter_code, selectMulti })){
      this.setState({ buttonDisabled: true })
      serviceServices.registerBill(lecture_now_value, lecture_now_date,meter_code, selectMulti ).then(res => this.setState({ buttonDisabled: false }))
    }
  }


  handleMultiple = e => {
    var options = e.target.options
    var value = []
    for (var i = 0; i < options.length; i++){
      if(options[i].selected){
        value.push(options[i].value)
      }
    }
    this.setState({ selectMulti: value })
  }

  handleChangeAddress = e => {
    const { meters } = this.state
    const meter = meters[e.target.value]
    const { category } = meter
    this.setState({ 
      meter_code: meter.id, 
      meter_catas: meter.m_catastro_code,
      category: category.ca_description,
      activity: category.ca_activity
     })
  }

  render() {
    const { services, name, lecture_now_date, lecture_now_value, lastname, id, selectMulti,  meters, meter_code, meter_catas, category, activity,buttonDisabled , submited } = this.state
    return (
      <div className="container">
        <div className="card ">
          <div className="card-header card__background">
            <strong>Configuración Perfil</strong> Usuario
          </div>
          <div className="card-body">
            <form className="form-horizontal" action="" method="post">
              <div className="row">
                <div className="col-6">
                  <div className="form-group">
                    <div className="input-group">
                      <input
                        type="text"
                        name="id"
                        id="id"
                        placeholder="Ingrese el código de usuario"
                        onChange={this.handleChange}
                        onBlur={this.handleBlur}
                        className="form-control"
                      />
                    </div>
                    {submited && !id && (
                      <small className=" badge badge-danger">
                        Debe ingresar un DNI{" "}
                      </small>
                    )}
                  </div>
                  <div className="form-group">
                    <div className="input-group">
                      <div className="input-group-prepend">
                        <span className="input-group-text">Nombre:</span>
                      </div>
                      <input
                        type="text"
                        readOnly
                        value={`${lastname} ${name}`}
                        className="form-control"
                      />
                    </div>
                  </div>
                  <div className="form-group">
                    <label htmlFor="caddress">Dirección:</label>
                    <select
                      onChange={this.handleChangeAddress}
                      className="form-control"
                      id="caddress"
                    >
                      {meters &&
                        meters.map((item, index) => (
                          <option value={index} key={item.id}>
                            {item.m_address}
                          </option>
                        ))}
                    </select>
                  </div>
                  <div className="form-group">
                    <div className="input-group">
                      <div className="input-group-prepend">
                        <span className="input-group-text">RUC/DNI:</span>
                      </div>
                      <input
                        type="text"
                        name="dni"
                        id="dni"
                        readOnly
                        value={id}
                        className="form-control"
                      />
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="input-group">
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          COD. CATAS:
                        </span>
                      </div>
                      <input
                        type="text"
                        name="cod_catas"
                        id="cod_catas"
                        readOnly
                        value={meter_catas}
                        className="form-control"
                      />
                    </div>
                  </div>
                </div>
                <div className="col-6 text-center d-flex justify-content-center">
                  <div className="align-self-center font-weight-bold">
                    <img className="img-fluid" src={logo_otass} alt="" />
                    <p className="mt-2 mb-1 strong">RUC 20115851919</p>
                    <p className="mb-1">PJ. MIRAMAR MZ C.S/N PARTE PRIMA</p>
                    <p className="mb-1">ILO - ILO - MOQUEGUA </p>
                  </div>
                </div>
              </div>
              <div className="row font-weight-bold">
                <div className="col-3">
                  <p>Ruta: 2</p>
                  <p>481309 - 481075</p>
                </div>
                <div className="col-3">
                  <p>Secu: 47</p>
                  <p>EPSILO SA</p>
                </div>
                <div className="col-3">
                  <p>Ciclo: 001</p>
                  <p>956326866</p>
                </div>
                <div className="col-3">
                  <p>22085</p>
                  <p>www.epsilo.com.pe</p>
                </div>
              </div>
              <div className="row mb-3">
                <div className="col-6 header__success">
                  DATOS FACTURACIÓN
                </div>
                <div className="col-6 header__success">
                  CONCEPTOS FACTURADOS
                </div>
              </div>
              <div className="row">
                <div className="col-6">
                  <div className="form-group">
                    <div className="input-group">
                      <div className="input-group-prepend">
                        <span className="input-group-text ">Categoria</span>
                      </div>
                      <input
                        type="text"
                        name="category"
                        id="category"
                        value={category}
                        readOnly
                        className="form-control"
                      />
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="input-group">
                      <div className="input-group-prepend">
                        <span className="input-group-text">Actividad</span>
                      </div>
                      <input
                        type="text"
                        name="activity"
                        id="activity"
                        value={activity}
                        readOnly
                        className="form-control"
                      />
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="input-group">
                      <div className="input-group-prepend">
                        <span className="input-group-text">Medidor</span>
                      </div>
                      <input
                        type="text"
                        name="meter_code"
                        id="meter_code"
                        readOnly
                        value={meter_code}
                        className="form-control"
                      />
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="input-group">
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          Modalidad de facturación
                        </span>
                      </div>
                      <input
                        type="text"
                        name="dni"
                        id="dni"
                        readOnly
                        value="MEDIDO"
                        className="form-control"
                      />
                    </div>
                  </div>
                </div>
                <div className="col-6">
                  <div className="form-group row">
                    <label
                      className="col-md-3 col-form-label"
                      htmlFor="multiple-select"
                    >
                      Servicios prestados
                    </label>
                    <div className="col-md-9">
                      <Input
                        type="select"
                        name="selectMulti"
                        id="selectMulti"
                        onChange={this.handleMultiple}
                        multiple
                      >
                        {services.length !== 0 &&
                          services.map(item => (
                            <option
                              key={item.id}
                              value={item.id}
                              data={item.id}
                            >
                              {item.s_description}
                            </option>
                          ))}
                      </Input>
                      {submited && selectMulti.length === 0 && (
                        <small className=" badge badge-danger">
                          Debe seleccionar al menos un servicio{" "}
                        </small>
                      )}
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="input-group">
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          Lectura Actual Fecha
                        </span>
                      </div>
                      <input
                        type="date"
                        name="lecture_now_date"
                        id="lecture_now_date"
                        onChange={this.handleChange}
                        className="form-control"
                      />
                    </div>
                    {submited && !lecture_now_date && (
                      <small className=" badge badge-danger">
                        Debe Ingresar la fecha del recibo{" "}
                      </small>
                    )}
                  </div>

                  <div className="form-group">
                    <div className="input-group">
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          Lectura Actual Valor
                        </span>
                      </div>
                      <input
                        type="text"
                        name="lecture_now_value"
                        id="lecture_now_value"
                        onChange={this.handleChange}
                        className="form-control"
                      />
                    </div>
                    {submited && !lecture_now_value && (
                      <small className=" badge badge-danger">
                        Debe ser una lectura{" "}
                      </small>
                    )}
                    {submited &&
                      !validation.isNumber(lecture_now_value) && (
                        <small className=" badge badge-danger">
                          Debe ser un número{" "}
                        </small>
                      )}
                  </div>
                </div>
              </div>
              <div className="card-footer">
                <button
                  disabled={buttonDisabled}
                  onClick={this.handleClick}
                  className="btn btn-sm btn-success"
                  type="submit"
                >
                  <i className="fa fa-check" /> Guardar
                </button>
                <button className="btn btn-sm btn-danger" type="reset">
                  <i className="fa fa-ban" /> Cancelar
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default RegisterService;
