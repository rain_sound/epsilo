import React, { Component } from "react";
import {
  Button,
  Card,
  CardBody,
  Col,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Row
} from "reactstrap";
import { connect } from 'react-redux'

import "react-tabulator/lib/css/tabulator.min.css"; // theme
import "react-tabulator/css/bootstrap/tabulator_bootstrap.min.css"; // use Theme(s)
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import "react-bootstrap-table2-filter/dist/react-bootstrap-table2-filter.min.css";
import "react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit.min.css";
import ToolkitProvider, {  CSVExport,  Search } from "react-bootstrap-table2-toolkit";
import BootstrapTable from "react-bootstrap-table-next";

// for React 16.4.x use: import { ReactTabulator }

import { ButtonRow } from "../../../components/Other/Table";

import { clientActions } from '../../../actions/client'
import { validation } from '../../../helpers/validation';

class Employee extends Component {
  constructor() {
    super();
    this.UpdateButton = this.UpdateButton.bind(this);
    this.state = {
      modalUpdate: false,
      modalDelete: false,
      id: null,
      c_name: null,
      c_lastname: null,
      c_email: null,
      submited: false,
      buttonDisabled: false,
      columns: [
        {
          dataField: "id",
          text: "Codigo",
          sort: true,
          onSort: (field, order) => {
            console.log("field", field, "order", order);
          }
        },
        {
          dataField: "c_name",
          text: "Nombre",
          sort: true
        },
        {
          dataField: "c_lastname",
          text: "Apellido",
          sort: true
        },
        {
          dataField: "c_email",
          text: "Email",
          sort: true
        },
        {
          dataField: "actionsUpdate",
          text: "Actions",
          csvExport: false,
          isDummyField: false,
          formatter: this.UpdateButton,
          style: { textAlign: "center" }
        }
      ]
    };
  }

  componentDidMount = () => {
    const { getAll, list } = this.props 
    if(!list || list.length === 0) getAll()
  }

  handleChange = e => {
    const { name, value } = e.target;
    this.setState({
      [name]: value
    });
  };

  //IMPORTANT:
  // Modal Methods
  toggleUpdate = ({ c_name = "", c_lastname = "", c_email = "", id = "" }) => {
    this.setState({
      modalUpdate: !this.state.modalUpdate,
      c_name,
      c_lastname,
      c_email,
      id
    });
  };

  toggleDelete = ({ code }) => {
    this.setState({ modalDelete: !this.state.modalDelete, code });
  };
  //Modal Methods
  //IMPORTANT:

  //IMPORTANT:
  // Update row after update click button
  // Update server 
  // If update server was successful update view
  updateDataTable = e => {
    e.preventDefault();
    const { id, c_name, c_lastname, c_email } = this.state;
    const { updateData } = this.props
    this.setState({ submited: true })
    if(!validation.isBlank({ id, c_name, c_lastname, c_email})){
      updateData(id, c_name, c_lastname, c_email)
      this.setState({ modalUpdate: false })
    }
   
  };
  //IMPORTANT:
  //BUTTON COLUMN TABLE
  UpdateButton(cell, row) {
    return (
      <ButtonRow
        title="Actualizar"
        toggle={this.toggleUpdate}
        row={row}
        width={"w-75"}
        type="success"
      />
    );
  }

 

  render() {
    const { id, c_name, c_lastname, c_email, submited } = this.state;
    const { list } = this.props
    const { ExportCSVButton } = CSVExport;
    const { SearchBar, ClearSearchButton } = Search;
    return (
      <div className="row">
        <div className="col-auto">
          {console.log("list of clietns", list)}
          {list && (
            <ToolkitProvider
              keyField={"id"}
              classes="card table-responsive"
              data={list}
              columns={this.state.columns}
              search
              loading
              exportCSV={{
                onlyExportFiltered: true,
                ignoreHeader: true,
                exportAll: false
              }}
            >
              {props => (
                <div className="card">
                  <div className="card-header">
                    <div className="row">
                      <div className="col-3">
                        <SearchBar tableId="id" {...props.searchProps} />
                      </div>
                      <div className="col-auto">
                        <ClearSearchButton
                          className="btn btn-secondary"
                          {...props.searchProps}
                        />
                      </div>
                      <div className="col-auto">
                        <ExportCSVButton
                          className="btn btn-success"
                          {...props.csvProps}
                        >
                          Export CSV
                        </ExportCSVButton>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12">
                      <BootstrapTable
                        hover
                        striped
                        {...props.baseProps}
                        rowEvents={this.rowEvents}
                      />
                    </div>
                  </div>
                </div>
              )}
            </ToolkitProvider>
          )}
        </div>

        {/* Modal Delete */}
        <Modal isOpen={this.state.modalDelete} toggle={this.toggleDelete}>
          <ModalHeader toggle={this.toggle}>Modal Delete</ModalHeader>
          <ModalBody>Este es un modal delete</ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.toggleDelete}>
              Do Something
            </Button>
            <Button color="secondary" onClick={this.toggleDelete}>
              Cancel
            </Button>
          </ModalFooter>
        </Modal>

        {/* Modal Update */}
        <Modal isOpen={this.state.modalUpdate} toggle={this.modalUpdate}>
          <ModalHeader className="bg-info color-white" toggle={this.toggle}>
            Actualizar Informacion
          </ModalHeader>
          <ModalBody>
            <Row>
              <Col xs="12">
                <Card>
                  <CardBody>
                    <Form action="" method="post">
                      <FormGroup>
                        <InputGroup>
                          <InputGroupAddon
                            addonType="prepend"
                            className="w-25"
                          >
                            <InputGroupText className="w-100">
                              Codigo
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="text"
                            onChange={this.handleChange}
                            name="id"
                            id="id"
                            disabled
                            value={id}
                          />
                        </InputGroup>
                      </FormGroup>
                      <FormGroup>
                        <InputGroup>
                          <InputGroupAddon
                            addonType="prepend"
                            className="w-25"
                          >
                            <InputGroupText className="w-100">
                              Nombre
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="text"
                            onChange={this.handleChange}
                            name="c_name"
                            id="c_name"
                            value={c_name}
                          />
                          
                        </InputGroup>
                        {submited && !c_name && (
                            <small className=" badge badge-danger">
                              Debe ingresar un nombre{" "}
                            </small>
                          )}
                      </FormGroup>
                      <FormGroup>
                        <InputGroup>
                          <InputGroupAddon
                            addonType="prepend"
                            className="w-25"
                          >
                            <InputGroupText className="w-100">
                              Apellido
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="text"
                            onChange={this.handleChange}
                            name="c_lastname"
                            id="c_lastname"
                            value={c_lastname}
                          />
                        
                        </InputGroup>
                        {submited && !c_lastname && (
                            <small className=" badge badge-danger">
                              Debe ingresar un apellido{" "}
                            </small>
                          )}
                      </FormGroup>
                      <FormGroup>
                        <InputGroup>
                          <InputGroupAddon
                            addonType="prepend"
                            className="w-25"
                          >
                            <InputGroupText className="w-100">
                              Correo
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="text"
                            onChange={this.handleChange}
                            name="c_email"
                            id="c_email"
                            value={c_email}
                          />
                         
                        </InputGroup>
                        {submited && !c_email && (
                            <small className=" badge badge-danger">
                              Debe ingresar un correo{" "}
                            </small>
                          )}
                      </FormGroup>
                      <FormGroup className="form-actions text-center">
                        <Button
                          type="submit"
                          color="primary"
                          onClick={this.updateDataTable}
                        >
                          <i className="fa fa-dot-circle-o" /> Actualizar
                        </Button>
                        <Button
                          type="reset"
                          color="danger"
                          onClick={this.toggleUpdate}
                        >
                          <i className="fa fa-ban" /> Limpiar
                        </Button>
                      </FormGroup>
                    </Form>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = ({client: { list }}) => ({ list })

const mapDispatchToProps = dispatch => ({
  getAll: () => dispatch(clientActions.getAll()),
  updateData: ( id, name, lastname, email ) => dispatch(clientActions.updateData(id, name, lastname, email))
})
export default connect(mapStateToProps, mapDispatchToProps)(Employee);
