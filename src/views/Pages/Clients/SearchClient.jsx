import React, { Component } from "react";
import {
  Button,
  Card,
  CardBody,
  Col,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Modal,
  ModalHeader,
  ModalBody,
  Label,
  Row
} from "reactstrap";
import update from 'immutability-helper'

import { clientServices } from "../../../services/clientServices";
import { serviceServices } from "../../../services/serviceServices";
import { validation } from '../../../helpers/validation';

class SearchClient extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dni: "",
      name: "",
      modalUpdate: false,
      lastname: "",
      cellphone: "",
      email: "",
      daybirth: "",
      address: "",
      addressM: "",
      categoryM: 1,
      modelM: 1,
      meter_serieM: "",
      meter_cod_catasM: "",
      meter_modelM: "",
      meter_routeM: "",
      categories: [],
      models: [],
      submited: true,
      submitedAdd: false,
      buttonDisabled: false
    };
  }

  componentDidMount = () => {
    serviceServices.getAllCategories().then(res => {
      this.setState({ categories: res.data });
    });
    serviceServices
      .getAllModels()
      .then(res => this.setState({ models: res.data }));
  };

  handleChange = e => {
    const { value, name } = e.target;
    this.setState({
      [name]: value
    });
  };

  clearText = () => {
    this.setState(prevState =>
      update(prevState, {
        name: { $set: "" },
        lastname: { $set: "" },
        cellphone: { $set: "" },
        address: { $set: "" },
        email: { $set: "" },
        daybirth: { $set: "" }
      })
    );
  };

 

  handleSubmit = e => {
    e.preventDefault();
    const {
      dni,
      addressM,
      meter_cod_catasM,
      meter_routeM,
      meter_serieM,
      categoryM,
      modelM
    } = this.state;
    
    this.setState({ submitedAdd: true })
    if (!validation.isBlank({ dni, addressM, meter_cod_catasM, meter_routeM, meter_serieM, categoryM, modelM }) ) {
      this.setState({ buttonDisabled: true })
      serviceServices.createMeter(
        dni,
        categoryM,
        meter_serieM,
        meter_routeM,
        addressM,
        meter_cod_catasM,
        modelM
      ).then(res => this.setState({ buttonDisabled: false }))
    }
  };

  handleBlur = () => {
    this.clearText();
    const { dni } = this.state;
    if (dni !== "") {
      clientServices.getClientById(dni).then(res => {
        const {
          name,
          lastname,
          cellphone,
          address,
          email,
          daybirth
        } = res.data;

        this.setState({
          name,
          lastname,
          cellphone,
          address,
          email,
          daybirth
        });
        if (name && name !== "") this.setState({ submited: false });
      });
    }
  };

  render() {
    const {
      submited,
      dni,
      name,
      addressM,
      lastname,
      cellphone,
      address,
      email,
      categories,
      daybirth,
      models,
      buttonDisabled,
      meter_serieM,
      meter_cod_catasM,
      meter_routeM,
      submitedAdd
    } = this.state;
    return (
      <div className="col-xs-12">
        <div className="card">
          <div className="card-header">
            <strong>Buscar</strong> Cliente
            {console.log("estado inicial", this.state)}
          </div>
          <div className="row mt-2">
            <div className="col-12">
              <div className="mt-3 ml-4 card-body">
                <div className="form-group">
                  <div className="input-group">
                    <div className="input-group-prepend">
                      <span className="input-group-text">Código</span>
                    </div>
                    <input
                      type="text"
                      name="dni"
                      id="dni"
                      className="form-control"
                      onChange={this.handleChange}
                      onBlur={this.handleBlur}
                    />
                  </div>
                  {submited && !dni && (
                    <small className=" badge badge-danger">
                      Debe ingresar un código{" "}
                    </small>
                  )}
                </div>
                <div className="form-group">
                  <div className="input-group">
                    <div className="input-group-prepend">
                      <span className="input-group-text">Cliente</span>
                    </div>
                    <input
                      type="text"
                      name="name"
                      id="name"
                      disabled
                      value={`${name} ${lastname}`}
                      className="form-control"
                    />
                  </div>
                </div>
                <div className="form-group">
                  <div className="input-group">
                    <div className="input-group-prepend">
                      <span className="input-group-text">Correo</span>
                    </div>
                    <input
                      type="email"
                      name="email"
                      id="email"
                      disabled
                      value={email}
                      className="form-control"
                    />
                  </div>
                </div>
                <div className="form-group">
                  <div className="input-group">
                    <div className="input-group-prepend">
                      <span className="input-group-text">Nacimiento</span>
                    </div>
                    <input
                      type="date"
                      name="daybirth"
                      id="daybirth"
                      disabled
                      value={daybirth || ""}
                      className="form-control"
                    />
                  </div>
                </div>
                <div className="form-group">
                  <div className="input-group">
                    <div className="input-group-prepend">
                      <span className="input-group-text">Dirección</span>
                    </div>
                    <input
                      type="text"
                      name="address"
                      id="address"
                      disabled
                      value={address}
                      className="form-control"
                    />
                  </div>
                </div>
                <div className="form-group">
                  <div className="input-group">
                    <div className="input-group-prepend">
                      <span className="input-group-text">Celular</span>
                    </div>
                    <input
                      type="text"
                      name="cellphone"
                      id="cellphone"
                      disabled
                      value={cellphone}
                      className="form-control"
                    />
                  </div>
                </div>
              </div>
              <div className="card-footer">
                <button
                  onClick={() => this.setState({ modalUpdate: true })}
                  className="btn btn-sm btn-success"
                  disabled={submited}
                  type="submit"
                >
                  <i className="fa fa-address-book" /> Añadir Dirección
                </button>
              </div>
            </div>
          </div>
        </div>
        {/* Modal Update */}
        <Modal isOpen={this.state.modalUpdate} toggle={this.modalUpdate}>
          <ModalHeader className="bg-info color-white" toggle={this.toggle}>
            Añadir Dirección
          </ModalHeader>
          <ModalBody>
            <Row>
              <Col xs="12">
                <Card>
                  <CardBody>
                    <Form action="" method="post">
                      <FormGroup>
                        <InputGroup>
                          <InputGroupAddon
                            addonType="prepend"
                            className="w-25"
                          >
                            <InputGroupText className="w-100">
                              Dirección
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            id="addressM"
                            name="addressM"
                            type="text"
                            onChange={this.handleChange}
                          />
                        </InputGroup>
                        {submitedAdd && !addressM && (
                          <small className=" badge badge-danger">
                            Debe ingresar una dirección{" "}
                          </small>
                        )}
                      </FormGroup>
                      <FormGroup>
                        <Label for="categoryM">Categoría</Label>
                        <Input
                          type="select"
                          name="categoryM"
                          id="categoryM"
                          onChange={this.handleChange}
                        >
                          {categories.length !== 0 &&
                            categories.map(item => (
                              <option value={item.id} key={item.id}>
                                {item.ca_description}
                              </option>
                            ))}
                        </Input>
                      </FormGroup>

                      <span className="text-muted mb-2">Medidor</span>
                      <FormGroup>
                        <InputGroup>
                          <InputGroupAddon
                            addonType="prepend"
                            className="w-25"
                          >
                            <InputGroupText className="w-100">
                              Serie
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            id="meter_serieM"
                            name="meter_serieM"
                            type="text"
                            onChange={this.handleChange}
                          />
                        </InputGroup>
                        {submitedAdd && !meter_serieM && (
                          <small className=" badge badge-danger">
                            Debe ingresar un codigo de serie{" "}
                          </small>
                        )}
                      </FormGroup>
                      <FormGroup>
                        <InputGroup>
                          <InputGroupAddon
                            addonType="prepend"
                            className="w-25"
                          >
                            <InputGroupText className="w-100">
                              Cod. Catastro
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            id="meter_cod_catasM"
                            name="meter_cod_catasM"
                            type="text"
                            onChange={this.handleChange}
                          />
                        </InputGroup>
                        {submitedAdd && !meter_cod_catasM && (
                          <small className=" badge badge-danger">
                            Debe ingresar un código de catastro{" "}
                          </small>
                        )}
                      </FormGroup>
                      <FormGroup>
                        <Label for="modelM">Modelo</Label>
                        <Input
                          type="select"
                          name="modelM"
                          id="modelM"
                          onChange={this.handleChange}
                        >
                          {models.length !== 0 &&
                            models.map(item => (
                              <option value={item.id} key={item.id}>
                                {item.m_description}
                              </option>
                            ))}
                        </Input>
                      </FormGroup>

                      <FormGroup>
                        <InputGroup>
                          <InputGroupAddon
                            addonType="prepend"
                            className="w-25"
                          >
                            <InputGroupText className="w-100">
                              Ruta
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            id="meter_routeM"
                            name="meter_routeM"
                            type="text"
                            onChange={this.handleChange}
                          />
                        </InputGroup>
                        {submitedAdd && !meter_routeM && (
                          <small className=" badge badge-danger">
                            Debe ingresar un código de ruta{" "}
                          </small>
                        )}
                      </FormGroup>
                      <FormGroup className="form-actions text-center">
                        <Button
                          onClick={this.handleSubmit}
                          type="submit"
                          color="primary"
                          disabled={buttonDisabled}
                        >
                          <i className="fa fa-check" /> Actualizar
                        </Button>
                        <Button
                          type="reset"
                          color="danger"
                          onClick={() =>
                            this.setState({ modalUpdate: false })
                          }
                        >
                          <i className="fa fa-ban" /> Cerrar
                        </Button>
                      </FormGroup>
                    </Form>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

export default SearchClient;
