import React, { Component } from "react";
import update from 'immutability-helper'

import { clientServices } from "../../../services/clientServices";
import { validation } from '../../../helpers/validation'

class NewClient extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dni: "",
      name: "",
      lastname: "",
      cellphone: "",
      email: "",
      daybirth: "",
      submited: false,
      buttonDisabled: false
    };
  }

  handleChange = e => {
    const { value, name } = e.target;
    this.setState({
      [name]: value
    });
  };

  clearText = () => {
    this.setState(prevState =>
      update(prevState, {
        name: { $set: "" },
        lastname: { $set: "" },
        cellphone: { $set: "" },
        email: { $set: "" },
        daybirth: { $set: "" }
      })
    );
  };

  verifyText = () => {
    const { dni, name, lastname, cellphone, email, daybirth } = this.state;
    if (
      dni === "" ||
      name === "" ||
      lastname === "" ||
      email === "" ||
      daybirth === "" ||
      cellphone === ""
    ) {
      return false;
    }
    if (
      validation.isNumber(dni) === true &&
      validation.isNumber(cellphone) &&
      validation.isEmail(email)
    ) {
     
      return true;
    }
    return false;
  };

  handleSubmit = () => {
    
    const {
      submited,
      dni,
      name,
      lastname,
      cellphone,
      email,
      daybirth
    } = this.state;
    
  
    this.setState({
      submited: true
    })

    if (this.verifyText()) {
     
      this.setState({ buttonDisabled: true })
      clientServices
        .createClient(dni, name, lastname, email, daybirth, cellphone)
        .then(res =>
          this.setState({
            buttonDisabled: false
          })
        );
    }
  };

  render() {
    const {
      submited,
      dni,
      name,
      lastname,
      cellphone,
      address,
      email,
      daybirth,
      buttonDisabled
    } = this.state;
    return (
      <div className="col-xs-8">
        <div className="card">
          <div className="card-header">
            <strong>Crear</strong> Cliente
          </div>
          <div className="row mt-2">
            <div className="col-12">
              <div className="mt-3 ml-4 card-body">
                <form>
                  <div className="form-group">
                    <div className="input-group">
                      <div className="input-group-prepend">
                        <span className="input-group-text">Código</span>
                      </div>
                      <input
                        type="text"
                        name="dni"
                        id="dni"
                        className="form-control"
                        onChange={this.handleChange}
                      />
                    </div>
                    {submited && !dni && (
                      <small className=" badge badge-danger">
                        Debe ingresar un código{" "}
                      </small>
                    )}
                    {submited && !validation.isNumber(dni) && (
                      <small className=" badge badge-danger">
                        Debe ser un número{" "}
                      </small>
                    )}
                  </div>
                  <div className="row">
                    <div className="col-12 col-md-6 form-group">
                      <div className="input-group">
                        <div className="input-group-prepend">
                          <span className="input-group-text">Nombres</span>
                        </div>
                        <input
                          type="text"
                          id="name"
                          name="name"
                          onChange={this.handleChange}
                          className="form-control"
                        />
                      </div>
                      {submited && !name && (
                        <small className=" badge badge-danger">
                          Debe ingresar un Nombre{" "}
                        </small>
                      )}
                    </div>
                    <div className="col-12 col-md-6 form-group">
                      <div className="input-group">
                        <div className="input-group-prepend">
                          <span className="input-group-text">Apellidos</span>
                        </div>
                        <input
                          id="lastname"
                          name="lastname"
                          onChange={this.handleChange}
                          className="form-control"
                        />
                      </div>
                      {submited && !lastname && (
                        <small className=" badge badge-danger">
                          Debe ingresar los Apellidos{" "}
                        </small>
                      )}
                    </div>
                    <div className="col-12 col-md-6 form-group">
                      <div className="input-group">
                        <div className="input-group-prepend">
                          <span className="input-group-text">Correo</span>
                        </div>
                        <input
                          id="email"
                          name="email"
                          onChange={this.handleChange}
                          className="form-control"
                        />
                      </div>
                      {submited && !email && (
                        <small className=" badge badge-danger">
                          Debe ingresar un Correo{" "}
                        </small>
                      )}
                      {submited && !validation.isEmail(email) && (
                        <small className=" badge badge-danger">
                          Debe ser un email válido{" "}
                        </small>
                      )}
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12 col-md-6 form-group">
                      <div className="input-group">
                        <div className="input-group-prepend">
                          <span className="input-group-text">Nac.</span>
                        </div>
                        <input
                          id="daybirth"
                          name="daybirth"
                          onChange={this.handleChange}
                          type="date"
                          className="form-control"
                        />
                      </div>
                      {submited && !daybirth && (
                        <small className=" badge badge-danger">
                          Debe ingresar una Fecha{" "}
                        </small>
                      )}
                    </div>
                    <div className="col-12 col-md-6 form-group">
                      <div className="input-group">
                        <div className="input-group-prepend">
                          <span className="input-group-text">Celular</span>
                        </div>
                        <input
                          id="cellphone"
                          name="cellphone"
                          onChange={this.handleChange}
                          type="text"
                          className="form-control"
                        />
                      </div>
                      {submited && !cellphone && (
                        <small className=" badge badge-danger">
                          Debe ingresar un Celular{" "}
                        </small>
                      )}
                      {submited && !validation.isNumber(cellphone) && (
                        <small className=" badge badge-danger">
                          Debe ser un número{" "}
                        </small>
                      )}
                    </div>
                  </div>
                </form>
              </div>
              <div className="card-footer">
                <button
                disabled = { buttonDisabled }
                  onClick={this.handleSubmit}
                  className="btn btn-sm btn-success"
                  type="submit"
                >
                  <i className="fa fa-check" /> Grabar
                </button>
                <button className="btn btn-sm btn-danger" type="reset">
                  <i className="fa fa-ban" /> Cancelar
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default NewClient;
