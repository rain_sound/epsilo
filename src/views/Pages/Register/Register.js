import React, { Component } from "react";
import { userService } from '../../../services/userServices'
import ToastList from '../../Notifications/Toasts/ToastList'

class Register extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      password: "",
      confirmPassword: "",
      email: "",
      submited: false
    };
  }
  handleSubmit = e => {
    e.preventDefault();
    this.setState({ submited: true, modal: false });
    const { username, password, confirmPassword, email } = this.state;

    if (username && password && email && password === confirmPassword) {
      userService.register(username, password, email).then(res => this.props.history.replace('/login'));
    }
  };

  handleChange = e => {
    const { name, value } = e.target;
    this.setState({
      [name]: value
    });
  };

  render() {
    const { username, password, confirmPassword, email, submited } = this.state
    return (
      <div className="app flex-row align-items-center">
        <div className="container">
        <ToastList/>/ 
          <div className="row justify-content-center">
            <div className="col-md-6">
              <div className="card mx-4">
                <div className="card-body p-4">
                  <h1>Registro</h1>
                  <p className="text-muted">Crea tu cuenta</p>
                  <div className="input-group mb-3">
                    <div className="input-group-prepend">
                      <span className="input-group-text">
                        <i className="icon-user" />
                      </span>
                    </div>
                    <input className="form-control" placeholder="Ingrese un username" type="text" name="username" value={ username } onChange={ this.handleChange }/>
                  </div>
                  { submited && !username && <small className="badge badge-danger"> Debe de ingresar un username </small>}
                  <div className="input-group mb-3">
                    <div className="input-group-prepend">
                      <span className="input-group-text"><i className="icon-lock" /></span>
                    </div>
                    <input className="form-control" placeholder="Ingrese una contraseña" type="password" name="password" value={ password } onChange={ this.handleChange }/>
                  </div>
                  { submited && !password && <small className="badge badge-danger"> Debe de ingresar un contraseña </small>}
                  <div className="input-group mb-3">
                    <div className="input-group-prepend">
                      <span className="input-group-text">
                        <i className="icon-lock" />
                      </span>
                    </div>
                    <input className="form-control" placeholder="Repita la contraseña " type="password" name="confirmPassword" value={ confirmPassword } onChange={ this.handleChange }/>
                  </div>
                  { submited && !(confirmPassword === password) && <small className="badge badge-danger"> Las contraseñas deben de ser iguales </small>}
                  <div className="input-group mb-4">
                    <div className="input-group-prepend">
                      <span className="input-group-text">
                        @
                      </span>
                    </div>
                    <input className="form-control" placeholder="Ingrese un correo" type="text" name="email" value={ email } onChange={ this.handleChange }/>
                  </div>
                  { submited && !email && <small className="badge badge-danger"> Debe de ingresar un correo </small>}
                  <button onClick={ this.handleSubmit } className="btn btn-block btn-success" type="button">
                    Crear cuenta
                  </button>
                </div>
                <div className="card-footer p-4" />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Register;
