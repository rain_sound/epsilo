import React, { Component } from "react";
import update from "immutability-helper";

import { employeeServices } from "../../../services/employeeServices";
import { validation } from "../../../helpers/validation";
class NewEmployee extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      lastname: "",
      cellphone: "",
      address: "",
      email: "",
      daybirth: "",
      submited: false,
      buttonDisabled: false
    };
  }

  handleChange = e => {
    const { value, name } = e.target;
    this.setState({
      [name]: value
    });
  };

  clearText = () => {
    this.setState(prevState =>
      update(prevState, {
        name: { $set: "" },
        lastname: { $set: "" },
        cellphone: { $set: "" },
        email: { $set: "" },
        daybirth: { $set: "" },
        address: { $set: "" }
      })
    );
  };

  handleSubmit = () => {
    const {
      submited,
      name,
      lastname,
      cellphone,
      email,
      daybirth,
      address
    } = this.state;
    this.setState({
      submited: true
    });
    if (
      !validation.isBlank({
        name,
        lastname,
        address,
        cellphone,
        email,
        daybirth
      }) && validation.isNumber(cellphone) && validation.isEmail(email)
    ) {
      this.setState({
        buttonDisabled: true
      });
      employeeServices
        .createEmployee(name, lastname, email, daybirth, cellphone, address)
        .then(res =>
          this.setState({
            buttonDisabled: false
          })
        );
    }
  };

  render() {
    const {
      submited,
      name,
      lastname,
      cellphone,
      address,
      email,
      daybirth,
      buttonDisabled
    } = this.state;
    return (
      <div className="col-xs-8">
        <div className="card">
          <div className="card-header">
            <strong>Crear</strong> Trabajador
          </div>
          <div className="row mt-2">
            <div className="col-12">
              <div className="mt-3 ml-4 card-body">
                <form>
                  <div className="row">
                    <div className="col-12 col-md-6 form-group">
                      <div className="input-group">
                        <div className="input-group-prepend">
                          <span className="input-group-text">Nombres</span>
                        </div>
                        <input
                          type="text"
                          id="name"
                          name="name"
                          onChange={this.handleChange}
                          className="form-control"
                        />
                      </div>
                      {submited && !name && (
                        <small className=" badge badge-danger">
                          Debe ingresar un Nombre{" "}
                        </small>
                      )}
                    </div>
                    <div className="col-12 col-md-6 form-group">
                      <div className="input-group">
                        <div className="input-group-prepend">
                          <span className="input-group-text">Apellidos</span>
                        </div>
                        <input
                          id="lastname"
                          name="lastname"
                          onChange={this.handleChange}
                          className="form-control"
                        />
                      </div>
                      {submited && !lastname && (
                        <small className=" badge badge-danger">
                          Debe ingresar los Apellidos{" "}
                        </small>
                      )}
                    </div>
                    <div className="col-12 col-md-6 form-group">
                      <div className="input-group">
                        <div className="input-group-prepend">
                          <span className="input-group-text">Correo</span>
                        </div>
                        <input
                          id="email"
                          name="email"
                          onChange={this.handleChange}
                          className="form-control"
                        />
                      </div>
                      {submited && !email && (
                        <small className=" badge badge-danger">
                          Debe ingresar un Correo{" "}
                        </small>
                      )}
                      {submited && !validation.isEmail(email) && (
                        <small className=" badge badge-danger">
                          El correo debe ser válido{" "}
                        </small>
                      )}
                    </div>
                    <div className="col-12 col-md-6 form-group">
                      <div className="input-group">
                        <div className="input-group-prepend">
                          <span className="input-group-text">Dirección</span>
                        </div>
                        <input
                          id="address"
                          name="address"
                          onChange={this.handleChange}
                          className="form-control"
                        />
                      </div>
                      {submited && !address && (
                        <small className=" badge badge-danger">
                          Debe ingresar una Dirección{" "}
                        </small>
                      )}
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12 col-md-6 form-group">
                      <div className="input-group">
                        <div className="input-group-prepend">
                          <span className="input-group-text">Nac.</span>
                        </div>
                        <input
                          id="daybirth"
                          name="daybirth"
                          onChange={this.handleChange}
                          type="date"
                          className="form-control"
                        />
                      </div>
                      {submited && !daybirth && (
                        <small className=" badge badge-danger">
                          Debe ingresar una Fecha{" "}
                        </small>
                      )}
                    </div>
                    <div className="col-12 col-md-6 form-group">
                      <div className="input-group">
                        <div className="input-group-prepend">
                          <span className="input-group-text">Celular</span>
                        </div>
                        <input
                          id="cellphone"
                          name="cellphone"
                          onChange={this.handleChange}
                          type="text"
                          className="form-control"
                        />
                      </div>
                      {submited && !cellphone && (
                        <small className=" badge badge-danger">
                          Debe ingresar un Celular{" "}
                        </small>
                      )}
                      {submited && !validation.isNumber(cellphone) && (
                        <small className=" badge badge-danger">
                          Debe ingresar un número{" "}
                        </small>
                      )}
                    </div>
                  </div>
                </form>
              </div>
              <div className="card-footer">
                <button
                  onClick={this.handleSubmit}
                  className="btn btn-sm btn-success"
                  type="submit"
                  disabled={buttonDisabled}
                >
                  <i className="fa fa-check" /> Grabar
                </button>
                <button className="btn btn-sm btn-danger" type="reset">
                  <i className="fa fa-ban" /> Cancelar
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default NewEmployee;
