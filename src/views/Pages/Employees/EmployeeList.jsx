import React, { Component } from "react";
import {
  Button,
  Card,
  CardBody,
  Col,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Modal,
  ModalHeader,
  ModalBody,
  Row
} from "reactstrap";
import { employeeServices } from '../../../services/employeeServices'
import CardFacebook from "../../../components/Other/CardFacebook";

class EmployeeList extends Component {
  constructor() {
    super()
    this.state = {
      employees: [],
      filteredEmployees: [],
      modalInfo: false,
      employee: null,
      txtSearch:""
    };
  }

  componentDidMount = () => {
    const { list } = this.props 
    if(!list || list.length === 0) {
      employeeServices.getAll().then(res => {
        this.setState({ employees: res.data, filteredEmployees: res.data })
      })
    }
  }

  handleChange = e => {
    const { employees } = this.state
    const { name, value } = e.target;
    this.setState({
      [name]: value.toLowerCase(),
    });
    
    if(value.length === 0){
      this.setState({ filteredEmployees: employees })
    }
    else {
      this.filterEmployee()
    }
  };
  
  filterEmployee = () => {
    const { txtSearch, employees } = this.state
    let itemLower
    console.log('empleados', employees);
    const filteredEmployees = employees.filter(item => {
      itemLower = (item.e_name).toLowerCase()
      return (itemLower).includes(txtSearch)
    })
    console.log('empleados filtrados', filteredEmployees);
    this.setState({ filteredEmployees })
  }

  toggle = (id) => {
    console.log('id', id);
    const { employees } = this.state
    /* console.log('entro', employees.filter(item => item.id === id)); */
    this.setState({
      modalInfo: !this.state.modalInfo,
      employee: employees.filter(item => item.id === id)[0]
    });
  };

 

  render() {
    const { employees, filteredEmployees,  employee, id, name, lastname, email, txtSearch } = this.state
    return (
      <div className="container">
        <div className="row">
          <div className="col-8 col-md-4">
            <div className="form-group">
              <div className="input-group">
                <div className="input-group-prepend">
                  <span className="input-group-text">Buscar</span>
                </div>
                <input
                  className="form-control"
                  id="txtSearch"
                  onChange={this.handleChange}
                  type="text"
                  name="txtSearch"
                />
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          {filteredEmployees.length !== 0  &&
            filteredEmployees.map(item => {
              if(item.e_name.includes(txtSearch)){
                return (
                  <CardFacebook
                    key={item.id}
                    id={item.id}
                    name={item.e_name}
                    lastname={item.e_lastname}
                    email={item.e_email}
                    profile={item.e_imgprofile}
                    toggle={this.toggle}
                  />
                );
              }
            
            })
           }
        </div>

        {/* Modal Update */}
        {employee && (
          <Modal isOpen={this.state.modalInfo} toggle={this.toggle}>
            <ModalHeader
              className="bg-info color-white"
              toggle={this.toggle}
            >
              Información Trabajador
            </ModalHeader>
            <ModalBody>
              <Row>
                <Col xs="12">
                  <Card>
                    <CardBody>
                      <Form action="" method="post">
                        <FormGroup>
                          <InputGroup>
                            <InputGroupAddon
                              addonType="prepend"
                              className="w-25"
                            >
                              <InputGroupText className="w-100">
                                Codigo
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              type="text"
                              value={employee.id}
                              disabled
                            />
                          </InputGroup>
                        </FormGroup>
                        <FormGroup>
                          <InputGroup>
                            <InputGroupAddon
                              addonType="prepend"
                              className="w-25"
                            >
                              <InputGroupText className="w-100">
                                Nombre
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              type="text"
                              value={employee.e_name}
                              disabled
                            />
                            
                          </InputGroup>
                        </FormGroup>
                        <FormGroup>
                          <InputGroup>
                            <InputGroupAddon
                              addonType="prepend"
                              className="w-25"
                            >
                              <InputGroupText className="w-100">
                                Apellido
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              type="text"
                              value={employee.e_lastname}
                              disabled
                            />
                            
                          </InputGroup>
                        </FormGroup>
                        <FormGroup>
                          <InputGroup>
                            <InputGroupAddon
                              addonType="prepend"
                              className="w-25"
                            >
                              <InputGroupText className="w-100">
                                Correo
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              type="text"
                              value={employee.e_email}
                              disabled
                            />
                           
                          </InputGroup>
                        </FormGroup>
                        <FormGroup>
                          <InputGroup>
                            <InputGroupAddon
                              addonType="prepend"
                              className="w-25"
                            >
                              <InputGroupText className="w-100">
                                Nacimiento
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              type="text"
                              value={employee.e_daybirth}
                              disabled
                            />
                           
                          </InputGroup>
                        </FormGroup>
                        <FormGroup>
                          <InputGroup>
                            <InputGroupAddon
                              addonType="prepend"
                              className="w-25"
                            >
                              <InputGroupText className="w-100">
                                Celular
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              type="text"
                              value={employee.e_cellphone}
                              disabled
                            />
                           
                          </InputGroup>
                        </FormGroup>
                        <FormGroup>
                          <InputGroup>
                            <InputGroupAddon
                              addonType="prepend"
                              className="w-25"
                            >
                              <InputGroupText className="w-100">
                                Dirección
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              type="text"
                              value={employee.e_address}
                              disabled
                            />
                           
                          </InputGroup>
                        </FormGroup>
                        <FormGroup className="form-actions text-center">
                          <Button
                            type="reset"
                            color="danger"
                            onClick={this.toggle}
                          >
                            <i className="fa fa-ban" /> Cerrar
                          </Button>
                        </FormGroup>
                      </Form>
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </ModalBody>
          </Modal>
        )}
      </div>
    );
  }
}

export default EmployeeList;
