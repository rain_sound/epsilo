import React from 'react'
import { userService } from '../../../services/userServices'
import ToastList from '../../Notifications/Toasts/ToastList'

class ResetPassword extends React.Component {
    constructor(props){
        super(props)

        this.state = {
            email: '',
            submited: false,
        }
    }

    handleSubmit = (e) => {
        e.preventDefault()
        this.setState({ submited: true })
        const { email } = this.state
        if(email) userService.resetPassword(email)
        
    }

    handleChange = (e) => {
        e.preventDefault()
        this.setState({ email: e.target.value })
    }

    render() {
        const { email, submited } = this.state
        return(
            <div className="container">
            <ToastList/>
                <div className="row">
                 <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
                    <div className="card card-signin my-5">
                        <div className="card-body">
                            <h5 className="card-title text-center">Recuperar contrasenia</h5>
                            <form onSubmit={ this.handleSubmit }>
                                <input className="form-control" type="text" onChange={ this.handleChange } value={ email } placeholder="Ingrese correo electronico" />
                                { submited && !email && <small className="text--error">Debe de ingresar un correo</small>}
                                <input type="submit" className="mt-3 mb-3 form-control btn btn-primary" value="Recuperar"/>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        )
    }

}

export default ResetPassword
