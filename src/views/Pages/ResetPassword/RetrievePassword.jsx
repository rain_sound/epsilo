import React from 'react'

import { userService } from '../../../services/userServices'
import { Link } from 'react-router-dom'
import ToastList from '../../Notifications/Toasts/ToastList'

class RetrievePassword extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            name: '',
            password: '',
            submited: false,
            confirmPassword: '',
        }

     }

     componentDidMount() {
         console.log('enrto al retrieve')
        this.retrievePassword()
     }

     retrievePassword = () => {
        const urlParams = new URLSearchParams(window.location.search)
        userService.verifyToken(urlParams.get('token'))
                    .then(res => {
                        const { name } = res
                        this.setState({ name })
                    })
    
     }

     handleChange = (e) => {
        e.preventDefault()
        const { name, value } = e.target
        this.setState({
            [name]: value
        })
     }

     handleSubmit = (e) => {
         e.preventDefault()
         this.setState({ submited: true })
        const { password, confirmPassword, name } = this.state
        if(password && confirmPassword && (password === confirmPassword)) {
            userService.updatePassword(name, password, null)
        }
     }

     render() {
         const { password, confirmPassword, submited } = this.state
         return(
            <div className="container">
            <ToastList/>
                <div className="row">
                <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
                    <div className="card card-signin my-5">
                        <div className="card-body">
                            <h5 className="card-title text-center">Recuperar contraseña</h5>
                                <div>
                                    <form onSubmit={ this.handleSubmit }>
                                        <input placeholder="Ingrese nueva contraseña" name="password" value={ password } onChange={this.handleChange} className="form-control" type="password"/>
                                        { submited && !password && <small className="text--error">Debe de ingresar una contraseña</small>}
                                        <input placeholder="Vuelva a ingresar la contraseña" name="confirmPassword" value={ confirmPassword } onChange={this.handleChange} className="form-control mt-3" type="password"/>
                                        { !(password === confirmPassword) && <small className="text--error">Las contraseñas deben ser iguales</small>}
                                        <input type="submit" className="form-control mb-3 mt-3 btn btn-primary" value="Cambiar"/>
                                    </form>
                                    <Link to="/login" className="btn btn-primary form-control">Login</Link>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         )
     }
}

export default RetrievePassword