
import React, { Component } from 'react';
import { connect } from "react-redux";
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import ToastList from '../../Notifications/Toasts/ToastList'

import { userActions } from "../../../actions/user";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      submited: false,
      modal: false
    };
  }

  handleChange = e => {
    const { name, value } = e.target;
    this.setState({
      [name]: value
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.setState({ submited: true });
    const { username, password } = this.state;
    const { login, history } = this.props;
    if (username && password)
      login(username, password, history)
  };


  render() {
    return (
      <div className="w-100 bg-success col-12 header-bg">
      
      <div className="app flex-row align-items-center">
        <Container>
         
          <ToastList/>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form onSubmit={ this.handleSubmit }>
                      <h1>EPSILO</h1>
                      <p className="text-muted">Bienvenido a Epsilo</p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder="Username" name="username" onChange={ this.handleChange } autoComplete="username" />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" placeholder="Password" name="password" onChange={ this.handleChange } autoComplete="current-password" />
                      </InputGroup>
                      <Row>
                        <Col xs="6">
                          <Button color="primary" className="px-4">Ingresar</Button>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  login: (username, password, history) =>
    dispatch(userActions.login(username, password, history))
});

export default connect(null, mapDispatchToProps)(Login);
