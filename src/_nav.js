export default {
  items: [
    {
      name: "Usuarios",
      url: "users",
      icon: "icon-user",
      children: [
        {
          name: "Lista de Clientes",
          url: `/clients/list`,
          icon: "icon-list"
        },
        {
          name: "Nuevo Cliente",
          url: `/clients/new`,
          icon: "icon-user-follow"
        },
        {
          name: "Búsqueda de Clientes",
          url: `/clients/search`,
          icon: "icon-magnifier"
        },
        {
          name: "Lecturas de medidores",
          url: "/clients/lecture",
          icon: "icon-note"
        },
        {
          name: "Catastro",
          url: "/clients/catastro",
          icon: "icon-map"
        }
      ]
    },
    {
      name: "Trabajadores",
      url: "employee",
      icon: "icon-people",
      children: [
        {
          name: "Nuevo Trabajador",
          url: "/employees/new",
          icon: "icon-user-follow"
        },
        {
          name: "Lista de trabajadores",
          url: "/employees/list",
          icon: "icon-list"
        },
        {
          name: "Galeria Fotos Institucionales",
          url: "/employees/pictures",
          icon: "icon-picture"
        },
        {
          name: "Control de Asistencias",
          url: "/employees/asistance",
          icon: "icon-note"
        },
        {
          name: "Planillas",
          url: "/employees/planilla",
          icon: "icon-notebook"
        }
      ]
    },
    {
      name: "Mantenimientos",
      icon: "icon-wrench",
      children: [
        {
          name: "Áreas",
          url: "/sections",
          icon: "icon-settings"
        },
        {
          name: "Modelos medidor",
          url: "/models",
          icon: "icon-settings"
        }
      ]
    },
    {
      name: "Otros",
      icon: "icon-arrow-down-circle",
      children: [
        {
          name: "Activos fijos",
          url: "/other/activos",
          icon: "icon-list"
        },
        {
          name: "Tramite documentario",
          url: "/other/tramites",
          icon: "icon-folder-alt"
        },
        {
          name: "Caja chica (data market)",
          url: "/other/caja",
          icon: "icon-drawer"
        }
      ]
    }
  ]
};
