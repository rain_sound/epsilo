import { userService } from '../services/userServices'
import { userConstants } from '../constants/user/actions'

const login = (name, password, history) => {

    const request = payload => ({ type: userConstants.LOGGING, payload })
    const success = payload => ({ type: userConstants.LOGED, payload })
    const failure = payload => ({ type: userConstants.FAILURE, payload })

    return dispatch => {
        dispatch(request({ name }))
        userService.login(name, password)
            .then(data => {
                const { auth, reason, name, id, email, imgProfile } = data
                if(auth) { 
                    dispatch(success({ auth, name, id, email, imgProfile }))
                    history.push('foro')
                }
                else dispatch(failure({ reason })) 

            })
    }
}

const logout = () => {
    localStorage.clear()
    const request = ()      => ({ type: userConstants.LOGOUT })
    return dispatch => {
        dispatch(request())
    }
}


const updateImage = (image, id) => {
    
    const request = ()      => ({ type: userConstants.UPDATE_IMAGE_REQUEST })
    const success = payload => ({ type: userConstants.UPDATE_IMAGE_SUCCESS, payload })
    const failure = payload => ({ type: userConstants.UPDATE_IMAGE_FAILURE, payload })

    return dispatch => {
        dispatch(request())
        userService.updateImage(image, id)
            .then(data => {
                const { data:{ filename } ,  message } = data
                if(filename) {
                    dispatch(success({ filename, message }))
                }
                else dispatch(failure({ message }))
            })
    }
}


const clearImage = () => {
    const request = ()      => ({ type: userConstants.CLEAR_IMAGE })
    
    return dispatch => {
        dispatch(request())
    }
}


export const userActions = {
    login,
    logout,
    updateImage,
    clearImage,
}
