import { clientConstants } from '../constants/client/actions'
import { clientServices } from '../services/clientServices'


const getAll = () => {

    const request = ()      => ({ type: clientConstants.GET_ALL_CLIENTS_REQUEST })
    const success = payload => ({ type: clientConstants.GET_ALL_CLIENTS_SUCCESS, payload })
    const failure = ()      => ({ type: clientConstants.GET_ALL_CLIENTS_FAILED })

    return dispatch => {
        dispatch(request())
        clientServices.getAll()
            .then(data => {
                const { err } = data
                if(!err) dispatch(success(data))
                else dispatch(failure())
            })
    }
}

const updateData = (id, name, lastname, email) => {
    
    const request = () => ({ type: clientConstants.UPDATE_CLIENT_REQUEST })
    const success = payload => ({ type: clientConstants.UPDATE_CLIENT_SUCCESS, payload })
    const failure = payload => ({ type: clientConstants.UPDATE_CLIENT_FAILED, payload })

    return dispatch => {
        dispatch(request())
        clientServices.updateData(id, name, lastname, email)
            .then(data => {
                const { err } = data
                if(!err) dispatch(success(data))
                else dispatch(failure(err))
            })
    }

}

export const clientActions = {
    getAll,
    updateData
}