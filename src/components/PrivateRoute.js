import React from 'react'
import { Route } from 'react-router-dom'
import { connect } from 'react-redux';
import { Forbidden } from '../components/Forbidden'

const Test = () => {
    return (<div>error</div>)
}
 const PrivateRoute = ({ user, component: Component, ...rest }) => {
    if(Component) {
        return <Route { ...rest } render={ props =>
            localStorage.getItem('user')? 
            <Component { ...props }/>:
            <Forbidden />
        }/>
    }
    else {
        if (localStorage.getItem('user')) return <Route {...rest}/>}
        return  <Forbidden />
}

const mapStateToProps = ({ user }) => ({ user })


export default connect(mapStateToProps, null)(PrivateRoute)