import React from 'react';
import { MdPerson } from 'react-icons/md'
import { Component } from 'react';

class DropDown extends Component {
    constructor(props){
        super(props);
        this.state = {
            toggleDropDown: false
        }
    }

  toggle = () => {
      this.setState({ toggleDropDown: !this.state.toggleDropDown })
  }

  render () {
    const { name, children } = this.props
    const { toggleDropDown } = this.state
    return (
        <div className="dropdown" onClick={ this.toggle }>
            <button className="btn btn-secondary dropdown__button" type="button">
            <MdPerson className="sidebar__icon"/>
            { name }
            </button>
            <div className={`"dropdown-menu ${toggleDropDown?'dropdown--show':'dropdown--hide'}`}>
                <div>
                    { children }
                </div>
            </div>
        </div>
    );
   
  }
}

export default (DropDown);