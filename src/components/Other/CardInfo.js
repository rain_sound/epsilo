import React from "react";
import { Card, CardTitle, Col } from "reactstrap";

const CardInfo = ({ width, body, title }) => {
  return (
    <Col sm={width}>
      <Card body>
        <CardTitle className="text-center">
          <h3>{title} </h3>
        </CardTitle>
        {body}
      </Card>
    </Col>
  );
};

export default CardInfo;
