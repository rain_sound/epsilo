import React from "react";

const CardFacebook = ({ width,id, name, lastname, email, profile, toggle }) => {
  return (
    <div className="mt-2 col-6 col-md-4">
      <div className="row card--facebook">
        <div className="col-12 col-md-5 card--facebook--image">
          <img
            className="img-fluid "
            src={profile}
            /* src={`${URL_PUBLIC}${profile}`} */
            alt=""
          />
        </div>
        <div className="col-12 col-md-7">
          <p>{`${name} ${lastname}`}</p>
          <p className="text-muted card--facebook--content">{email}</p>
        </div>
        <div className="row">
          <div className="col-12 ">
              <input
                className="text-white btn btn-sm btn-info mr-2"
                type="button"
                value="Mas información"
                onClick={ () => toggle(id) }
              />
          </div>
        </div>
      </div>
    </div>
  );
};

export default CardFacebook;
