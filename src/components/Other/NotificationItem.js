import React from "react";
import { ListGroup, ListGroupItem } from 'reactstrap';

const NotificationItem = ({ comment }) => {

  return (
    <ListGroup className="list-group-accent" tag={"div"}>
      <ListGroupItem className="list-group-item-accent-secondary bg-light text-center font-weight-bold text-muted text-uppercase small">
        Today
      </ListGroupItem>
      <ListGroupItem
        action
        tag="a"
        href="#"
        className="list-group-item-accent-warning list-group-item-divider"
      >
        <div className="avatar float-right">
          <img
            className="img-avatar"
            src="assets/img/avatars/7.jpg"
            alt="admin@bootstrapmaster.com"
          />
        </div>
        <div>
          <strong>{ comment.user }</strong> Respondio a tu post
        </div>
        <small className="text-muted mr-3">
          <i className="icon-calendar" />&nbsp; { comment.time }
        </small>
      </ListGroupItem>
    </ListGroup>
  );
};

export default NotificationItem;
