import React, { Component } from "react";

class Toast extends Component {
  render() {
    return (
      <div className={`toast ${this.props.type === 'error'? 'toast--error':'toast--success'}`}>
        <p className="toast__content">
          {this.props.message}
        </p>
        
        <button className="toast__dismiss" onClick={this.props.onDismissClick}>
          x
        </button>
      </div>
    );
  }

  shouldComponentUpdate() {
    return false;
  }
}

export default Toast;