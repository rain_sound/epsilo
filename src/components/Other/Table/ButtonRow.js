import React from "react";

const ButtonRow = ({ title, toggle, row, width, type }) => {
  return (
    <React.Fragment>
      <button
        className={`btn btn-${type} ${width}`}
        onClick={() => {
          toggle(row);
        }}
      >
        {title}
      </button>
    </React.Fragment>
  );
};

export default ButtonRow;
