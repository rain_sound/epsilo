import React, { Component } from "react";
import { connect } from "react-redux";
import { CardImg } from "reactstrap";
import { userActions } from "../../actions/user";
import { URL_PUBLIC } from "../../helpers/api";

//IMPORTANT:
// Config Variables Component

const MAX_SIZE_IMAGE = 2 * 1024 * 1024;
const EXT_ALLOWED_IMAGE = ["jpeg", "jpg"];

const ERROR_SIZE = "Archivo muy grande";
const ERROR_EXTENSION = "Extension no permitida";

class CardImage extends Component {
  constructor() {
    super();
    this.state = {
      error: null,
      srcImage: "",
      selectedImage: null,
      file: null
    };
  }


  handleChangeImage = e => {
    //Clear messages
    this.props.clearImage();

    const fileTemp = e.target.files[0];

    this.setState({
      selectedImage: fileTemp,
      file: URL.createObjectURL(fileTemp),
      error: null
    });

    //SIZE validation
    if (fileTemp.size > MAX_SIZE_IMAGE) {
      this.setState({ error: ERROR_SIZE });
    }

    //Extension validation
    var checked = false;
    EXT_ALLOWED_IMAGE.forEach(ext => {
      if (`image/${ext}` === fileTemp.type) {
        checked = true;
      }
    });
    if (!checked) this.setState({ error: ERROR_EXTENSION });
  };

  handleUpdate = e => {
    e.preventDefault();

    const { selectedImage } = this.state;
    const { id } = this.props.user;
    if (selectedImage) {
      this.props.updateImage(selectedImage, id);
      this.setState({ file: null });
    }
  };

  render() {
    const { file, error } = this.state;
    return (
      <form onSubmit={this.handleUpdate}>
        <div className="form-group form-row ">
          <div className="col-12 text-center">
            <input hidden
                  accept="image/*"
                  name="image"
                  id="image"
                  type="file"
                  onChange={this.handleChangeImage} />
            <label htmlFor="image" className="mt-2 btn btn-sm btn-info text-white">Choose a file</label>
            <input
              className="ml-2 btn btn-sm btn-success"
              disabled={error}
              type="submit"
              value="Subir"
            />
          </div>
          <div className="col-12 text-center">
            <CardImg
              top
              width="100px"
              height="auto"
              src={file ? file : `${URL_PUBLIC}${this.props.imgSrc}`}
              alt="Card image cap"
              className={`img--default ${
                error
                  ? "img--validation--error"
                  : "img--validation--success"
              }`}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-12 text-center">
            
          </div>
        </div>
      </form>
    );
  }
}

const mapStateToProps = ({ user }) => ({
  user,
  imgSrc: user.profile && user.profile.filename,
});

const mapDispatchToProps = dispatch => ({
  updateImage: (image, id) => dispatch(userActions.updateImage(image, id)),
  clearImage: () => dispatch(userActions.clearImage())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CardImage);
