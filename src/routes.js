import React from 'react';
const Clients           = React.lazy(() => import('./views/Pages/Clients/ClientList'))
const Employees         = React.lazy(() => import('./views/Pages/Employees/EmployeeList'))
const SearchClient      = React.lazy(() => import('./views/Pages/Clients/SearchClient'))
const NewClient         = React.lazy(() => import('./views/Pages/Clients/NewClient'))
const NewEmployee       = React.lazy(() => import('./views/Pages/Employees/NewEmployee'))
const RegisterService   = React.lazy(() => import('./views/Pages/Clients/RegisterService'))
const Profile           = React.lazy(() => import('./views/Pages/Profile/Profile'))
const PasswordRecovery  = React.lazy(() => import('./views/Pages/Profile/PasswordRecovery'))

const routes = [
   { path: '/', exact: true, name: 'Home'}, 
   { path: '/employees' ,exact: true, name: 'Empleados'} ,
   { path: '/employees/new' ,exact: true, name: 'Nuevo', component: NewEmployee } ,
   { path: '/employees/list' ,exact: true, name: 'Lista', component: Employees } ,
   { path: '/clients' ,exact: true, name: 'Clientes'} ,
   { path: '/clients/list', exact: true, name: 'Lista', component: Clients},
   { path: '/clients/new', exact: true, name: 'Nuevo', component: NewClient},
   { path: '/clients/search', exact: true, name: 'Busqueda', component: SearchClient},
   { path: '/clients/lecture', exact: true, name: 'Lectura', component: RegisterService},
   { path: '/profile', exact:true, name: 'Perfil' },
   { path: '/profile/:name/reset', name: 'Resetear Contraseña', component: PasswordRecovery },
   { path: '/profile/:name', name: 'Imagen de perfil', component: Profile }
];

export default routes;
