import api from '../helpers/api'
import { clientConstants } from '../constants/client/api'

const getAll = () => {
    return api
    .get(clientConstants.GET_ALL_CLIENTS)
    .then(res => res)
}

const getClientById = id => {
    return api
     .get(clientConstants.GET_CLIENT_BY_ID(id))
     .then(res => res)
}

const getClientBill = id => {
    return api
     .get(clientConstants.GET_CLIENT_BILL(id))
     .then(res => res)
}

const createClient = (id, username, lastname, email, daybirth, cellphone) => {
    return api
     .post(clientConstants.CREATE_CLIENT, {
         id,
         username,
         lastname,
         email,
         daybirth,
         cellphone
     })

}

const updateData = ( id, c_name, c_lastname, c_email ) => {
    return api
     .put(clientConstants.UPDATE_CLIENT, {
        id,
        c_name,
        c_lastname,
        c_email
     })
     .then(res => res)
}


export const clientServices = {
    getAll,
    getClientById,
    getClientBill,
    updateData,
    createClient
}