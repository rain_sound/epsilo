import api from '../helpers/api'
import { employeeConstants } from '../constants/employee/api'

const getAll = () => {
    return api
    .get(employeeConstants.GET_ALL_EMPLOYEES)
    .then(res => res)
}

const getClientById = id => {
    return api
     .get(employeeConstants.GET_CLIENT_BY_ID(id))
     .then(res => res)
}


const createEmployee = ( username, lastname, email, daybirth, cellphone, address) => {
    return api
     .post(employeeConstants.CREATE_EMPLOYEE, {
         username,
         lastname,
         email,
         daybirth,
         cellphone,
         address
     })

}


export const employeeServices = {
    getAll,
    createEmployee,
    getClientById
}