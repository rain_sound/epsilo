import api from "../helpers/api";
import { serviceConstants } from "../constants/service/api";


const getAll = () => {
  return api
    .get(serviceConstants.GET_ALL)
    .then(res => res)
};

const getAllCategories = () => {
  return api
    .get(serviceConstants.GET_ALL_CATEGORIES)
    .then(res => res)
}

const getAllModels = () => {
  return api
    .get(serviceConstants.GET_ALL_MODELS)
    .then(res => res)
}

const createMeter = (clientId, categoryId, m_serie, m_route, m_address, m_catastro_code, modelMeterId) => {
  return api
    .post(serviceConstants.CREATE_METER, {
      m_serie,
      m_route,
      m_address,
      m_catastro_code,
      clientId,
      categoryId,
      modelMeterId
    })
    .then(res => res);
}

const registerBill = (r_reading, r_date_issue, meterId, services) => {
  return api
    .post(serviceConstants.REGISTER_BILL, {
      r_reading,
      r_date_issue,
      meterId,
      services
    })
    .then(res => res);
}

export const serviceServices = {
  getAll,
  getAllCategories,
  createMeter,
  getAllModels,
  registerBill
};
