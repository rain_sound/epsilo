import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { DropdownItem, DropdownMenu, DropdownToggle, Nav, NavItem } from 'reactstrap';
import { connect } from 'react-redux'
import PropTypes from 'prop-types';

import { AppAsideToggler, AppHeaderDropdown, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import logo from '../../assets/img/brand/eps.png'

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {
 
  render() {
    // eslint-disable-next-line
    const { children, user, ...attributes } = this.props;

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          full={{ src: logo, width: 100, height: 40, alt: "Innicia Logo" }}
          minimized={{
            src: logo,
            width: 60,
            height: 35,
            alt: "Innicia Logo"
          }}
        />
        <AppSidebarToggler className="d-md-down-none" display="lg" />

        <Nav className="d-md-down-none" navbar>
          <NavItem className="px-3">
            <NavLink to="/dashboard" className="nav-link text-white">
              Epsilo
            </NavLink>
          </NavItem>
        </Nav>
        <Nav className="ml-auto" navbar>
          <NavItem className="px-3">
            <NavLink to={`/profile/${user.name}/reset`} className="nav-link text-white">
              Perfil
            </NavLink>
          </NavItem>
          <NavItem className="px-3">
            <NavLink to={`/profile/${user.name}`} className="nav-link text-white">
              Cuenta
            </NavLink>
          </NavItem>
          <span>
            <strong>{user.name.toUpperCase()}</strong>
          </span>
          <AppHeaderDropdown className="text-white" direction="down">
            <DropdownToggle nav>
              <img
                src={logo}
                className="img-avatar"
                alt="admin@bootstrapmaster.com"
              />
            </DropdownToggle>
            <DropdownMenu  right style={{ right: "auto" }}>
              <DropdownItem >
                <i className="fa fa-shield" /> Lock Account
              </DropdownItem>
              <DropdownItem onClick={e => this.props.onLogout(e)}>
                <i className="fa fa-lock" /> Logout
              </DropdownItem>
            </DropdownMenu>
          </AppHeaderDropdown>
        </Nav>
        <AppAsideToggler className="d-md-down-none" />
        {/*<AppAsideToggler className="d-lg-none" mobile />*/}
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

const mapStateToProps = ({ user }) => ({ user })

export default connect(mapStateToProps, null)(DefaultHeader);
