

const isNumber = text => {
    if(!isNaN(text)){
        if(text.length === 0) {
            return false
        }
        return true
    }
    return false
}

const isEmail = text => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(text).toLowerCase());
}

const isBlank = object => {
    var isValid = false
    Object.keys(object).forEach(key => {
      var value = object[key]
      if(value === "" || value === undefined || value.length === 0){
        isValid = true
      } 
    })
    return isValid
  }

export const validation = {
    isNumber,
    isEmail,
    isBlank
}