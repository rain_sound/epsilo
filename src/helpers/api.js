import axios from 'axios';
import { authHeader } from './auth_header'
import { Service } from 'axios-middleware';
import { alertActions } from '../actions/alert'
import { store } from '../store/index'
import { generateId } from '../helpers/alerts'
//Se utiliza cuando se quieren hacer solicitudes con form-data
const port = 35000;

export const configFormData = { headers: { "Content-Type": "multipart/form-data" } };
export const URL = 'https://heldsoftware.com:35000/'
export const URL_PUBLIC = 'https://heldsoftware.com:35000/public/'


const client = axios.create({
  baseURL: URL,
  headers: authHeader(),
  responseType: 'json', 
})

const service = new Service(client);
service.register({
  onResponse(response) {
    const { data } = response; 
    const id = generateId()
    if(data.alert){
      store.dispatch(alertActions.addAlert(data, id))
    }
    return data;
  }
});

export default client;
