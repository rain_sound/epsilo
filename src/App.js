
import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { connect } from "react-redux";
// import { renderRoutes } from 'react-router-config';
import ToastList from './views/Notifications/Toasts/ToastList'
import './App.scss';

const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;

// Containers
const DefaultLayout = React.lazy(() => import('./containers/DefaultLayout'));

// Pages
const Login          = React.lazy(() => import('./views/Pages/Login/Login'));
const Register       = React.lazy(() => import('./views/Pages/Register/Register'));
const ResetPassword  = React.lazy(() => import('./views/Pages/ResetPassword'));
const UpdatePassword = React.lazy(() => import('./views/Pages/ResetPassword/RetrievePassword'));


class App extends Component {

  render() {
    return (
      <Router>
        
          <React.Suspense fallback={loading()}>
            <Switch>
            
              <Route exact path="/login" name="Login Page" render={props => <Login {...props}/>} />
              <Route exact path="/register" name="Register Page" render={props => <Register {...props}/>} />
              <Route exact path="/recoverypassword" name="Password Recovery Page" render={props => <ResetPassword {...props}/>} />
              <Route exact path="/clients/resetpassword" name="Upload Password Page" render={props => <UpdatePassword {...props}/>} />
              <Route path="/" name="Home" render={props => <DefaultLayout {...props}/>} />
              <ToastList/>
            </Switch>
          </React.Suspense>
      </Router>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user && state.user
});

export default connect(
  mapStateToProps,
  null
)(App);
