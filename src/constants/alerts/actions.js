//NOTE:
// Alert Toast
const ADD_TOAST         = 'ADD_TOAST'
const REMOVE_TOAST      = 'REMOVE_TOAST'
const CLEAR_ALL_TOAST   = 'CLEAR_ALL_TOAST'
//END NOTE:

export const alertConstants = {
    ADD_TOAST,
    REMOVE_TOAST,
    CLEAR_ALL_TOAST
}