import { URL } from '../../helpers/api'

const GET_ALL_CLIENTS  = `${URL}clients/`
const UPDATE_CLIENT    = `${URL}clients/updatedata`
const CREATE_CLIENT    = `${URL}clients/signup`
const GET_CLIENT_BY_ID  = id => `${URL}clients/${id}`
const GET_CLIENT_BILL = id =>  `${URL}clients/${id}/bill`

export const clientConstants = {
    GET_ALL_CLIENTS,
    GET_CLIENT_BY_ID,
    CREATE_CLIENT,
    UPDATE_CLIENT,
    GET_CLIENT_BILL
}