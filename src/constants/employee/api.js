import { URL } from '../../helpers/api'

const GET_ALL_EMPLOYEES  = `${URL}employees/`
const UPDATE_EMPLOYEES    = `${URL}employees/updatedata`
const CREATE_EMPLOYEE    = `${URL}employees/signup`

export const employeeConstants = {
    GET_ALL_EMPLOYEES,
    CREATE_EMPLOYEE,
    UPDATE_EMPLOYEES,
}