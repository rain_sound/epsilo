import { URL } from '../../helpers/api'

const SIGNIN            = `${URL}employees/signin` 
const SIGNUP            = `${URL}employees/signup` 
const FORGOTPASSWORD    = `${URL}employees/forgotpassword` 
const RESETPASSWORD     = `${URL}employees/resetpassword` 
const UPDATEPASSWORD    = `${URL}employees/updatepassword` 
const UPDATEIMAGE       = `${URL}employees/uploadImage` 

export const userConstants = {
    SIGNIN,
    SIGNUP,
    FORGOTPASSWORD,
    RESETPASSWORD,
    UPDATEPASSWORD,
    UPDATEIMAGE
}