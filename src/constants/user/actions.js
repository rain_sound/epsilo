//NOTE:
//User login
const LOGGING                 = 'USER_LOGGING'
const LOGED                   = 'USER_LOGED'
const LOGOUT                  = 'USER_LOGOUT'
const FAILURE                 = 'USER_FAILURE'
// END NOTE: 

//NOTE:
//Update image
const UPDATE_IMAGE_REQUEST    = 'USER_UPDATE_IMAGE_REQUEST'
const UPDATE_IMAGE_SUCCESS    = 'USER_UPDATE_IMAGE_SUCCESS'
const UPDATE_IMAGE_FAILURE    = 'USER_UPDATE_IMAGE_FAILURE'
const CLEAR_IMAGE             = 'USER_CLEAR_IMAGE'
// END NOTE: 

export const userConstants = {
    LOGGING,
    LOGED,
    LOGOUT,
    FAILURE,
    UPDATE_IMAGE_REQUEST,
    UPDATE_IMAGE_SUCCESS,
    UPDATE_IMAGE_FAILURE,
    CLEAR_IMAGE
}