import { URL } from '../../helpers/api'

    
const GET_ALL            = `${URL}services/` 
const CREATE_METER       = `${URL}services/new/meter` 
const REGISTER_BILL      = `${URL}services/new/bill` 
const GET_ALL_CATEGORIES = `${URL}categories/` 
const GET_ALL_MODELS     = `${URL}models/` 

export const serviceConstants = {
    GET_ALL,
    GET_ALL_CATEGORIES,
    CREATE_METER,
    GET_ALL_MODELS,
    REGISTER_BILL
}