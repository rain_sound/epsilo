package com.held_software.epsilo.util;

import android.os.Parcel;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jose_ on 3/12/2019.
 */

public class ConceptReceipt {
    public ConceptReceipt(){

    }

    @Override
    public String toString() {
        Map<String, String> receipt = new HashMap<>();
        receipt.put("cAfeIgv", cAfeIgv);
        receipt.put("cCodCnx", cCodCnx);
        receipt.put("cMonLet", cMonLet);
        receipt.put("cNroCir", cNroCir);
        receipt.put("cNroCon", cNroCon);
        receipt.put("cValor", cValor);
        receipt.put("categoria", categoria);
        receipt.put("cliente", cliente);
        return receipt.toString();
    }

    //public static final Creator<ConceptReceipt> CREATOR = new C18761();
    private String cAfeIgv;
    private String cCodCnx;
    private String cMonLet;
    private String cNroCir;
    private String cNroCon;
    private String cValor;
    private String categoria;
    private String cliente;
    private String codigoCat;
    private String codigoUnix;
    private String consumo;
    private String consumoReal;
    private String continuidad;
    private String desCon;
    private String direccion;
    private String distrito;
    private String docIdentidad;
    private String emision;
    private String fechaLecAct;
    private String fechaLecAnt;
    private String lecturaAct;
    private String lecturaAnt;
    private String medidor;
    private String nCargo;
    private String nMesDeu;
    private String nSubTot;
    private String nTotDeu;
    private String nTotal;
    private String nroCmp;
    private String rutaReparto;
    private String secuencia;
    private String vencimiento;

    /*static class C18761 implements Creator<ConceptReceipt> {
        C18761() {
        }

        public ConceptReceipt createFromParcel(Parcel in) {
            return new ConceptReceipt(in);
        }

        public ConceptReceipt[] newArray(int size) {
            return new ConceptReceipt[size];
        }
    }*/

    protected ConceptReceipt(Parcel in) {
        this.nroCmp = in.readString();
        this.emision = in.readString();
        this.vencimiento = in.readString();
        this.cliente = in.readString();
        this.direccion = in.readString();
        this.cCodCnx = in.readString();
        this.distrito = in.readString();
        this.docIdentidad = in.readString();
        this.codigoUnix = in.readString();
        this.codigoCat = in.readString();
        this.categoria = in.readString();
        this.rutaReparto = in.readString();
        this.cNroCir = in.readString();
        this.secuencia = in.readString();
        this.continuidad = in.readString();
        this.medidor = in.readString();
        this.lecturaAnt = in.readString();
        this.fechaLecAnt = in.readString();
        this.lecturaAct = in.readString();
        this.fechaLecAct = in.readString();
        this.consumoReal = in.readString();
        this.consumo = in.readString();
        this.cValor = in.readString();
        this.cNroCon = in.readString();
        this.desCon = in.readString();
        this.nCargo = in.readString();
        this.nSubTot = in.readString();
        this.nTotal = in.readString();
        this.nMesDeu = in.readString();
        this.nTotDeu = in.readString();
        this.cMonLet = in.readString();
        this.cAfeIgv = in.readString();
    }

    public String getNroCmp() {
        return this.nroCmp;
    }

    public void setNroCmp(String nroCmp) {
        this.nroCmp = nroCmp;
    }

    public String getEmision() {
        return this.emision;
    }

    public void setEmision(String emision) {
        this.emision = emision;
    }

    public String getVencimiento() {
        return this.vencimiento;
    }

    public void setVencimiento(String vencimiento) {
        this.vencimiento = vencimiento;
    }

    public String getCliente() {
        return this.cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getcCodCnx() {
        return this.cCodCnx;
    }

    public void setcCodCnx(String cCodCnx) {
        this.cCodCnx = cCodCnx;
    }

    public String getDistrito() {
        return this.distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getDocIdentidad() {
        return this.docIdentidad;
    }

    public void setDocIdentidad(String docIdentidad) {
        this.docIdentidad = docIdentidad;
    }

    public String getCodigoUnix() {
        return this.codigoUnix;
    }

    public void setCodigoUnix(String codigoUnix) {
        this.codigoUnix = codigoUnix;
    }

    public String getCodigoCat() {
        return this.codigoCat;
    }

    public void setCodigoCat(String codigoCat) {
        this.codigoCat = codigoCat;
    }

    public String getCategoria() {
        return this.categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getRutaReparto() {
        return this.rutaReparto;
    }

    public void setRutaReparto(String rutaReparto) {
        this.rutaReparto = rutaReparto;
    }

    public String getcNroCir() {
        return this.cNroCir;
    }

    public void setcNroCir(String cNroCir) {
        this.cNroCir = cNroCir;
    }

    public String getSecuencia() {
        return this.secuencia;
    }

    public void setSecuencia(String secuencia) {
        this.secuencia = secuencia;
    }

    public String getContinuidad() {
        return this.continuidad;
    }

    public void setContinuidad(String continuidad) {
        this.continuidad = continuidad;
    }

    public String getMedidor() {
        return this.medidor;
    }

    public void setMedidor(String medidor) {
        this.medidor = medidor;
    }

    public String getLecturaAnt() {
        return this.lecturaAnt;
    }

    public void setLecturaAnt(String lecturaAnt) {
        this.lecturaAnt = lecturaAnt;
    }

    public String getFechaLecAnt() {
        return this.fechaLecAnt;
    }

    public void setFechaLecAnt(String fechaLecAnt) {
        this.fechaLecAnt = fechaLecAnt;
    }

    public String getLecturaAct() {
        return this.lecturaAct;
    }

    public void setLecturaAct(String lecturaAct) {
        this.lecturaAct = lecturaAct;
    }

    public String getFechaLecAct() {
        return this.fechaLecAct;
    }

    public void setFechaLecAct(String fechaLecAct) {
        this.fechaLecAct = fechaLecAct;
    }

    public String getConsumoReal() {
        return this.consumoReal;
    }

    public void setConsumoReal(String consumoReal) {
        this.consumoReal = consumoReal;
    }

    public String getConsumo() {
        return this.consumo;
    }

    public void setConsumo(String consumo) {
        this.consumo = consumo;
    }

    public String getcValor() {
        return this.cValor;
    }

    public void setcValor(String cValor) {
        this.cValor = cValor;
    }

    public String getcNroCon() {
        return this.cNroCon;
    }

    public void setcNroCon(String cNroCon) {
        this.cNroCon = cNroCon;
    }

    public String getDesCon() {
        return this.desCon;
    }

    public void setDesCon(String desCon) {
        this.desCon = desCon;
    }

    public String getnCargo() {
        return this.nCargo;
    }

    public void setnCargo(String nCargo) {
        this.nCargo = nCargo;
    }

    public String getnSubTot() {
        return this.nSubTot;
    }

    public void setnSubTot(String nSubTot) {
        this.nSubTot = nSubTot;
    }

    public String getnTotal() {
        return this.nTotal;
    }

    public void setnTotal(String nTotal) {
        this.nTotal = nTotal;
    }

    public String getnMesDeu() {
        return this.nMesDeu;
    }

    public void setnMesDeu(String nMesDeu) {
        this.nMesDeu = nMesDeu;
    }

    public String getnTotDeu() {
        return this.nTotDeu;
    }

    public void setnTotDeu(String nTotDeu) {
        this.nTotDeu = nTotDeu;
    }

    public String getcMonLet() {
        return this.cMonLet;
    }

    public void setcMonLet(String cMonLet) {
        this.cMonLet = cMonLet;
    }

    public String getcAfeIgv() {
        return this.cAfeIgv;
    }

    public void setcAfeIgv(String cAfeIgv) {
        this.cAfeIgv = cAfeIgv;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.nroCmp);
        dest.writeString(this.emision);
        dest.writeString(this.vencimiento);
        dest.writeString(this.cliente);
        dest.writeString(this.direccion);
        dest.writeString(this.cCodCnx);
        dest.writeString(this.distrito);
        dest.writeString(this.docIdentidad);
        dest.writeString(this.codigoUnix);
        dest.writeString(this.codigoCat);
        dest.writeString(this.categoria);
        dest.writeString(this.rutaReparto);
        dest.writeString(this.cNroCir);
        dest.writeString(this.secuencia);
        dest.writeString(this.continuidad);
        dest.writeString(this.medidor);
        dest.writeString(this.lecturaAnt);
        dest.writeString(this.fechaLecAnt);
        dest.writeString(this.lecturaAct);
        dest.writeString(this.fechaLecAct);
        dest.writeString(this.consumoReal);
        dest.writeString(this.consumo);
        dest.writeString(this.cValor);
        dest.writeString(this.cNroCon);
        dest.writeString(this.desCon);
        dest.writeString(this.nCargo);
        dest.writeString(this.nSubTot);
        dest.writeString(this.nTotal);
        dest.writeString(this.nMesDeu);
        dest.writeString(this.nTotDeu);
        dest.writeString(this.cMonLet);
        dest.writeString(this.cAfeIgv);
    }
}
