package com.held_software.epsilo.util;

/**
 * Created by jose_ on 3/12/2019.
 */

public interface IRestResult {
    void onSuccess(int p_nCode, String p_cResponse);
    void onError(int p_nCode, String p_cError);
}
