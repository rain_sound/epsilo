package com.held_software.epsilo.util;

import android.os.AsyncTask;
import android.util.Log;
import android.util.Pair;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

/**
 * Created by manuel on 4/02/18.
 */

public class RestClient {
   private static String TAG = "RESTCLIENT";
   private String lcParams = null;
   private List<Pair<String, String>> laHeaders = null;
   private String lnCode = "0";
   private String lcUrl = null;
   /*private static class SingletonHolder{
       public static final RestClient INSTANCE = new RestClient();
   }
   public static RestClient getInstance(){
       return SingletonHolder.INSTANCE;
   }*/
   private IRestResult iRestResult;

    /*public void execute(String url, int code, IRestResult iRestResult){
        this.iRestResult = iRestResult;
        new RestGetSet().execute(new String[]{url, String.valueOf(code)});
    }

    public void execute(String url, String params, int code, IRestResult iRestResult){
        this.iRestResult = iRestResult;
        new RestGetSet().execute(new String[]{url, String.valueOf(code), params});
    }*/

   public RestClient setParams(String p_cParams) {
      this.lcParams = p_cParams;
      return this;
   }

   public RestClient setHeaders(List<Pair<String, String>> p_aHeaders) {
      this.laHeaders = p_aHeaders;
      return this;
   }

   public void execute(String p_cUrl, IRestResult iRestResult) {
      this.iRestResult = iRestResult;
      this.lcUrl = p_cUrl;
      new RestGetSet().execute(new String[]{});
   }

   private class RestGetSet extends AsyncTask<String, JSONObject, JSONObject>{

      @Override
      protected JSONObject doInBackground(String... params) {
         int code = Integer.parseInt(lnCode);
         try {
            URL urlService = new URL(lcUrl);
            HttpURLConnection service = (HttpURLConnection) urlService.openConnection();

            /*SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, null, new SecureRandom());
            service.setSSLSocketFactory(sc.getSocketFactory());*/

            service.setRequestMethod("POST");
            if (laHeaders != null) {
               Iterator<Pair<String, String>> it = laHeaders.iterator();
               while (it.hasNext()) {
                  Pair<String, String> laTmp = it.next();
                  service.setRequestProperty(laTmp.first, laTmp.second);
               }
            }
            if (lcParams != null) {
               service.setDoOutput(true);
               OutputStream outputStream = new BufferedOutputStream(service.getOutputStream());
               outputStream.write(lcParams.getBytes());
               outputStream.flush();
               outputStream.close();
            }
            BufferedReader br = null;
            StringBuilder sb = new StringBuilder();
            if (service.getResponseCode() == 200) {
               BufferedReader br2 = new BufferedReader(new InputStreamReader(service.getInputStream()));
               String line = br2.readLine();
               if(line != null){
                  sb.append(line);
               }
               br2.close();
               br = br2;
            }
            String stringBuilder = sb.toString();
            if(service != null){
               service.disconnect();
            }
            if(br == null){
               return null;
            }
            br.close();
            Map<String, Object> result = new HashMap<>();
            result.put("success", true);
            result.put("code", code);
            result.put("response", stringBuilder);
            JSONObject res = new JSONObject(result);
            //Log.e(TAG, res.toString());
            return res;
         } catch (Exception e) {
            e.printStackTrace();
            Map<String, Object> result = new HashMap<>();
            result.put("success", false);
            result.put("code", code);
            result.put("response", e.getMessage());
            JSONObject res = new JSONObject(result);
            return res;
         }
      }

      @Override
      protected void onPostExecute(JSONObject jsonObject) {
         //super.onPostExecute(jsonObject);
         if (jsonObject == null) {
            return;
         }
         try {
            if (jsonObject.getBoolean("success")){
               iRestResult.onSuccess(jsonObject.getInt("code"), jsonObject.getString("response"));
            }
            else{
               iRestResult.onError(jsonObject.getInt("code"), jsonObject.getString("response"));
            }
         } catch (JSONException e) {
            e.printStackTrace();
         }
      }
   }

   public static String getParams(Map<String, Object> jsonParams) {
      String params = "";
      boolean first = true;
      Iterator<String> iterator = jsonParams.keySet().iterator();
      while(iterator.hasNext()){
         String key = iterator.next();
         if (first) {
            first = false;
         }
         else {
            params += "&";
         }
         Object loObj = jsonParams.get(key);
         if (loObj instanceof String || loObj instanceof CharSequence) {
            params += key + "=" + loObj;
         } else if (loObj instanceof List) {
            String param = "";
            Iterator it = ((List) loObj).iterator();
            while (it.hasNext()) {
               String item = ((String)it.next());
               param += String.format("%s[]=%s%s", key, item, (it.hasNext() ? "&" : ""));
            }
            params += param;
         }
      }
      return params;
   }

   public RestClient addHeader(String p_cKey, String p_cValue) {
      if (this.laHeaders == null) {
         this.laHeaders = new ArrayList<>();
      }
      Pair<String, String> laTmp = new Pair<>(p_cKey, p_cValue);
      this.laHeaders.add(laTmp);
      return this;
   }
}