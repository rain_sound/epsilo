package com.held_software.epsilo.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.held_software.epsilo.entities.Conexiones;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jose_ on 3/12/2019.
 */

public class LocalDatabase {
    private static final String TAG = "LOCALDATABASE";
    private static final String DATABASE_NAME = "EPSILO";
    private static final int DATABASE_VERSION = 4;
    private static final String RECIBO_TABLE = "recibo";
    private static final String CONEXION_TABLE = "conexion";
    private static final String DATABASE_CREATE_CONEXION = String.format("CREATE TABLE %s(\n" +
            "nroConexion CHAR(7),\n" +
            "cnombre VARCHAR(240))", CONEXION_TABLE);
    private static final String DATABASE_CREATE_RECIBO = String.format("CREATE TABLE %s(\n" +
            "    NroCmp TEXT,\n" +
            "    emision TEXT,\n" +
            "    vencimiento TEXT,\n" +
            "    Cliente TEXT,\n" +
            "    Direccion TEXT,\n" +
            "    cCodCnx TEXT,\n" +
            "    Distrito TEXT,\n" +
            "    DocIdentidad TEXT,\n" +
            "    CodigoUnix TEXT,\n" +
            "    CodigoCat TEXT,\n" +
            "    Categoria TEXT,\n" +
            "    RutaReparto TEXT,\n" +
            "    cNroCir TEXT,\n" +
            "    Secuencia TEXT,\n" +
            "    Continuidad TEXT,\n" +
            "    Medidor TEXT,\n" +
            "    LecturaAnt TEXT,\n" +
            "    FechaLecAnt TEXT,\n" +
            "    LecturaAct TEXT,\n" +
            "    FechaLecAct TEXT,\n" +
            "    ConsumoReal TEXT,\n" +
            "    Consumo TEXT,\n" +
            "    cValor TEXT,\n" +
            "    cNroCon TEXT,\n" +
            "    DesCon TEXT,\n" +
            "    nCargo TEXT,\n" +
            "    nSubTot TEXT,\n" +
            "    nTotal TEXT,\n" +
            "    nMesDeu TEXT,\n" +
            "    nTotDeu TEXT,\n" +
            "    cMonLet TEXT,\n" +
            "    cAfeIgv TEXT\n" +
            ");", RECIBO_TABLE);

    //private Context context;
    public DatabaseHelper databaseHelper;
    private static SQLiteDatabase db;

    private static class SingletonHolder{
        public static final LocalDatabase INSTANCE = new LocalDatabase();
    }

    public static LocalDatabase getInstance(){
        return SingletonHolder.INSTANCE;
    }

    private LocalDatabase(){ }

    public void setContext(Context context){
        //this.context = context;
        this.databaseHelper = new DatabaseHelper(context);
    }

    private boolean open(){
        try {
            db = databaseHelper.getWritableDatabase();
            return true;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    private void close(){
        databaseHelper.close();
    }

    public void insertReceipt(ConceptReceipt receipt){
        open();
        long count = DatabaseUtils.queryNumEntries(
                db,
                RECIBO_TABLE,
                "NroCmp = ? and cNroCon = ?",
                new String[]{receipt.getNroCmp(),receipt.getcNroCon()}
        );
        close();
        Log.e(TAG, "insertReceipt count = " + count);
        if(count != 0){
            return;
        }
        ContentValues values = new ContentValues();
        values.put("NroCmp", receipt.getNroCmp());
        values.put("emision", receipt.getEmision());
        values.put("vencimiento", receipt.getVencimiento());
        values.put("Cliente", receipt.getCliente());
        values.put("Direccion", receipt.getDireccion());
        values.put("cCodCnx", receipt.getcCodCnx());
        values.put("Distrito", receipt.getDistrito());
        values.put("DocIdentidad", receipt.getDocIdentidad());
        values.put("CodigoUnix", receipt.getCodigoUnix());
        values.put("CodigoCat", receipt.getCodigoCat());
        values.put("Categoria", receipt.getCategoria());
        values.put("RutaReparto", receipt.getRutaReparto());
        values.put("cNroCir", receipt.getcNroCir());
        values.put("Secuencia", receipt.getSecuencia());
        values.put("Continuidad", receipt.getContinuidad());
        values.put("Medidor", receipt.getMedidor());
        values.put("LecturaAnt", receipt.getLecturaAnt());
        values.put("FechaLecAnt", receipt.getFechaLecAnt());
        values.put("LecturaAct", receipt.getLecturaAct());
        values.put("FechaLecAct", receipt.getFechaLecAct());
        values.put("ConsumoReal", receipt.getConsumoReal());
        values.put("Consumo", receipt.getConsumo());
        values.put("cValor", receipt.getcValor());
        values.put("cNroCon", receipt.getcNroCon());
        values.put("DesCon", receipt.getDesCon());
        values.put("nCargo", receipt.getnCargo());
        values.put("nSubTot", receipt.getnSubTot());
        values.put("nTotal", receipt.getnTotal());
        values.put("nMesDeu", receipt.getnMesDeu());
        values.put("nTotDeu", receipt.getnTotal());
        values.put("cMonLet", receipt.getcMonLet());
        values.put("cAfeIgv", receipt.getcAfeIgv());
        open();
        db.insert(RECIBO_TABLE, null, values);
        close();
    }

    public void deleteConexion(Conexiones conexion) {
        open();
        db.delete(CONEXION_TABLE, "nroConexion = ?", new String[]{
                conexion.getConexion()
        });
        close();
    }

    public void insertConexiones(Conexiones conexion){
        open();
        long count = DatabaseUtils.queryNumEntries(
                db,
                CONEXION_TABLE,
                "nroConexion = ?",
                new String[]{conexion.getConexion()}
        );
        close();
        if(count != 0){
            return;
        }
        ContentValues values = new ContentValues();
        values.put("cnombre", conexion.getNombreCliente());
        values.put("nroConexion", conexion.getConexion());
        open();
        db.insert(CONEXION_TABLE, null, values);
        close();
    }

    public List<Conexiones> getConexiones(){
        List<Conexiones> conexiones = new ArrayList<>();
        open();
        Cursor cursor = db.query(CONEXION_TABLE, new String[]{
                        "nroConexion",
                        "cnombre"
                },
                null,
                null,
                null,
                null,
                null,
                null
        );
        if(cursor != null) {
            if (!cursor.moveToFirst()) {
                close();
                return conexiones;
            }
            do {
                Conexiones tmp = new Conexiones(cursor.getString(0), cursor.getString(1));
                conexiones.add(tmp);
            } while (cursor.moveToNext());
            close();
        }
        return conexiones;
    }

    public List<ConceptReceipt> getReceipts(){
        List<ConceptReceipt> receipts = new ArrayList<>();
        open();
        Cursor cursor = db.query(RECIBO_TABLE,
                new String[]{
                        "NroCmp",
                        "emision",
                        "vencimiento",
                        "Cliente",
                        "Direccion",
                        "cCodCnx",
                        "Distrito",
                        "DocIdentidad",
                        "CodigoUnix",
                        "CodigoCat",
                        "Categoria",
                        "RutaReparto",
                        "cNroCir",
                        "Secuencia",
                        "Continuidad",
                        "Medidor",
                        "LecturaAnt",
                        "FechaLecAnt",
                        "LecturaAct",
                        "FechaLecAct",
                        "ConsumoReal",
                        "Consumo",
                        "cValor",
                        "cNroCon",
                        "DesCon",
                        "nCargo",
                        "nSubTot",
                        "nTotal",
                        "nMesDeu",
                        "nTotDeu",
                        "cMonLet",
                        "cAfeIgv"
                },
                null,
                null,
                null,
                null,
                null,
                null
        );
        if(cursor != null){
            if(!cursor.moveToFirst()){
                return receipts;
            }
            do{
                ConceptReceipt tmp = new ConceptReceipt();
                tmp.setNroCmp(cursor.getString(0));
                tmp.setEmision(cursor.getString(1));
                tmp.setVencimiento(cursor.getString(2));
                tmp.setCliente(cursor.getString(3));
                tmp.setDireccion(cursor.getString(4));
                tmp.setcCodCnx(cursor.getString(5));
                tmp.setDistrito(cursor.getString(6));
                tmp.setDocIdentidad(cursor.getString(7));
                tmp.setCodigoUnix(cursor.getString(8));
                tmp.setCodigoCat(cursor.getString(9));
                tmp.setCategoria(cursor.getString(10));
                tmp.setRutaReparto(cursor.getString(11));
                tmp.setcNroCir(cursor.getString(12));
                tmp.setSecuencia(cursor.getString(13));
                tmp.setContinuidad(cursor.getString(14));
                tmp.setMedidor(cursor.getString(15));
                tmp.setLecturaAnt(cursor.getString(16));
                tmp.setFechaLecAnt(cursor.getString(17));
                tmp.setLecturaAct(cursor.getString(18));
                tmp.setFechaLecAct(cursor.getString(19));
                tmp.setConsumoReal(cursor.getString(20));
                tmp.setConsumo(cursor.getString(21));
                tmp.setcValor(cursor.getString(22));
                tmp.setcNroCon(cursor.getString(23));
                tmp.setDesCon(cursor.getString(24));
                tmp.setnCargo(cursor.getString(25));
                tmp.setnSubTot(cursor.getString(26));
                tmp.setnTotal(cursor.getString(27));
                tmp.setnMesDeu(cursor.getString(28));
                tmp.setnTotDeu(cursor.getString(29));
                tmp.setcMonLet(cursor.getString(30));
                tmp.setcAfeIgv(cursor.getString(31));
                receipts.add(tmp);
                Log.e(TAG, tmp.toString());
            } while (cursor.moveToNext());
            close();
        }
        return receipts;
    }

    private static class DatabaseHelper extends SQLiteOpenHelper{

        public DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            try{
                sqLiteDatabase.execSQL(DATABASE_CREATE_RECIBO);
                sqLiteDatabase.execSQL(DATABASE_CREATE_CONEXION);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
            sqLiteDatabase.execSQL(String.format("DROP TABLE IF EXISTS %s", RECIBO_TABLE));
            sqLiteDatabase.execSQL(String.format("DROP TABLE IF EXISTS %s", CONEXION_TABLE));
            onCreate(sqLiteDatabase);
        }
    }
}

