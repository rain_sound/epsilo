package com.held_software.epsilo;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.TypedValue;

import com.held_software.epsilo.util.LocalDatabase;

/**
 * Created by jose_ on 3/12/2019.
 */

public class EpsApplication extends Application {
   private static final String PREFERENCE_KEY = "com.held_software.epsilo";
   private static final String CONEXION_KEY = "conexion";
   private static final String TOKEN_KEY = "token";
   private static final String NOMBRE_KEY = "nombre";
   private static SharedPreferences sharedPreferences;
   private static Context context;

   @Override
   public void onCreate() {
      super.onCreate();
      context = getApplicationContext();
      sharedPreferences = getSharedPreferences(PREFERENCE_KEY, MODE_PRIVATE);
      LocalDatabase.getInstance().setContext(this);
   }

   public static String getConexion(){
   return sharedPreferences.getString(CONEXION_KEY, null);
   }

   public static void saveConexion(String conexion){
      sharedPreferences.edit()
      .putString(CONEXION_KEY, conexion)
      .apply();
   }

   public static String getToken() {
      return sharedPreferences.getString(TOKEN_KEY, null);
   }

   public static void saveToken(String p_cToken) {
      sharedPreferences.edit()
              .putString(TOKEN_KEY, p_cToken)
              .apply();
   }

   public static void clear(){
   sharedPreferences.edit().clear().apply();
   }

   public static void saveNombre(String nombre){
      sharedPreferences.edit()
      .putString(NOMBRE_KEY, nombre)
      .apply();
   }

   public static boolean isNetworkAvailable(Context context) {
      ConnectivityManager connectivityManager
      = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
      NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
      return activeNetworkInfo != null && activeNetworkInfo.isConnected();
   }

   public static String getNombre(){
      return sharedPreferences.getString(NOMBRE_KEY, null);
   }

   public static float dp_to_px(float dp) {
      Resources r = context.getResources();
      float px = TypedValue.applyDimension(
              TypedValue.COMPLEX_UNIT_DIP,
              dp,
              r.getDisplayMetrics()
      );
      return px;
   }
}