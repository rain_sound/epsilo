package com.held_software.epsilo.clases;

import android.util.Log;
import android.util.Pair;

import com.held_software.epsilo.entities.Bill;
import com.held_software.epsilo.entities.Recibo;
import com.held_software.epsilo.fragments.recibo.ReciboFragment;
import com.held_software.epsilo.util.IRestResult;
import com.held_software.epsilo.util.RestClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CRecibo extends CBase {
   private static final int CODE_INVOICE = 0;
   private static final String TAG = "CRECIBO";
   private static final String CLASS_URL = BASE_URL + "clients/";

   public boolean omRecuperarRecibo() {
      boolean llOk = this.mxValParamRecuperarRecibo();
      if (!llOk) {
         return false;
      }
      llOk = this.mxRecuperarRecibo();
      return llOk;
   }

   private boolean mxValParamRecuperarRecibo() {
      if (!this.paData.containsKey("TOKEN") || this.paData.get("TOKEN") == null) {
         this.pcError = "TOKEN NO DEFINIDO";
         return false;
      }
      return true;
   }

   private boolean mxRecuperarRecibo() {
      RestClient loRest = new RestClient();
      loRest
        .setParams(RestClient.getParams(this.paData))
        .addHeader("access-token", (String)paData.get("TOKEN"))
        .execute(CLASS_URL + "getInvoice", new IRestResult() {
         @Override
         public void onSuccess(int p_nCode, String p_cResponse) {
            Log.e(TAG, p_cResponse);
            try {
               JSONObject loJson = new JSONObject(p_cResponse);
               if (loJson.getBoolean("error")) {
                  ((ReciboFragment)CRecibo.this.iViewActions).onError(loJson.getString("message"));
               } else {
                  Recibo loRecibo = new Recibo();
                  JSONArray laData = loJson.getJSONArray("data");
                  if (laData.length() == 0) {
                     onError(CODE_INVOICE, "SIN INFORMACION DEL RECIBO");
                     return;
                  }
                  loJson = laData.getJSONObject(0);
                  JSONObject meter = loJson.getJSONArray("meters").getJSONObject(0);
                  JSONArray billsJSON = meter.getJSONArray("bills");

                  loRecibo.setNombres(loJson.getString("c_name") + loJson.getString("c_lastname"));
                  //loRecibo.setDireccion(loJson.getString("c_direccion"));
                  loRecibo.setConexion(meter.getString("c_conexion"));
                  loRecibo.setCatastro(meter.getString("m_catastro_code"));
                  loRecibo.setDireccion(meter.getString("m_address"));
                  List<Bill> bills = new ArrayList<>();
                  for (int i = 0; i < billsJSON.length(); i++) {
                     JSONObject laFila = billsJSON.getJSONObject(i);
                     Bill tmp = new Bill();
                     tmp.setReading(laFila.getString("r_reading"));
                     tmp.setFechaExpiracion(laFila.getString("r_date_expi"));
                     bills.add(tmp);
                  }
                  loRecibo.setBills(bills);

                  ((ReciboFragment)CRecibo.this.iViewActions).onReciboSuccess(loRecibo);
               }
            } catch (JSONException e) {
               e.printStackTrace();
               ((ReciboFragment)CRecibo.this.iViewActions).onError("RESPUESTA INCORRECTA DEL SERVIDOR");
            }
         }

         @Override
         public void onError(int p_nCode, String p_cError) {
            ((ReciboFragment)CRecibo.this.iViewActions).onError(p_cError);
         }
      });
      return true;
   }
}
