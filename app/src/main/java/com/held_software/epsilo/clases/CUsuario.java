package com.held_software.epsilo.clases;

import android.util.Log;

import com.held_software.epsilo.EpsApplication;
import com.held_software.epsilo.activities.Login.ILoginView;
import com.held_software.epsilo.entities.Consumo;
import com.held_software.epsilo.fragments.consumos.IConsumosFragment;
import com.held_software.epsilo.util.IRestResult;
import com.held_software.epsilo.util.RestClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CUsuario extends CBase {
   private static final String TAG = "CUsuario";
   private static final String CLASS_URL = CBase.BASE_URL + "clients/";

   private static final int CODE_INICIAR = 0;

   public boolean omIniciarSesion() {
      boolean llOk = this.mxValParamIniciarSesion();
      if (!llOk) {
         return false;
      }
      llOk = this.mxIniciarSesion();
      return llOk;
   }

   protected boolean mxValParamIniciarSesion() {
      if (!this.paData.containsKey("CONEXION")) {
         this.pcError = "CONEXION NO DEFINIDA";
         return false;
      }
      if (!this.paData.containsKey("DNI")) {
         this.pcError = "CONEXION NO DEFINIDA";
         return false;
      }
      if (!this.paData.containsKey("CLAVE")) {
         this.pcError = "CONEXION NO DEFINIDA";
         return false;
      }
      return true;
   }

   protected boolean mxIniciarSesion() {
      RestClient loRest = new RestClient();
      loRest
        .setParams(RestClient.getParams(this.paData))
        .execute(CLASS_URL + "signin" , new IRestResult() {
         @Override
         public void onSuccess(int code, String response) {
            if (CUsuario.this.iViewActions instanceof ILoginView) {
               try {
                  JSONObject loJson = new JSONObject(response);
                  if (loJson.getBoolean("error")) {
                     ((ILoginView) CUsuario.this.iViewActions).onLoginFailed(loJson.getString("message"));
                  } else {
                     JSONObject loData = loJson.getJSONObject("data");
                     EpsApplication.saveToken(loData.getString("accessToken"));
                     EpsApplication.saveNombre(loData.getString("name"));
                     EpsApplication.saveConexion((String)paData.get("CONEXION"));
                     ((ILoginView) CUsuario.this.iViewActions).onLoginSuccess();
                  }
               } catch (JSONException e) {
                  e.printStackTrace();
                  ((ILoginView) CUsuario.this.iViewActions).onLoginFailed("RESPUESTA DEL SERVIDOR INCORRECTA");
               }
            }
         }

         @Override
         public void onError(int code, String error) {
            if (CUsuario.this.iViewActions instanceof ILoginView) {
               ((ILoginView) CUsuario.this.iViewActions).onLoginFailed(error);
            }
         }
      });
      return true;
   }

   public boolean omRecuperarConsumos() {
      boolean llOk = this.mxValParamRecuperarConsumos();
      if (!llOk) {
         return false;
      }
      llOk = this.mxRecuperarConsumos();
      return llOk;
   }

   protected boolean mxValParamRecuperarConsumos() {
      if (!this.paData.containsKey("TOKEN") || this.paData.get("TOKEN") == null) {
         this.pcError = "TOKEN NO DEFINIDO";
         return false;
      }
      return true;
   }

   private boolean mxRecuperarConsumos() {
      final RestClient loRest = new RestClient();
      loRest
        .setParams(RestClient.getParams(this.paData))
        .addHeader("access-token", (String) this.paData.get("TOKEN"))
        .execute(CLASS_URL + "getConsumos",  new IRestResult() {
         @Override
         public void onSuccess(int p_nCode, String p_cResponse) {
            Log.e(TAG, p_cResponse);
            List<Consumo> laDatos = new ArrayList<>();
            try {
               JSONObject loJson = new JSONObject(p_cResponse);
               if (loJson.getBoolean("error")) {
                  ((IConsumosFragment)CUsuario.this.iViewActions).showError(loJson.getString("message"));
                  return;
               }
               JSONArray laJson = loJson.getJSONArray("data");
               for (int i = laJson.length() - 1; i >= 0; i--) {
                  Consumo loTmp = new Consumo();
                  JSONObject laFila = laJson.getJSONObject(i);
                  loTmp.setVencimiento(laFila.getString("r_date_expi"));
                  loTmp.setConsumo(laFila.getString("r_reading"));
                  laDatos.add(loTmp);
               }
               ((IConsumosFragment)CUsuario.this.iViewActions).onConsumosLoaded(laDatos);
            } catch (JSONException e) {
               e.printStackTrace();
               ((IConsumosFragment)CUsuario.this.iViewActions).showError("RESPUESTA DEL SERVIDOR INCORRECTA");
            }
         }

         @Override
         public void onError(int p_nCode, String p_cError) {
            ((IConsumosFragment)CUsuario.this.iViewActions).showError(p_cError);
         }
      });
      return true;
   }
}
