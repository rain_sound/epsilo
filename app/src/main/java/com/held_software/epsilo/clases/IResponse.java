package com.held_software.epsilo.clases;

import java.util.Map;

public interface IResponse {
    void onSuccces(Map<String, Object> p_aResponse);
    void onError(String p_cError);
}
