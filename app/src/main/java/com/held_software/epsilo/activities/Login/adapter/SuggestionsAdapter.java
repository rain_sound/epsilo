package com.held_software.epsilo.activities.Login.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import androidx.recyclerview.widget.RecyclerView;

import com.held_software.epsilo.R;
import com.held_software.epsilo.entities.Conexiones;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jose_ on 3/12/2019.
 */
public class SuggestionsAdapter extends RecyclerView.Adapter<SuggestionsAdapter.ViewHolder>{
    private List<Conexiones> suggestionList = new ArrayList<>();
    private ConexionSuggestClick conexionSuggestClick;

    public SuggestionsAdapter(ConexionSuggestClick conexionSuggestClick) {
        this.conexionSuggestClick = conexionSuggestClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.suggestion_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.txtSuggest.setText(suggestionList.get(position).getConexion());
        holder.setOnClick(suggestionList.get(position), conexionSuggestClick);
    }

    @Override
    public int getItemCount() {
        return suggestionList.size();
    }

    public void updateSuggestionList(List<Conexiones> suggestionList){
        this.suggestionList = suggestionList;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtSuggest;
        public ViewHolder(View itemView) {
            super(itemView);
            bind();
        }

        private void bind() {
            txtSuggest = itemView.findViewById(R.id.txtSuggest);
        }

        public void setOnClick(final Conexiones conexion, final ConexionSuggestClick conexionSuggestClick){
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    conexionSuggestClick.onClickSuggest(conexion);
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    conexionSuggestClick.onLongCkickSuggest(conexion);
                    return false;
                }
            });
        }
    }
}
