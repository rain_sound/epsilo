package com.held_software.epsilo.activities.Login;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Intent;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.snackbar.Snackbar;
import com.held_software.epsilo.EpsApplication;
import com.held_software.epsilo.R;
import com.held_software.epsilo.activities.Main.MainActivity;
import com.held_software.epsilo.activities.Register.RegisterActivity;
import com.held_software.epsilo.clases.CUsuario;
import com.held_software.epsilo.marquee.MarqueeAdapter;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by jose_ on 3/12/2019.
 */

public class LoginActivity extends AppCompatActivity implements ILoginView, View.OnClickListener {
   private static final String TAG = "LOGINACTIVITY";
   private EditText txtDNI;
   private EditText txtConexion;
   private EditText txtClave;
   private FrameLayout btnAceptar;
   //private Button btnAceptar;
   private ProgressBar progressbar;
   private TextView txtBtnAceptar;
   private FrameLayout btnRegistrar;

   private MarqueeAdapter adapterMarquee;
   private Timer timerMarquee;
   private ViewPager viewPagerMarquee;
   private View main_view;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_login_constraint);

      if (EpsApplication.getConexion() != null) {
         onLoginSuccess();
      }

      bind();
      setupMarquee();
   }

   private void bind() {
      txtDNI = this.findViewById(R.id.txtDNI);
      txtClave = this.findViewById(R.id.txtClave);
      txtConexion = findViewById(R.id.txtConexion);
      main_view = findViewById(R.id.main_view);
      btnAceptar = findViewById(R.id.btnAceptar);
      btnAceptar.setOnClickListener(this);
      viewPagerMarquee = findViewById(R.id.viewPagerMarquee);
      progressbar = findViewById(R.id.progressbarLogin);
      txtBtnAceptar = findViewById(R.id.txtAceptar);

      //reveal = findViewById(R.id.reveal);
      btnRegistrar = findViewById(R.id.btnRegistrar);
      btnRegistrar.setOnClickListener(this);
   }

   @Override
   public void onClick(View view) {
      switch (view.getId()) {
         case R.id.btnAceptar:
            Map<String, Object> laData = new HashMap<>();
            laData.put("DNI", txtDNI.getText().toString());
            laData.put("CLAVE", txtClave.getText().toString());
            laData.put("CONEXION", txtConexion.getText().toString());

            CUsuario lo = new CUsuario();
            lo.iViewActions = this;
            lo.paData = laData;

            Log.e(TAG, laData.toString());

            disableIniciarButton();
            boolean llOk = lo.omIniciarSesion();
            if (!llOk) {
               enableIniciarButton();
               Snackbar.make(main_view, lo.pcError, Snackbar.LENGTH_SHORT).show();
            }
            break;
         case R.id.btnRegistrar:
            Intent intent = new Intent(this, RegisterActivity.class);
            startActivity(intent);
            break;
      }
   }

   private void setupMarquee() {
      adapterMarquee = new MarqueeAdapter(MarqueeAdapter.STYLE_MARQUEE);
      viewPagerMarquee.setAdapter(adapterMarquee);

      adapterMarquee.addItem("VISUALIZA TU RECIBO", getLayoutInflater());
      adapterMarquee.addItem("REVISA TUS CONSUMOS", getLayoutInflater());
      adapterMarquee.addItem("PAGA TU RECIBO", getLayoutInflater());
      adapterMarquee.addItem("REALIZA TU RECLAMO", getLayoutInflater());
      adapterMarquee.addItem("ALERTAS DE CORTES", getLayoutInflater());

      final Runnable mUpdateResults = new Runnable() {
         public void run() {
            animateMarquee();
         }
      };
      viewPagerMarquee.setCurrentItem(0, true);
      int delay = 1400;
      int period = 8000;
      final Handler mHandler = new Handler();
      timerMarquee = new Timer();
      timerMarquee.scheduleAtFixedRate(new TimerTask() {
         @Override
         public void run() {
            mHandler.post(mUpdateResults);
         }
      }, delay, period);
   }


   private void animateMarquee() {
      int currentItem = viewPagerMarquee.getCurrentItem() + 1;
      if(currentItem >= viewPagerMarquee.getAdapter().getCount()){
         currentItem = 0;
      }
      viewPagerMarquee.setCurrentItem(currentItem, true);
   }

   @Override
   public void onLoginSuccess() {
      Intent loMainActivity = new Intent(getBaseContext(),   MainActivity.class);
      loMainActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
      startActivity(loMainActivity);
   }

   @Override
   public void onLoginFailed(String p_cError) {
      enableIniciarButton();
      Snackbar.make(main_view, p_cError, Snackbar.LENGTH_SHORT).show();
   }

   @Override
   public void disableIniciarButton() {
      //animateButtonWidth(false);
      fadeOutTextAndShowProgress();
   }

   @Override
   public void enableIniciarButton() {
      //animateButtonWidth(true);
      fadeInTextAndHideProgress();
   }

   private void animateButtonWidth(boolean p_lEnable) {
      ValueAnimator loAnimator;
      if (p_lEnable) {
         loAnimator = ValueAnimator.ofInt(btnAceptar.getMeasuredWidth(), convertDpToPixel(300));
      } else {
         loAnimator = ValueAnimator.ofInt(btnAceptar.getMeasuredWidth(), convertDpToPixel(56));
      }
      loAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
         @Override
         public void onAnimationUpdate(ValueAnimator valueAnimator) {
            int lnValue = (Integer) valueAnimator.getAnimatedValue();
            ViewGroup.LayoutParams loParams = btnAceptar.getLayoutParams();
            loParams.width = lnValue;
            btnAceptar.requestLayout();
         }
      });
      loAnimator.setDuration(250).start();
   }

   private void fadeOutTextAndShowProgress() {
      txtBtnAceptar.animate().alpha(0.0f)
              .setDuration(250)
              .setListener(new AnimatorListenerAdapter() {
                 @Override
                 public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    progressbar.getIndeterminateDrawable()
                            .setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
                    progressbar.setVisibility(View.VISIBLE);
                 }
              });
   }

   private void fadeInTextAndHideProgress() {
      txtBtnAceptar.animate().alpha(1f)
              .setDuration(250)
              .setListener(new AnimatorListenerAdapter() {
                 @Override
                 public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    progressbar.getIndeterminateDrawable()
                            .setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_IN);
                    progressbar.setVisibility(View.INVISIBLE);
                 }
              });
   }

   private int convertDpToPixel(int p_nDP) {
      DisplayMetrics loMetrics = Resources.getSystem().getDisplayMetrics();
      float lnPx = p_nDP * (loMetrics.densityDpi / 160f);
      return Math.round(lnPx);
   }
}
