package com.held_software.epsilo.activities.Perfil;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;
import com.held_software.epsilo.EpsApplication;
import com.held_software.epsilo.R;
import com.held_software.epsilo.activities.Main.adapters.AsociadoAdapter;
import com.held_software.epsilo.entities.Asociado;

public class PerfilActivity extends AppCompatActivity implements View.OnClickListener {
   private TextView txtNombre;
   private RecyclerView recyclerviewAsociados;
   private AsociadoAdapter asociadoAdapter;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_perfil);
      setTitle("Mi perfil");
      mxBind();
      setup();
   }

   private void setup() {
      txtNombre.setText(EpsApplication.getNombre());
      asociadoAdapter = new AsociadoAdapter();
      recyclerviewAsociados.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
      recyclerviewAsociados.setAdapter(asociadoAdapter);
   }

   private void mxBind() {
      txtNombre = findViewById(R.id.txtNombre);
      recyclerviewAsociados = findViewById(R.id.recyclerviewAsociados);
      findViewById(R.id.btnInfoAsociados).setOnClickListener(this);
      findViewById(R.id.btnAddAsociado).setOnClickListener(this);
   }

   @Override
   public void onClick(View view) {
      if (view.getId() == R.id.btnInfoAsociados) {
         new AlertDialog.Builder(this)
                 .setTitle("Usuario asociados")
                 .setMessage("Los usuarios asociados son personas externas que pueden ver información de la conexión")
                 .show();
      } else if (view.getId() == R.id.btnAddAsociado) {
         new AlertDialog.Builder(this).setTitle("Agregar asociado").setView(R.layout.dialog_asociado).setPositiveButton("Agregar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
               TextInputEditText txtDNI = ((AlertDialog)dialogInterface).findViewById(R.id.txtDNI);

               String lcNroDni = txtDNI.getText().toString();

               Asociado asociado = new Asociado();
               asociado.setDni(lcNroDni);
               asociadoAdapter.addAsociado(asociado);

               onSuccessAsociacion();
            }
         })
         .show();
      }
   }

   private void onSuccessAsociacion() {
      new AlertDialog.Builder(PerfilActivity.this)
           .setMessage("Asociación realizada con éxito")
           .setNeutralButton("Cerrar", new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialogInterface, int i) {
                 dialogInterface.dismiss();
              }
           })
           .show();
   }
}
