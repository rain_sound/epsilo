package com.held_software.epsilo.activities.Medidor;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.held_software.epsilo.R;
import com.held_software.epsilo.util.IRestResult;
import com.held_software.epsilo.util.RestClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.held_software.epsilo.clases.CBase.BASE_URL;

public class MedidorActvity extends AppCompatActivity implements View.OnClickListener, IRestResult {
   private static final String TAG = "MEDIDORACTIVITY";
   private View main_view;
   private TextInputEditText txtMedida;
   private TextInputEditText txtConexion;
   private Button btnMedir;
   private CardView cardData;
   private TextView txtNombre;
   private TextView txtDireccion;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_medidor);
      setTitle("Toma de lectura");
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      mxBind();
   }

   private void mxBind() {
      main_view = findViewById(R.id.main_view);
      txtConexion = findViewById(R.id.txtConexion);
      txtConexion.addTextChangedListener(new ConexionKeyListener());
      txtMedida= findViewById(R.id.txtMedida);
      btnMedir = findViewById(R.id.btnMedir);
      cardData = findViewById(R.id.cardData);
      txtNombre = findViewById(R.id.txtNombre);
      txtDireccion = findViewById(R.id.txtDireccion);

      btnMedir.setOnClickListener(this);
   }

   @Override
   public void onClick(View v) {
      if (v.getId() == R.id.btnMedir) {
         mxMedir();
      }
   }

   private void mxMedir() {
      String lcIdConexion = txtConexion.getText().toString();
      if (lcIdConexion.length() != 6) {
         Snackbar.make(main_view, "CONEXION NO VALIDA", Snackbar.LENGTH_SHORT).show();
         return;
      }
      int lnMedida = Integer.parseInt(txtMedida.getText().toString());
      if (lnMedida < 1) {
         Snackbar.make(main_view, "MEDIDA NO VALIDA", Snackbar.LENGTH_SHORT).show();
         return;
      }
      InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
      View view = getCurrentFocus();
      imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
      btnMedir.setEnabled(false);

      Map<String, Object> laData = new HashMap<>();
      laData.put("r_reading", String.valueOf(lnMedida));
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
      laData.put("r_date_issue", sdf.format(new Date()));
      laData.put("meterId", lcIdConexion);
      laData.put("services[]", "1");
      RestClient restClient = new RestClient();
      restClient.setParams(RestClient.getParams(laData))
              .execute(BASE_URL + "services/new/bill", this);
   }

   @Override
   public void onSuccess(int p_nCode, String p_cResponse) {
      txtConexion.requestFocus();
      btnMedir.setEnabled(true);
      try {
         JSONObject jsonObject = new JSONObject(p_cResponse);
         if (jsonObject.getBoolean("error")) {
            Snackbar.make(main_view, jsonObject.getString("message"), Snackbar.LENGTH_SHORT).show();
         } else {
            txtMedida.setText("");
            txtConexion.setText("");
            Snackbar.make(main_view, "GUARDADO CORRECTAMENTE", Snackbar.LENGTH_SHORT).show();
         }
      } catch (JSONException e) {
         e.printStackTrace();
      }
   }

   @Override
   public void onError(int p_nCode, String p_cError) {
      btnMedir.setEnabled(true);
      Snackbar.make(main_view, p_cError, Snackbar.LENGTH_SHORT).show();
   }

   private class ConexionKeyListener implements TextWatcher, IRestResult {

      @Override
      public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void onTextChanged(CharSequence p_cText, int i, int i1, int i2) {
         if (p_cText.length() == 6) {
            RestClient loClient = new RestClient();

            Map<String, Object> laData = new HashMap<>();
            laData.put("c_conexion", p_cText);

            loClient.setParams(RestClient.getParams(laData));
            loClient.execute(BASE_URL + "clients/getDatosConexion", this);
         } else {
            cardData.setVisibility(View.GONE);
         }
      }

      @Override
      public void afterTextChanged(Editable editable) {
      }

      @Override
      public void onSuccess(int p_nCode, String p_cResponse) {
         try {
            JSONObject loJson = new JSONObject(p_cResponse);
            JSONObject laData = loJson.getJSONObject("data");
            JSONObject laMeter = laData.getJSONArray("meters").getJSONObject(0);
            txtNombre.setText(String.format("Nombres: %s %s", laData.getString("c_lastname"), laData.getString("c_name")));
            String lcAddress = laMeter.getString("m_address");
            lcAddress = (lcAddress == "null" ? "" : lcAddress);
            txtDireccion.setText(String.format("Dirección: %s", lcAddress));
            cardData.setVisibility(View.VISIBLE);
         } catch (JSONException e) {
            e.printStackTrace();
         }
      }

      @Override
      public void onError(int p_nCode, String p_cError) {
         Snackbar.make(main_view, p_cError, Snackbar.LENGTH_SHORT).show();
      }
   }
}
