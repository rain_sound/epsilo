package com.held_software.epsilo.activities.Main.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.held_software.epsilo.R;
import com.held_software.epsilo.entities.Asociado;

import java.util.ArrayList;
import java.util.List;

public class AsociadoAdapter extends RecyclerView.Adapter<AsociadoAdapter.ViewHolder> {
   private List<Asociado> asociadoList = new ArrayList<>();

   @Override
   public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.asociado_item, parent, false);
      return new ViewHolder(view);
   }

   @Override
   public void onBindViewHolder(ViewHolder holder, int position) {
      holder.txtDNI.setText(asociadoList.get(position).getDni());
   }

   @Override
   public int getItemCount() {
      return asociadoList.size();
   }

   public class ViewHolder extends RecyclerView.ViewHolder {
      private TextView txtDNI;
      public ViewHolder(View itemView) {
         super(itemView);
         mxBind();
      }

      private void mxBind() {
         txtDNI = itemView.findViewById(R.id.txtDNI);
      }
   }

   public void addAsociado(Asociado p_oAsociado) {
      asociadoList.add(p_oAsociado);
      notifyDataSetChanged();
   }
}
