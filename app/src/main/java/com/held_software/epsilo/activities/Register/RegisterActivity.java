package com.held_software.epsilo.activities.Register;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.held_software.epsilo.R;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
   private Toolbar toolbar;
   private FrameLayout btnRegistrar;
   private TextView txtRegistrar;
   private ProgressBar progressBar;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_register);
      toolbar = findViewById(R.id.toolbar);
      setSupportActionBar(toolbar);
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      setTitle("Registrarse");
      mxBind();
   }

   private void mxBind() {
      btnRegistrar = findViewById(R.id.btnRegistrar);
      txtRegistrar = findViewById(R.id.txtRegistrar);
      progressBar = findViewById(R.id.progressbar);

      btnRegistrar.setOnClickListener(this);
   }

   public void disableIniciarButton() {
      animateButtonWidth(false);
      fadeOutTextAndShowProgress();
   }

   private void animateButtonWidth(boolean p_lEnable) {
      ValueAnimator loAnimator;
      if (p_lEnable) {
         loAnimator = ValueAnimator.ofInt(btnRegistrar.getMeasuredWidth(), convertDpToPixel(300));
      } else {
         loAnimator = ValueAnimator.ofInt(btnRegistrar.getMeasuredWidth(), convertDpToPixel(56));
      }
      loAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
         @Override
         public void onAnimationUpdate(ValueAnimator valueAnimator) {
            int lnValue = (Integer) valueAnimator.getAnimatedValue();
            ViewGroup.LayoutParams loParams = btnRegistrar.getLayoutParams();
            loParams.width = lnValue;
            btnRegistrar.requestLayout();
         }
      });
      loAnimator.setDuration(250).start();
   }

   private void fadeOutTextAndShowProgress() {
      txtRegistrar.animate().alpha(0.0f)
              .setDuration(250)
              .setListener(new AnimatorListenerAdapter() {
                 @Override
                 public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    progressBar.getIndeterminateDrawable()
                            .setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
                    progressBar.setVisibility(View.VISIBLE);
                 }
              });
   }

   private void fadeInTextAndHideProgress() {
      txtRegistrar.animate().alpha(1f)
              .setDuration(250)
              .setListener(new AnimatorListenerAdapter() {
                 @Override
                 public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    progressBar.getIndeterminateDrawable()
                            .setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_IN);
                    progressBar.setVisibility(View.INVISIBLE);
                 }
              });
   }

   public void enableIniciarButton() {
      animateButtonWidth(true);
      fadeInTextAndHideProgress();
   }

   private int convertDpToPixel(int p_nDP) {
      DisplayMetrics loMetrics = Resources.getSystem().getDisplayMetrics();
      float lnPx = p_nDP * (loMetrics.densityDpi / 160f);
      return Math.round(lnPx);
   }

   @Override
   public void onClick(View view) {
      switch (view.getId()) {
         case R.id.btnRegistrar:
            disableIniciarButton();

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
               @Override
               public void run() {
                  enableIniciarButton();
               }
            }, 3000);
            break;
      }
   }
}
