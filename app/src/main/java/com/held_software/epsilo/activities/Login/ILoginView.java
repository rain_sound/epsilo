package com.held_software.epsilo.activities.Login;

public interface ILoginView {
    void onLoginSuccess();
    void onLoginFailed(String p_cError);
    void disableIniciarButton();
    void enableIniciarButton();
}
