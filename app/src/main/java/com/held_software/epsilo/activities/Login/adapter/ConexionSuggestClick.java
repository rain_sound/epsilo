package com.held_software.epsilo.activities.Login.adapter;

import com.held_software.epsilo.entities.Conexiones;

/**
 * Created by jose_ on 3/12/2019.
 */

public interface ConexionSuggestClick {
   void onClickSuggest(Conexiones conexion);
   void onLongCkickSuggest(Conexiones conexion);
}
