package com.held_software.epsilo.activities.Main;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.held_software.epsilo.EpsApplication;
import com.held_software.epsilo.R;
import com.held_software.epsilo.activities.Login.LoginActivity;
import com.held_software.epsilo.activities.Medidor.MedidorActvity;
import com.held_software.epsilo.activities.Perfil.PerfilActivity;
import com.held_software.epsilo.fragments.consumos.ConsumosFragment;
import com.held_software.epsilo.activities.Incidencias.IncidenciasActivity;
import com.held_software.epsilo.fragments.pagar.PagarFragment;
import com.held_software.epsilo.fragments.recibo.ReciboFragment;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener, NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {
    private static final String TAG = "MAINACTIVITY";
    private BottomNavigationView bottomNavigationView;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupToolbar();
        setupBottomNavigation();
        bind();
    }

    private void setupBottomNavigation() {
        bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        bottomNavigationView.inflateMenu(R.menu.bottom_menu);
        bottomNavigationView.setSelectedItemId(R.id.btnRecibo);
    }

    private void bind() {
        navigationView.getHeaderView(0).findViewById(R.id.imgProfile).setOnClickListener(this);
    }

    private void setupToolbar() {
        Toolbar loToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(loToolbar);

        DrawerLayout loDrawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle loToggle = new ActionBarDrawerToggle(
                this,
                loDrawer,
                loToolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close
        );
        loDrawer.addDrawerListener(loToggle);
        loToggle.syncState();
        loDrawer.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                final CircleImageView imgProfile = navigationView.getHeaderView(0).findViewById(R.id.imgProfile);
                final ImageView imgLogo = navigationView.getHeaderView(0).findViewById(R.id.imgLogo);

                // Animation
                final Animation fade_in = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
                imgProfile.setAnimation(fade_in);

                Animation fade_out = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out);
                imgLogo.setAnimation(fade_out);
                fade_out.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) { }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        imgLogo.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) { }
                });
                imgProfile.setVisibility(View.VISIBLE);
                fade_in.start();
                fade_out.start();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                CircleImageView imgProfile = navigationView.getHeaderView(0).findViewById(R.id.imgProfile);
                ImageView imgLogo = navigationView.getHeaderView(0).findViewById(R.id.imgLogo);
                imgProfile.setVisibility(View.GONE);
                imgLogo.setVisibility(View.VISIBLE);
                super.onDrawerClosed(drawerView);
            }
        });

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //NavigationView loRightNavView = findViewById(R.id.nav_view);
        setTitle(EpsApplication.getNombre());
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int lnId = item.getItemId();

        if (lnId == R.id.btnSignout) {
            EpsApplication.clear();
            Intent loginIntent = new Intent(this, LoginActivity.class);
            loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(loginIntent);
        } else if (lnId == R.id.btnRecibo) {
            FragmentManager loFragmentManager = getSupportFragmentManager();
            FragmentTransaction loTransaction = loFragmentManager.beginTransaction();
            loTransaction.replace(R.id.actionView, new ReciboFragment()).commit();
        } else if (lnId == R.id.btnMisConsumos) {
            FragmentManager loFragmentManager = getSupportFragmentManager();
            FragmentTransaction loTransaction = loFragmentManager.beginTransaction();
            loTransaction.replace(R.id.actionView, new ConsumosFragment()).commit();
        } else if (lnId == R.id.btnPagar) {
            FragmentManager loFragmentManager = getSupportFragmentManager();
            FragmentTransaction loTransaction = loFragmentManager.beginTransaction();
            loTransaction.replace(R.id.actionView, new PagarFragment()).commit();
        } else if (lnId  == R.id.btnIncidencias) {
            Intent intent = new Intent(this, IncidenciasActivity.class);
            startActivity(intent);
        } else if (lnId == R.id.btnMedir) {
            Intent intent = new Intent(this, MedidorActvity.class);
            startActivity(intent);
        }

        DrawerLayout loDraweLayout = findViewById(R.id.drawer_layout);
        loDraweLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.imgProfile) {
            Intent intent = new Intent(this, PerfilActivity.class);
            startActivity(intent);
        }
    }
}
