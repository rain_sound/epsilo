package com.held_software.epsilo.activities.Incidencias;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.held_software.epsilo.R;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class IncidenciasActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
   private static final String TAG = "INCIDENCIASACTIVITY";
   private static final int REQUEST_IMAGE_CAPTURE = 0;
   private static final int REQUEST_GPS_LOCATION = 1;

   private View main_view;
   private TextInputEditText txtUbicacion;
   private TextInputEditText txtObservacion;
   private Button btnAttach;
   private ImageView imgThumb;
   private CheckBox checkGPS;

   private LocationManager locationManager;
   private MyLocation myLocationListener = new MyLocation();

   @Override
   protected void onActivityResult(int requestCode, int resultCode, Intent data) {
      super.onActivityResult(requestCode, resultCode, data);
      if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
         btnAttach.setVisibility(View.GONE);
         imgThumb.setOnClickListener(this);

         Bundle extras = data.getExtras();
         Bitmap imageBitmap = (Bitmap) extras.get("data");
         imgThumb.setImageBitmap(imageBitmap);
         imgThumb.invalidate();
         imgThumb.requestLayout();
         imgThumb.forceLayout();
      }
   }

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_incidencias);
      setTitle("Reportar de incidencias");
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      mxBind();
      mxVerifyCamera();
   }

   private void mxVerifyCamera() {
      if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)) {
         btnAttach.setEnabled(false);
      }
   }

   private void mxBind() {
      txtUbicacion = findViewById(R.id.txtUbicacion);
      txtObservacion = findViewById(R.id.txtObservacion);
      btnAttach = findViewById(R.id.btnAttach);
      imgThumb = findViewById(R.id.imgThumb);
      checkGPS = findViewById(R.id.checkGPS);
      main_view = findViewById(R.id.main_view);

      btnAttach.setOnClickListener(this);
      checkGPS.setOnCheckedChangeListener(this);
      findViewById(R.id.btnEnviar).setOnClickListener(this);
   }

   @Override
   public void onClick(View view) {
      if (view.getId() == R.id.btnAttach || view.getId() == R.id.imgThumb) {
         imgThumb.setOnClickListener(null);
         Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
         if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
         }
      } else if (view.getId() == R.id.btnEnviar) {

      }
   }

   @Override
   protected void onPause() {
      if (locationManager != null) {
         locationManager.removeUpdates(myLocationListener);
      }
      super.onPause();
   }

   @Override
   public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
      if (buttonView.getId() == R.id.checkGPS) {
         if (isChecked) {
            if (locationManager == null) {
               locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            }
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
               Snackbar.make(main_view, "ACTIVE SU GPS", Snackbar.LENGTH_LONG).show();
               buttonView.setChecked(false);
               return;
            }
            txtUbicacion.setEnabled(false);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
               if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                  requestPermissions(new String[] {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_GPS_LOCATION);
               }
               buttonView.setChecked(false);
               return;
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10, myLocationListener);
         } else {
            txtUbicacion.setEnabled(true);
            locationManager.removeUpdates(myLocationListener);
         }
      }
   }

   private class MyLocation implements LocationListener {

      @Override
      public void onLocationChanged(Location location) {
         Geocoder geocoder = new Geocoder(getBaseContext(), Locale.getDefault());
         try {
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            if (addresses.size() > 0) {
               Address current = addresses.get(0);

               String subThoroughfare = current.getSubThoroughfare();
               if (subThoroughfare == null) {
                  subThoroughfare = "";
               }

               txtUbicacion.setText(String.format("%s %s", current.getThoroughfare(), subThoroughfare));
            }
         } catch (IOException e) {
            e.printStackTrace();
         }
      }

      @Override
      public void onStatusChanged(String provider, int status, Bundle extras) {

      }

      @Override
      public void onProviderEnabled(String provider) {

      }

      @Override
      public void onProviderDisabled(String provider) {

      }
   }
}