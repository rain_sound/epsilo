package com.held_software.epsilo.entities;

public class Asociado {
   private String dni;

   public String getDni() {
      return dni;
   }

   public void setDni(String dni) {
      this.dni = dni;
   }
}
