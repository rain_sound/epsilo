package com.held_software.epsilo.entities;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by jose_ on 3/12/2019.
 */

public class Consumo {
    private String periodo;
    private String categoria;
    private String emision;
    private String vencimiento;
    private String consumo;
    private String monto;

    public Consumo() {
    }

    public Consumo(JSONObject object){
        try {
            periodo = object.getString("periodo");
            categoria = object.getString("categoria");
            emision = object.getString("emision");
            vencimiento = object.getString("vencimiento");
            consumo = object.getString("consumo");
            monto = object.getString("monto");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getEmision() {
        return emision;
    }

    public void setEmision(String emision) {
        this.emision = emision;
    }

    public String getVencimiento() {
        return vencimiento;
    }

    public void setVencimiento(String vencimiento) {
        this.vencimiento = vencimiento;
    }

    public String getConsumo() {
        return consumo;
    }

    public void setConsumo(String consumo) {
        this.consumo = consumo;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }
}
