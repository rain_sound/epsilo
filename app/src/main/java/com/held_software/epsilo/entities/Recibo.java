package com.held_software.epsilo.entities;

import java.util.List;

public class Recibo {
   private String nombres;
   private String direccion;
   private String conexion;
   private String catastro;
   private List<Bill> bills;

   public String getNombres() {
      return nombres == null ? "" : nombres;
   }

   public void setNombres(String nombres) {
      this.nombres = nombres;
   }

   public String getDireccion() {
      return direccion == "null" ? "" : direccion;
   }

   public void setDireccion(String direccion) {
      this.direccion = direccion;
   }

   public String getConexion() {
      return conexion == null ? "" : conexion;
   }

   public void setConexion(String conexion) {
      this.conexion = conexion;
   }

   public String getCatastro() {
      return catastro == "null" ? "" : catastro;
   }

   public void setCatastro(String catastro) {
      this.catastro = catastro;
   }

   public List<Bill> getBills() {
      return bills;
   }

   public void setBills(List<Bill> bills) {
      this.bills = bills;
   }
}
