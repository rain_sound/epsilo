package com.held_software.epsilo.entities.Clients;

import android.util.Log;

public class ClientReponse {
    private static final String TAG = "CLIENTRESPONSE";

    private Client client;

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        Log.i(TAG,"entro aaqui al set client");
        this.client = client;
    }

    @Override
    public String toString(){
        Log.i(TAG, "entro aqui");
        return client.getId();
    }

}
