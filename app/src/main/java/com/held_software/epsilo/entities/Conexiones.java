package com.held_software.epsilo.entities;

/**
 * Created by jose_ on 3/12/2019.
 */

public class Conexiones {
    private String conexion;
    private String nombreCliente;

    public Conexiones(){

    }

    public Conexiones(String conexion, String nombreCliente) {
        this.conexion = conexion;
        this.nombreCliente = nombreCliente;
    }

    public String getConexion() {

        return conexion;
    }

    public void setConexion(String conexion) {
        this.conexion = conexion;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }
}