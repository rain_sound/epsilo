package com.held_software.epsilo.entities.Clients;

import android.util.Log;

public class Client {
    private static final String TAG = "CLIENT MODEL";
    private String name;
    private String id;
    private String email;
    private String token;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Client(String name, String id, String email, String token) {
        Log.i(TAG, "CONSTRUCTOR CLIENTE");
        this.name = name;
        this.id = id;
        this.email = email;
        this.token = token;
    }


    @Override
    public String toString() {
        return "cliente {" +
                "name='" + this.name + '\'' +
                ", email='" + this.email + '\'' +
                ", token=" + this.token +
                ", id=" + id +
                '}';
    }

}
