package com.held_software.epsilo.entities;

public class Bill {
   private String reading;
   private String fechaExpiracion;

   public String getReading() {
      return reading;
   }

   public void setReading(String reading) {
      this.reading = reading;
   }

   public String getFechaExpiracion() {
      return fechaExpiracion;
   }

   public void setFechaExpiracion(String fechaExpiracion) {
      this.fechaExpiracion = fechaExpiracion;
   }
}
