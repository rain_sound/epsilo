package com.held_software.epsilo.marquee;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;

import com.held_software.epsilo.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jose_ on 3/12/2019.
 */

public class MarqueeAdapter extends PagerAdapter {
    private static final String TAG = "MARQUEEADAPTER";
    private List<String> viewList = new ArrayList<>();
    public static int STYLE_EPSITO = 0;
    public static int STYLE_MARQUEE = 1;
    private int style = 1;


    @Override
    public int getCount() {
        return viewList.size();
    }
    public MarqueeAdapter(int style) {
        this.style = style;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(((View)object));
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View loView = LayoutInflater.from(container.getContext()).inflate(R.layout.marquee_item, null);
        TextView textView = loView.findViewById(R.id.txtMarquee);
        if(STYLE_MARQUEE == style){

        }
        if(style == STYLE_EPSITO){
            textView.setTextColor(Color.BLACK);
            textView.setTextSize(13.0f);
        }
        textView.setText(this.viewList.get(position));
        container.addView(loView);
        return loView;
    }

    public void addItem(String item, LayoutInflater inflater) {
        this.viewList.add(item);
        notifyDataSetChanged();
        /*FrameLayout tmp = (FrameLayout) inflater.inflate(R.layout.marquee_item, null, false);
        TextView textView = tmp.findViewById(R.id.txtMarquee);
        if(STYLE_MARQUEE == style){

        }
        if(style == STYLE_EPSITO){
            textView.setTextColor(Color.BLACK);
            textView.setTextSize(13.0f);
        }
        textView.setText(item);
        Log.e(TAG, "addItem" + tmp.toString());
        this.addView(tmp);*/
    }
}
