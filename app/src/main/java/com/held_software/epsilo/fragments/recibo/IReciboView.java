package com.held_software.epsilo.fragments.recibo;

import com.held_software.epsilo.entities.Recibo;

public interface IReciboView {
    void onReciboSuccess(Recibo p_oRecibo);
    void onError(String p_cError);
    void showProgress();
    void hideProgress();
    void showIntentarDeNuevo();
    void hideIntentarDeNuevo();
}
