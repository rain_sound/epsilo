package com.held_software.epsilo.fragments.consumos;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.held_software.epsilo.R;
import com.held_software.epsilo.entities.Consumo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jose_ on 3/12/2019.
 */

public class ConsumosAdapter extends RecyclerView.Adapter<ConsumosAdapter.ViewHolder> {
    private List<Consumo> consumoList = new ArrayList<>();

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.consumos_item, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Consumo current = consumoList.get(position);
        holder.txtPeriodo.setText(current.getPeriodo());
        holder.txtCategoria.setText(current.getCategoria());
        holder.txtEmision.setText(current.getEmision());
        holder.txtVencimiento.setText(current.getVencimiento());
        holder.txtConsumo.setText(current.getConsumo());
        holder.txtMonto.setText(current.getMonto());
    }

    @Override
    public int getItemCount() {
        return consumoList.size();
    }

    public void updateConsumosList(List<Consumo> consumosList){
        this.consumoList = consumosList;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtPeriodo;
        private TextView txtCategoria;
        private TextView txtEmision;
        private TextView txtVencimiento;
        private TextView txtConsumo;
        private TextView txtMonto;
        public ViewHolder(View itemView) {
            super(itemView);
            bind();
        }

        private void bind() {
            txtPeriodo = itemView.findViewById(R.id.txtPeriodo);
            txtCategoria = itemView.findViewById(R.id.txtCategoria);
            txtEmision = itemView.findViewById(R.id.txtEmision);
            txtVencimiento = itemView.findViewById(R.id.txtVencimiento);
            txtConsumo = itemView.findViewById(R.id.txtConsumo);
            txtMonto = itemView.findViewById(R.id.txtMonto);
        }
    }
}
