package com.held_software.epsilo.fragments.pagar;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.held_software.epsilo.R;

/**
 * Created by jose_ on 3/12/2019.
 */

public class PagarFragment extends Fragment {
    private Button btnPagar;
    private View mainView;
    private TextView txtPagoTotal;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.pagar_fragment, container, false);
        //bind();
        return mainView;
    }

    private void bind() {
        /*btnPagar = mainView.findViewById(R.id.btnPagar);
        txtPagoTotal = mainView.findViewById(R.id.txtPagoTotal);
        btnPagar.setOnClickListener(this);*/
    }

    @Override
    public void onDestroy() {
        /*if(bp != null){
            bp.release();
        }*/
        super.onDestroy();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         /*String apiKey = getResources().getString(R.string.apiKey);
        bp = new BillingProcessor(getContext(), apiKey, this);
        RestClient client = RestClient.getInstance();
        String conexion = SedaparApplication.getConexion();
        String url = "https://www.sedapar.com.pe/oficina-virtual-recibos/?connection=" + conexion;
        client.execute(url, 0, this);*/
    }

    public PagarFragment() {
    }

    /*@Override
    public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {

    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, @Nullable Throwable error) {

    }

    @Override
    public void onBillingInitialized() {

    }

    @Override
    public void onClick(View view) {
        bp.purchase(getActivity(), "android.test.purchased");
    }

    @Override
    public void onSuccess(int code, String response) {
        try {
            JSONObject res = new JSONObject(response);
            JSONObject details = res.getJSONObject("details");
            txtPagoTotal.setText(details.getString("Saldo"));
            btnPagar.setEnabled(true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onError(int code, String error) {
        mostrarMensaje("NO TIENE CONEXION A INTERNET");
    }

    private void mostrarMensaje(String mensaje){
        Snackbar.make(mainView, mensaje, Snackbar.LENGTH_SHORT).show();
    }*/
}
