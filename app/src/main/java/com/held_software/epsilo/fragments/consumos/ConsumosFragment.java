package com.held_software.epsilo.fragments.consumos;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.snackbar.Snackbar;
import com.held_software.epsilo.EpsApplication;
import com.held_software.epsilo.R;
import com.held_software.epsilo.clases.CUsuario;
import com.held_software.epsilo.entities.Consumo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jose_ on 3/12/2019.
 */

public class ConsumosFragment extends Fragment implements IConsumosFragment{
   private View mainView;
   private RecyclerView recyclerviewConsumos;
   private SwipeRefreshLayout swipteRefreshLayoutConsumos;
   private ConsumosAdapter adapter;

   @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
      mainView = inflater.inflate(R.layout.consumos_fragment, container, false);
      bind();
      setupAdapter();
      //loadData();
      return mainView;
   }

   private void setupAdapter() {
      recyclerviewConsumos.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
      recyclerviewConsumos.setAdapter(adapter);
   }

   @Override
   public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      adapter = new ConsumosAdapter();
      loadData();
   }

   private void loadData() {
      if(swipteRefreshLayoutConsumos != null){
         swipteRefreshLayoutConsumos.setRefreshing(true);
      }

      Map<String, Object> laData = new HashMap<>();
      laData.put("TOKEN", EpsApplication.getToken());

      CUsuario lo = new CUsuario();
      lo.iViewActions = this;
      lo.paData = laData;
      boolean llOk = lo.omRecuperarConsumos();
      if (!llOk) {
         mostrarMensaje(lo.pcError);
      }
   }

   private void bind() {
      recyclerviewConsumos = mainView.findViewById(R.id.recyclerviewConsumos);
      swipteRefreshLayoutConsumos = mainView.findViewById(R.id.swipteRefreshLayoutConsumos);
      swipteRefreshLayoutConsumos.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
         @Override
         public void onRefresh() {
            loadData();
         }
      });
   }

   private void mostrarMensaje(String mensaje){
      if(mainView != null){
         Snackbar.make(mainView, mensaje, Snackbar.LENGTH_SHORT).show();
      }
   }

   @Override
   public void onDestroyView() {
      super.onDestroyView();
      mainView = null;
   }

   @Override
   public void onConsumosLoaded(List<Consumo> p_aConsumos) {
      adapter.updateConsumosList(p_aConsumos);
      swipteRefreshLayoutConsumos.setRefreshing(false);
   }

   @Override
   public void showError(String p_cError) {
      swipteRefreshLayoutConsumos.setRefreshing(false);
      mostrarMensaje(p_cError);
   }
}
