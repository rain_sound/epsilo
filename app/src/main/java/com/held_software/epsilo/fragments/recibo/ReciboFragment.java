package com.held_software.epsilo.fragments.recibo;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.icu.text.AlphabeticIndex;
import android.os.Bundle;
import android.text.TextPaint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.google.android.material.snackbar.Snackbar;
import com.held_software.epsilo.EpsApplication;
import com.held_software.epsilo.R;
import com.held_software.epsilo.clases.CRecibo;
import com.held_software.epsilo.entities.Bill;
import com.held_software.epsilo.entities.Recibo;
import com.held_software.epsilo.util.TouchImageView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.zip.CheckedOutputStream;

public class ReciboFragment extends Fragment implements IReciboView, View.OnLongClickListener {
   private static final String TAG = "RECIBOFRAGMENT";
   private View mainView;
   private TouchImageView imgRecibo;
   private ProgressBar progressBar;
   private Button btnIntentar;
   private Bitmap bitmapRecibo;
   private TextView txtConsumos;
   private TextView txtMonto;
   private RelativeLayout invoceStatus;
   private TextView txtVencido;
   private TextView txtVencimiento;
   private TextView txtNroRecibo;
   private TextView txtNombre;
   private TextView txtDireccion;
   private ImageView imgConsumos;

   @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
      mainView = inflater.inflate(R.layout.recibo_fragment, container, false);
      mxBind();
      mxLoadBitmapBase();
      mxLoad();
      return mainView;
   }

   private void mxLoadBitmapBase() {
      Bitmap bitmapFondo = BitmapFactory.decodeResource(getResources(), R.drawable.recibo_background);
      float scaleWidth = 1813.0f / (float) bitmapFondo.getWidth();
      float scaleHeight = 2539.0f / (float) bitmapFondo.getHeight();
      Matrix matrix = new Matrix();
      matrix.postScale(scaleWidth, scaleHeight);
      bitmapRecibo = Bitmap.createBitmap(bitmapFondo, 0, 0, bitmapFondo.getWidth(), bitmapFondo.getHeight(), matrix, false);
      imgRecibo.setImageBitmap(bitmapRecibo);
   }

   private void mxLoad() {
      showProgress();
      CRecibo lo = new CRecibo();
      lo.iViewActions = this;
      Map<String, Object> laData = new HashMap<>();
      laData.put("TOKEN", EpsApplication.getToken());
      laData.put("CONEXION", EpsApplication.getConexion());
      lo.paData = laData;
      boolean llOk = lo.omRecuperarRecibo();
      if (!llOk) {
         onError(lo.pcError);
      }
   }

   private void mxBind() {
      imgRecibo = mainView.findViewById(R.id.imgRecibo);
      progressBar = mainView.findViewById(R.id.progressbar);
      btnIntentar = mainView.findViewById(R.id.btnIntentar);
      txtConsumos = mainView.findViewById(R.id.txtConsumos);
      txtMonto = mainView.findViewById(R.id.txtMonto);
      invoceStatus = mainView.findViewById(R.id.invoceStatus);
      txtVencido = mainView.findViewById(R.id.txtVencido);
      txtVencimiento = mainView.findViewById(R.id.txtVencimiento);
      txtNroRecibo = mainView.findViewById(R.id.txtNroRecibo);
      txtNombre = mainView.findViewById(R.id.txtNombre);
      txtDireccion = mainView.findViewById(R.id.txtDireccion);
      imgConsumos = mainView.findViewById(R.id.imgConsumos);
      imgRecibo.setOnLongClickListener(this);
   }

   @Override
   public void onReciboSuccess(Recibo p_oRecibo) {
      hideProgress();
      hideIntentarDeNuevo();

      if (p_oRecibo.getBills().size() == 0) {
         txtConsumos.setVisibility(View.VISIBLE);
         return;
      } else {
         txtConsumos.setVisibility(View.GONE);
      }

      // GET REAL DATA
      Random random = new Random();

      int lnMonto = random.nextInt(70) + 20;
      txtMonto.setText("S/." + lnMonto);

      // Vencido
      if (random.nextInt(100) > 50) {
         invoceStatus.setBackground(getResources().getDrawable(R.drawable.recibo_vencido));
         txtVencido.setVisibility(View.VISIBLE);
      } else {
         invoceStatus.setBackground(getResources().getDrawable(R.drawable.recibo_normal));
         txtVencido.setVisibility(View.GONE);
      }

      txtNroRecibo.setText("#002" + random.nextInt(1000) + 576);

      Bill billActual = p_oRecibo.getBills().get(0);
      txtVencimiento.setText("Vencimiento: " + billActual.getFechaExpiracion());

      txtNombre.setText(EpsApplication.getNombre());
      txtDireccion.setText(p_oRecibo.getDireccion());
      TextPaint paintBold = new TextPaint();
      paintBold.setTextSize(45f);
      paintBold.setTypeface(Typeface.create("Arial", Typeface.BOLD));

      Paint paintNormal = new Paint();
      //paintNormal.setTextSize(35f);
      paintNormal.setTextSize(45f);
      paintNormal.setStyle(Paint.Style.STROKE);
      paintNormal.setStrokeWidth(4f);
      paintNormal.setColor(Color.parseColor("#aa1122"));

      Paint paintFill = new Paint();
      paintFill.setStyle(Paint.Style.FILL);
      paintFill.setColor(Color.parseColor("#ffddff"));

      // START
      int lnBlocks = (p_oRecibo.getBills().size() > 5 ? 5 : p_oRecibo.getBills().size());

      int maxBill = 0;                          // se puede recuperar de la consulta
      for (Bill bill : p_oRecibo.getBills()) {
         int lnTmp = Integer.parseInt(bill.getReading());
         if (lnTmp > maxBill) {
            maxBill = lnTmp;
         }
      }

      final int X_WIDTH = 164;
      final int X_OFFSET = 30;
      int lnX = X_OFFSET;

      Bitmap bitmapConsumos = Bitmap.createBitmap(lnBlocks * X_OFFSET + lnBlocks * X_WIDTH + X_OFFSET, 600, Bitmap.Config.ARGB_8888);
      Canvas canvasCosumos = new Canvas(bitmapConsumos);
      //canvasCosumos.drawColor(Color.GREEN);

      List<Bill> bills = p_oRecibo.getBills();
      Collections.reverse(bills);
      Iterator<Bill> iterator = bills.iterator();
      int lnCount = 0;
      while (iterator.hasNext() && lnCount < 5) {
         Bill currentBill = iterator.next();
         ++lnCount;
         int lnRead = Integer.parseInt(currentBill.getReading());
         int lnHeight = 600 * lnRead / maxBill;

         canvasCosumos.drawRect(lnX, 600 - lnHeight + 4, lnX + X_WIDTH, 596, paintNormal);
         canvasCosumos.drawRect(lnX, 600 - lnHeight + 4, lnX + X_WIDTH, 596, paintFill);

         canvasCosumos.save();
         canvasCosumos.rotate(-45, lnX + 35, 550);
         canvasCosumos.drawText(currentBill.getFechaExpiracion().substring(0, 7), lnX + 35, 550, paintNormal);
         canvasCosumos.restore();

         lnX += X_WIDTH + X_OFFSET;
      }

      imgConsumos.setImageBitmap(bitmapConsumos);
      // END

      Canvas canvas = new Canvas(bitmapRecibo);
      canvas.drawText(p_oRecibo.getConexion(), 791, 155, paintNormal);
      canvas.drawText(p_oRecibo.getNombres(), 30, 350, paintNormal);
      canvas.drawText("DIRECCIÓN: " + p_oRecibo.getDireccion(), 30, 400, paintNormal);
      canvas.drawText("CATASTRO: " + p_oRecibo.getCatastro(), 30, 450, paintNormal);

      // DATOS DE FACTURACION
      canvas.drawText("Servicios prestados: Agua y Desague", 30, 850, paintNormal);
      canvas.drawText("Categoria: 1", 30, 900, paintNormal);
      canvas.drawText("Actividad: VIVIENDA", 30, 950, paintNormal);
      //Bill billActual = p_oRecibo.getBills().get(0);
      canvas.drawText( String.format("Lectura actual: %s Fecha: %s", billActual.getReading(), billActual.getFechaExpiracion()), 30, 1000, paintBold);
      if (p_oRecibo.getBills().size() >= 2) {
         Bill billAnterior = p_oRecibo.getBills().get(1);
         canvas.drawText( String.format("Lectura anterior: %s Fecha: %s", billAnterior.getReading(), billAnterior.getFechaExpiracion()), 30, 1050, paintNormal);
      }

      imgRecibo.setImageBitmap(bitmapRecibo);
      //imgRecibo.setVisibility(View.VISIBLE);
   }

   @Override
   public void onError(String p_cError) {
      hideProgress();
      showIntentarDeNuevo();
      Snackbar.make(getActivity().findViewById(R.id.main_view), p_cError, Snackbar.LENGTH_SHORT).show();
   }

   @Override
   public void showProgress() {
      progressBar.setVisibility(View.VISIBLE);
   }

   @Override
   public void hideProgress() {
      progressBar.setVisibility(View.GONE);
   }

   @Override
   public void showIntentarDeNuevo() {
      btnIntentar.setVisibility(View.VISIBLE);
   }

   @Override
   public void hideIntentarDeNuevo() {
      btnIntentar.setVisibility(View.GONE);
   }

   @Override
   public boolean onLongClick(View view) {
      new AlertDialog.Builder(getContext()).setTitle("RECIBO").setPositiveButton("Descargar", new DialogInterface.OnClickListener() {
         @Override
         public void onClick(DialogInterface dialogInterface, int i) {

         }
      }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
         @Override
         public void onClick(DialogInterface dialogInterface, int i) {

         }
      }).create();
      return false;
   }
}
