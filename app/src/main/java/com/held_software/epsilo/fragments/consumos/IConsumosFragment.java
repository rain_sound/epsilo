package com.held_software.epsilo.fragments.consumos;

import com.held_software.epsilo.entities.Consumo;

import java.util.List;

public interface IConsumosFragment {
   void onConsumosLoaded(List<Consumo> p_aConsumos);
   void showError(String p_cError);
}
