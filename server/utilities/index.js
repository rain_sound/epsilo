const multer = require("multer");
const mime = require('mime');

const globalConfig = require("../config");

//IMPORTANT:
//Configuracion inicial (FOLDER DESTINO Y NOMBRE + EXT)

const extension = (file) => {
  return mime.getExtension(file.mimetype)
}

const conf = multer.diskStorage({
    destination: globalConfig.publicFolder,
    filename: function(req, file, callback) {
      callback(null, "image-" + Date.now() + "." + extension(file));
    }
  });

  const init = multer({
    storage: conf,
    limits: { fileSize: globalConfig.maxSizeUpload },
    fileFilter: function (req, file, callback) {
      var ext = extension(file)
      if(ext !== 'jpg' && ext !== 'gif' && ext !== 'jpeg') {
          return callback(new Error('Formato no permitido'))
      }
      callback(null, true)
  },
  });
  
  const upload = init.single("image");


  // Send Json
  //IMPORTANT:
  // ALERTS ALLWAYS WILL BE TRIGERED
  // DEFAULT ERROR ALWAYS WILL BE FALSE
  // END IMPORTANT:
  const sendJson = (res, message = {}, error = true, alert = true, data = {}) => {
    return res.status(200).json({ message: message, alert: alert, error: error, data: data });
  };


  module.exports = {
    upload,
    sendJson
  }