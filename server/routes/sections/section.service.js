const { upload, sendJson } = require("../../utilities");
const { Section } = require("../../db");
const config = require("../../config");

  
const getAll = (req, res, next) => {
    Section.findAll({ limit: 10 }).then(data => sendJson(res, "", false, false, data ))
}

const getById = (req, res, next) => {
    Section.findOne({
        where: {
            id: req.params.id
        }
    }).then( data => sendJson(res, "", false, false, data ))    
}

const newSection = (req, res, next) => {
    //IMPORTANT:
    //Verificar que envie el campo
    //IMPORTANT:

    if(!req.body.description) return sendJson(res, "No se enviaron datos")
    Section.create({
        s_description: req.body.description
    }).then(data => sendJson(res, "Se creo con éxito", false))
    .catch(err => sendJson(res, "Error al crear", true, true, err))
}

const updateSection = (req, res, next) => {
    if(!req.params.id) return sendJson(res, "No se enviaron datos")
    Section.findOne({
        where: {
            id: req.params.id
        }
    }).then(section => {
        if(section){
            section.update({
                s_description: req.body.description
            }).then(data => sendJson(res, "Se actualizó con éxito",false))
            .catch(err => sendJson(res, "Error en la actualización"))
        }
    })
    .catch(err => sendJson(res, "Error en la actualización"))
}

const deleteSection = (req, res, next) => {

}


module.exports = {
    getAll, 
    getById,
    newSection,
    updateSection,
    deleteSection
}