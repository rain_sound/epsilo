const router = require("express").Router();

const sectionService = require("./section.service");
const { verifyToken } = require("../middleware");

router.get("/",           sectionService.getAll);
router.get("/:id",         sectionService.getById);

router.post("/new",        sectionService.newSection);
router.put("/:id/update",  sectionService.updateSection);
router.delete("/:id/delete", sectionService.deleteSection);

module.exports = router;

