var router = require('express').Router()
    //Definir rutas
const clients           = require('./clients')
const employees         = require('./employees')
const categories        = require('./categories')
const services          = require('./services')
const sections          = require('./sections')
const models            = require('./modelsMeter')


//Controladores
router.use('/employees',           employees)
router.use('/clients',             clients)
router.use('/categories',          categories)
router.use('/services',            services)
router.use('/sections',            sections)
router.use('/models',              models)

module.exports = router