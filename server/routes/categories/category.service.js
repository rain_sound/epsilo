const { upload, sendJson } = require("../../utilities");
const { Category } = require("../../db");
const config = require("../../config");

  
const getAll = (req, res, next) => {
    Category.findAll({ limit: 10 }).then(data => sendJson(res, "", false, false, data ))
}

const getById = (req, res, next) => {
    Category.findOne({
        where: {
            id: req.params.id
        }
    }).then( data => sendJson(res, "", false, false, data ))    
}

const newCategory = (req, res, next) => {
    //IMPORTANT:
    //Verificar que envie el campo
    //IMPORTANT:

    if(!req.body.name) return sendJson(res, "No se enviaron datos")
    Category.create({
        name: req.body.name
    }).then(data => sendJson(res, "Se creo con éxito", false))
    .catch(err => sendJson(res, "Error al crear"))
}

const updateCategory = (req, res, next) => {
    if(!req.params.id) return sendJson(res, "No se enviaron datos")
    Category.findOne({
        where: {
            id: req.params.id
        }
    }).then(category => {
        if(category){
            category.update({
                name: req.body.name
            }).then(data => sendJson(res, "Se actualizó con éxito",false))
            .catch(err => sendJson(res, "Error en la actualización"))
        }
    })
    .catch(err => sendJson(res, "Error en la actualización"))
}

const deleteCategory = (req, res, next) => {

}


module.exports = {
    getAll, 
    getById,
    newCategory,
    updateCategory,
    deleteCategory
}