const router = require("express").Router();

const categoryService = require("./category.service");
const { verifyToken } = require("../middleware");

router.get("/",           categoryService.getAll);
router.get("/:id",         categoryService.getById);

router.post("/new",        categoryService.newCategory);
router.put("/:id/update",  categoryService.updateCategory);
router.delete("/:id/delete", categoryService.deleteCategory);

module.exports = router;

