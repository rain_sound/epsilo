const multer = require("multer");
const router = require("express").Router();

const employeeService = require("./employee.service");
const { verifyToken } = require("../middleware");

router.get("/resetpassword", employeeService.resetPassword);
router.post("/forgotpassword", employeeService.forgotPassword);
router.put("/updatepassword", employeeService.updatePassword);
router.post("/uploadImage", employeeService.uploadImage);

router.get("/", employeeService.getAll);
router.get("/:id", verifyToken, employeeService.getById);
router.post("/signin", employeeService.signin);
router.post("/signup", employeeService.signup);
router.post("/resetAdmin", employeeService.resetAdmin);

module.exports = router;
