
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const nodemailer = require("nodemailer");
const multer = require("multer");

const { upload, sendJson } = require("../../utilities");
const { Employee } = require("../../db");
const config = require("../../config");

//TODO:
// 1) Verificar token
// 2) Verificar mensaje de errores upload image
// END TODO:

const getAll = (req, res, next) => {
  Employee.findAll({
    attributes: ['id', 'e_name','e_lastname','e_email', 'e_imgprofile', 'e_address', 'e_cellphone', 'e_daybirth']
  }).then(employees => sendJson(res, {}, false, false, employees));
};

const getById = (req, res, next) => {
  Employee.findAll({
    attributes: ['id', 'e_name','e_lastname','e_email', 'e_imgprofile', 'e_address', 'e_cellphone', 'e_daybirth'],
    where: {
      id: req.params.id
    }
  }).then(employees => sendJson(res, {}, false, false, employees));
  //res.send(`El usuario consultado es ${req.params.id}`);
};

const signup = (req, res, next) => {
  Employee.create({
    e_name: req.body.username,
    e_email: req.body.email,
    e_lastname: req.body.lastname,
    e_address: req.body.address,
    e_daybirth: req.body.daybirth,
    e_imgprofile: 'default.png',
    e_password: bcrypt.hashSync('epsilos', 8)
  })
    .then(employee => {
      return sendJson(res, "Se registro con exito", false);
    })
    .catch(err => {
      return sendJson(res,  err.original.detail);
    });
  
};


const signin = (req, res, next) => {
  Employee.findOne({
    where: {
      "e_name": req.body.username
    }
  })
    .then(employee => {
      //Si no existe el usuario
      if (!employee) {
        return sendJson(res, "No existe usuario" );
      }
      var passwordIsValid = bcrypt.compareSync(
        req.body.password,
        employee.e_password
      );

      //Si existe error en la contrasenia
      if (!passwordIsValid) {
        return sendJson(res, "Contrasenia invalida");
      }

      //Se genera el token con fecha de expiracion
      var token = jwt.sign({ id: employee.id }, config.secret, {
        expiresIn: "1d"
      });

      res.status(200).json({
        auth: true,
        email: employee.e_email,
        name: employee.e_name,
        accessToken: token,
        id: employee.id,
      });
    })
    .catch(err => {
      return sendJson(res, err);
    });
  
};

//verifica que el link sea validado y envia el name para la actualizacion
const resetPassword = (req, res, next) => {
  Employee.findOne({
    where: {
      passwordResetToken: req.query.token,
      passwordResetTokenExpire: {
        $gt: Date.now()
      }
    }
  })
    .then(employee => {
      if (employee) return res.status(200).json({ name: employee.e_name, status: "ok" });
      return sendJson(res, "El link es invalido o el token expiro");
    })
    .catch(err => sendJson(res, "Error en la consulta", true, true, err));
};

//envia la url con el token
const forgotPassword = (req, res, next) => {
  Employee.findOne({
    where: {
      "e_email": req.body.email
    }
  })
    .then(employee => {
      if (employee) {
        //Por verse
        var token = bcrypt.hashSync(req.body.email, 8);

        employee.update({
          passwordResetToken: token,
          passwordResetTokenExpire: Date.now() + 36000000
        });
//al reves
        send_email(token, employee, res);
      } else {
        return sendJson(res, "No existe cuenta asociada");
      }
    })
    .catch(err => sendJson(res, "Error en la consulta", true, true, err));

};

const updateData = (req, res, next) => {
  if(req.body.id){
    Employee.findOne({
      where: {
        "id": req.body.id
      }
    })
    .then(employee => {
      employee.update({
        e_name:     req.body.e_name,
        e_lastname: req.body.e_lastname,
        e_email:    req.body.e_email
      })
      .then(() => sendJson(res, 'Se actualizó con éxito', false, true, employee))
      .catch(err => sendJson(res, 'No se pudo actualizar', true, true, err))
    })
    .catch(err => sendJson(res, 'Usuario no existe'))
  }
  else sendJson(res, 'No tienes permitido usar esta ruta', true, true)
}

const resetAdmin = (req, res, next) => {
  Employee.findOne({
    where: {
      "e_email": req.body.email
    }
  })
    .then(employee => {
      if (employee) {
        employee.update({
          e_password: bcrypt.hashSync('epsilos', 8)
        }).then(() => sendJson(res, 'Se actualizó con éxito', false, true, employee));
      }
    })
    .catch(err => { sendJson(res, "Error al intentar actualizar la contraseña", true, true, err) });
}

//Tengo que enviarle el name recuperado de la anterior ruta
const updatePassword = (req, res, next) => {
  Employee.findOne({
    where: {
      "e_name": req.body.username
    }
  })
    .then(employee => {
      const { oldPassword } = req.body;
      // IMPORTANT:
      // Si existe el campo oldPassword se utiliza para actualizar desde profile
      if (oldPassword) {
        const matchPassword = bcrypt.compareSync(oldPassword, employee.e_password);

        if (!matchPassword) {
          return sendJson(res, "Contraseña no coincide");
        }
      }
      // Existen dos flujos, cuando se actualiza desde recuperacion o desde profile
      if (employee) {
        employee
          .update({
            e_password: bcrypt.hashSync(req.body.password, 8),
            passwordResetToken: null,
            passwordResetTokenExpire: null
          })
          .then(() => sendJson(res, "Contraseña fue actualizada", false))
          .catch(err => { sendJson(res, "Error al intentar actualizar la contraseña", true, true, err) })
      } else {
        return sendJson(res, "No existe usuario");
      }
    })
    .catch(err => sendJson(res, 'Error al consultar usuario', true, true, err));

 
};

function uploadImage(req, res, next) {
    upload(req, res, err => {
      if (err) {
        if (err instanceof multer.MulterError || err.code === 'LIMIT_FILE_SIZE') {
          return sendJson(res, "Archivo muy grande");
        } else {
          return sendJson(res, "Formato no permitido");
        }
      } else {
        Employee.findOne({
          where: {
            id: req.body.id
          }
        })
          .then(employee => {
            if (employee) {
              employee.update({
                imgProfile: req.file.filename
              });
            } else sendJson(res, "El usuario no existe");
          })
          .catch(err => sendJson(res, "Error en la consulta", true, true, err));
      }
      return sendJson(res, "Se actualizó la imagen con éxito", false, true, { filename: req.file.filename })
    });
  };
  

const send_email = (token, user, res) => {
  const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: config.usermail,
      pass: config.passwordmail
    }
  });

  const mailOptions = {
    from: config.usermail,
    to: user.e_email,
    subject: "Enlace para resetear contrasenia",
    text:
      "Ingresa al siguiente enlace para poder resetear tu contraseña\n\n" +
      `http://localhost:3000/clients/resetpassword/?token=${token}`
  };

  transporter.sendMail(mailOptions, function(err, response) {
    if (err){
      console.log('error', err);
      return sendJson(res, "No se pudo enviar el correo");
    }
    return sendJson(res, "Correo de recuperacion enviado ", false);
  });
};

module.exports = {
  getAll,
  getById,
  signin,
  signup,
  resetPassword,
  forgotPassword,
  updatePassword,
  updateData,
  uploadImage,
  resetAdmin
};
