const router = require("express").Router();

const servicesService = require("./services.service");
const { verifyToken } = require("../middleware");


//Servicios encapsula medidores
router.get("/", servicesService.getAll);

router.post("/new/meter", servicesService.registerMeter);
router.post("/new/bill", servicesService.registerBill);

module.exports = router;
