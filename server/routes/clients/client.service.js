
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const nodemailer = require("nodemailer");

const { sendJson } = require("../../utilities");
const { Client, Category, Meter, Bill } = require("../../db");
const config = require("../../config");

//TODO:
// 1) Verificar token
// 2) Verificar mensaje de errores upload image
// END TODO:

const getBill = (req, res, next) => {
  
  Client.findAll({
    attributes: ['c_name', 'c_lastname', 'c_dni'],
    include: [
    {
      attributes: ['c_conexion','m_route','m_catastro_code','m_address'],
      model: Meter,
      required: true,
      include: [{
        model: Category,
        attributes: ['ca_description','ca_activity', 'id'],
        required: true
      }]
    }],
    where:{
      "c_dni": req.params.id
    }

  })
  .then(data =>  {
      console.log('data',data)
    const client = data[0];
    const meters = data[0].meters;
    return  sendJson(res,"",false,false, {
      name: client.c_name,
      lastname: client.c_lastname,
      id: client.id,
      meters: meters
    })
  } 
  )
  .catch(err =>  {
    console.log('error', err);
    return  sendJson(res, "El cliente no posee una dirección registrada", true, true, err);
  })
}

const getAll = (req, res, next) => {
  Client.findAll({
    attributes: ['c_dni', 'c_name','c_lastname','c_email']
  }).then(users => sendJson(res, {}, false, false, users));
};

const getById = (req, res, next) => {
    
  const id = req.params.id;
  if(!id){
    sendJson(res, "Debe de proporcionar un id")
  }
  else {
    Client.findOne({
      where: {
        "c_dni": id
      }
    })
    .then(user => {
      return sendJson(res, "Se encontró el cliente", false, true, {
        daybirth: user.c_daybirth,
        email: user.c_email,
        name: user.c_name,
        lastname: user.c_lastname,
        cellphone: user.c_cellphone,
        address: user.c_address
      })
    })
    .catch(err => sendJson(res, "No se pudo encontrar el cliente", true, true, err))
  }
}

const signup = (req, res, next) => {
  Client.create({
    c_name: req.body.username,
    c_cellphone: req.body.cellphone,
    c_email: req.body.email,
    c_lastname: req.body.lastname,
    c_daybirth: req.body.daybirth,
    c_password: bcrypt.hashSync(req.body.id, 8),
    c_dni: req.body.id
  })
    .then(user => {
      return sendJson(res, "Se registro con exito", false);
    })
    .catch(err => {
      return sendJson(res,  "El correo o el código ya existen", true, true, err.original.detail);
    });
  
};

const signin = (req, res, next) => {
  Client.findOne({
    where: {
      "c_dni": req.body.DNI
    },
    include: [{
      model: Meter,
      where: {
        "c_conexion": req.body.CONEXION
      }
    }]
  })
  .then(user => {
    //Si no existe el usuario
    if (!user) {
      return sendJson(res, "No existe usuario" );
    }
    var passwordIsValid = bcrypt.compareSync(
      req.body.CLAVE,
      user.c_password
    );

    //Si existe error en la contrasenia
    if (!passwordIsValid) {
      return sendJson(res, "Contrasenia invalida");
    }

    //Se genera el token con fecha de expiracion
    var token = jwt.sign({ id: user.c_dni }, config.secret, {
      expiresIn: "1d"
    });
    Meter.findAll({
      attributes: ['c_conexion'],
      raw: true,
      where: {
        "c_dni": req.body.DNI
      }
    })
    .then(meters => {
      var laMeters = [];
      meters.forEach(element => {
        laMeters.push(element['c_conexion']);
      });
      return sendJson(res, "", false, false, {
        auth: true,
        email: user.c_email,
        name: user.c_name,
        accessToken: token,
        id: user.id,
        meters: laMeters
      });
    })
    .catch(err => {
      return sendJson(res, "Error al conexiones del cliente", true, true, err);
    });
  })
  .catch(err => {
    return sendJson(res, "Error al buscar el cliente", true, true, err);
  });
};

const signinMobile = (req, res, next) => {
    
  Client.findOne({
    where: {
      "c_dni": req.body.DNI
    },
    include: [{
      model: Meter,
      where: {
        "c_conexion": req.body.CONEXION
      }
    }]
  })
  .then(user => {
    //Si no existe el usuario
    if (!user) {
      return sendJson(res, "No existe usuario o conexión registrada" );
    }
    var passwordIsValid = bcrypt.compareSync(
      req.body.CLAVE,
      user.c_password
    );

    //Si existe error en la contrasenia
    if (!passwordIsValid) {
      return sendJson(res, "Contrasenia invalida");
    }

    //Se genera el token con fecha de expiracion
    var token = jwt.sign({ id: user.c_dni }, config.secret, {
      expiresIn: "30d"
    });
    Meter.findAll({
      attributes: ['c_conexion'],
      raw: true,
      where: {
        "clientCDni": req.body.DNI
      }
    })
    .then(meters => {
      var laMeters = [];
      meters.forEach(element => {
        laMeters.push(element['c_conexion']);
      });
      return sendJson(res, "", false, false, {
        auth: true,
        email: user.c_email,
        name: user.c_name,
        accessToken: token,
        id: user.id,
        meters: laMeters
      });
    })
    .catch(err => {
      return sendJson(res, "Error al conexiones del cliente", true, true, err);
    });
  })
  .catch(err => {
    return sendJson(res, "Error al buscar el cliente", true, true, err);
  });
};

//verifica que el link sea validado y envia el name para la actualizacion
const resetPassword = (req, res, next) => {
  Client.findOne({
    where: {
      passwordResetToken: req.query.token,
      passwordResetTokenExpire: {
        $gt: Date.now()
      }
    }
  })
    .then(user => {
      if (user) return res.status(200).json({ name: user.c_name, status: "ok" });
      return sendJson(res, "El link es invalido o el token expiro");
    })
    .catch(err => sendJson(res, err));
};

//envia la url con el token
const forgotPassword = (req, res, next) => {
  Client.findOne({
    where: {
      "c_email": req.body.email
    }
  })
    .then(user => {
      if (user) {
        //Por verse
        var token = bcrypt.hashSync(req.body.email, 8);

        user.update({
          passwordResetToken: token,
          passwordResetTokenExpire: Date.now() + 36000000
        });
//al reves
        send_email(token, user, res);
      } else {
        return sendJson(res, "No existe cuenta asociada");
      }
    })
    .catch(err => sendJson(res, err));

};

const updateData = (req, res, next) => {
  if(req.body.id){
    Client.findOne({
      where: {
        "id": req.body.id
      }
    })
    .then(user => {
      user.update({
        c_name:     req.body.c_name,
        c_lastname: req.body.c_lastname,
        c_email:    req.body.c_email
      })
      .then(() => sendJson(res, 'Se actualizó con éxito', false, true, user))
      .catch(err => sendJson(res, 'No se pudo actualizar', true, true, err))
    })
    .catch(err => sendJson(res, 'Usuario no existe'))
  }
  else sendJson(res, 'No tienes permitido usar esta ruta', true, true)
}

const resetAdmin = (req, res, next) => {
  Client.findOne({
    where: {
      "c_dni": req.body.dni
    }
  })
  .then(client => {
    if (client) {
      client.update({
        c_password: bcrypt.hashSync('epsilos', 8)
      }).then(() => sendJson(res, 'Se actualizó con éxito', false, true, client));
    }
  })
  .catch(err => { sendJson(res, "Error al intentar actualizar la contraseña", true, true, err) });
}

//Tengo que enviarle el name recuperado de la anterior ruta
const updatePassword = (req, res, next) => {
  Client.findOne({
    where: {
      "c_name": req.body.username
    }
  })
    .then(user => {
      const { oldPassword } = req.body;
      // IMPORTANT:
      // Si existe el campo oldPassword se utiliza para actualizar desde profile
      if (oldPassword) {
        const matchPassword = bcrypt.compareSync(oldPassword, user.password);

        if (!matchPassword) {
          return sendJson(res, "Contraseña no coincide");
        }
      }
      // Existen dos flujos, cuando se actualiza desde recuperacion o desde profile
      if (user) {
        user
          .update({
            c_password: bcrypt.hashSync(req.body.password, 8),
            passwordResetToken: null,
            passwordResetTokenExpire: null
          })
          .then(() => sendJson(res, "Contraseña fue actualizada", false))
          .catch(err => sendJson(res, err));
      } else {
        return sendJson(res, "No existe usuario");
      }
    })
    .catch(err => sendJson(res, err));

 
};

function uploadImage(req, res, next) {
  upload(req, res, err => {
    if (err) {
      if (err instanceof multer.MulterError || err.code === 'LIMIT_FILE_SIZE') {
        return sendJson(res, "Archivo muy grande");
      } else {
        return sendJson(res, "Formato no permitido");
      }
    } else {
      User.findOne({
        where: {
          id: req.body.id
        }
      })
        .then(user => {
          if (user) {
            user.update({
              imgProfile: req.file.filename
            });
          } else sendJson(res, "El usuario no existe");
        })
        .catch(err => sendJson(res, err));
    }
    return res
      .status(200)
      .json({
        message: "Se actualizo la imagen con exito",
        filename: req.file.filename,
        error: false,
        alert: true
      });
  });
}

const send_email = (token, user, res) => {
  const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: config.usermail,
      pass: config.passwordmail
    }
  });

  const mailOptions = {
    from: config.usermail,
    to: user.c_email,
    subject: "Enlace para resetear contrasenia",
    text:
      "Ingresa al siguiente enlace para poder resetear tu contraseña\n\n" +
      `http://localhost:3000/clients/resetpassword/?token=${token}`
  };

  transporter.sendMail(mailOptions, function(err, response) {
    if (err){
      console.log('error', err);
      return sendJson(res, "No se pudo enviar el correo");
    }
    return sendJson(res, "Correo de recuperacion enviado ", false);
  });
};

const getInvoice = (req, res, next) => {
  
  Client.findAll({
    attributes: ['c_name', 'c_lastname'],    
    where: {
      "c_dni": req.userId
    },
    include: [{
      attributes: ['c_conexion', 'm_catastro_code', 'm_address'],
      model: Meter,
      require: true,
      where: {
        "c_conexion": req.body.CONEXION
      },
      include: [{
        attributes: ['r_reading', 'r_date_expi', 'meterCConexion'],
        model: Bill,
        require: true,
        limit: 5,
        order: [['r_date_expi', 'DESC']]
      }]
    }]
  })
  .then(data => {
      
      if(data.length === 0){
          
          
          return sendJson(res, 'No existe el usuario', false, false, data); 
      }
      else {
          return sendJson(res, {}, false, false, data);      
      }
 
  })
  .catch(err => {
    console.warn(err);
    return  sendJson(res, "No se pudo encontrar información del recibo: " + req.userId + "_" + req.body.CONEXION, true, true, err);
  });
}

const getConsumos = (req, res, next) => {
  
  
  Bill.findAll({
    attributes: ['r_reading', 'r_date_expi'],
    include: [{
      attributes: ['c_conexion'],
      model: Meter,
      require: true,
      include: [{
       
        model: Client,
        require: true,
        where: {
          c_dni: req.userId
        }
      }]
    }]
  })
  .then(data => {
    return sendJson(res, {}, false, false, data);
  })
  .catch(err => {
      
    return  sendJson(res, "Consumos del cliente no encontrados", true, true, err);
  });
}

module.exports = {
  getAll,
  getBill,
  getById,
  signin,
  signinMobile,
  signup,
  resetPassword,
  forgotPassword,
  updatePassword,
  updateData,
  uploadImage,
  resetAdmin,
  getInvoice,
  getConsumos
  
};
/*
INSERT INTO clients (c_dni, c_name, c_lastname, c_email, c_password, "createdAt", "updatedAt")
VALUES ('71119458', 'JOSE ALONZO', 'HELD BUENO', 'jose.alozo.held97@gmail.com', crypt('epsilos', gen_salt('bf')), NOW(), NOW());
INSERT INTO meters (c_conexion, "createdAt", "updatedAt", "clientCDni")
VALUES ('000001', NOW(), NOW(), '71119458');
*/