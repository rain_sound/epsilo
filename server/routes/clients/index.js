const router = require("express").Router();

const clientService = require("./client.service");
const { verifyToken } = require("../middleware");

router.get("/resetpassword", clientService.resetPassword);
router.post("/forgotpassword", clientService.forgotPassword);
router.put("/updatepassword", clientService.updatePassword);
router.put("/updatedata", clientService.updateData);

router.get("/", clientService.getAll);
router.get("/:id/bill", clientService.getBill);
router.get("/:id", clientService.getById);
router.post("/signin", clientService.signin);
router.post("/signinMobile", clientService.signinMobile);
router.post("/signup", clientService.signup);
router.post("/resetAdmin", clientService.resetAdmin);
router.post("/getConsumos",  verifyToken, clientService.getConsumos);
router.post("/getInvoice", verifyToken, clientService.getInvoice);

module.exports = router;
