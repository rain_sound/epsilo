const { upload, sendJson } = require("../../utilities");
const { ModelMeter } = require("../../db");
const config = require("../../config");

  
const getAll = (req, res, next) => {
    ModelMeter.findAll({ limit: 10 }).then(data => sendJson(res, "", false, false, data ))
}

const getById = (req, res, next) => {
    ModelMeter.findOne({
        where: {
            id: req.params.id
        }
    }).then( data => sendJson(res, "", false, false, data ))    
}

const newModelMeter = (req, res, next) => {
    //IMPORTANT:
    //Verificar que envie el campo
    //IMPORTANT:

    if(!req.body.description) return sendJson(res, "No se enviaron datos")
    ModelMeter.create({
        m_description: req.body.description
    }).then(data => sendJson(res, "Se creo con éxito", false))
    .catch(err => sendJson(res, "Error al crear", true, true, err));
}

const updateModelMeter = (req, res, next) => {
    if(!req.params.id) return sendJson(res, "No se enviaron datos")
    ModelMeter.findOne({
        where: {
            id: req.params.id
        }
    }).then(model => {
        if(model){
            model.update({
                m_description: req.body.m_description
            }).then(data => sendJson(res, "Se actualizó con éxito",false))
            .catch(err => sendJson(res, "Error en la actualización"))
        }
    })
    .catch(err => sendJson(res, "Error en la actualización"))
}

const deleteModelMeter = (req, res, next) => {

}


module.exports = {
    getAll, 
    getById,
    newModelMeter,
    updateModelMeter,
    deleteModelMeter
}