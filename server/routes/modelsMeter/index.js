const router = require("express").Router();

const modelMService = require("./modelM.service");
const { verifyToken } = require("../middleware");

router.get("/",           modelMService.getAll);
router.get("/:id",         modelMService.getById);

router.post("/new",        modelMService.newModelMeter);
router.put("/:id/update",  modelMService.updateModelMeter);
router.delete("/:id/delete", modelMService.deleteModelMeter);

module.exports = router;

