const jwt = require('jsonwebtoken')
const config = require('../../config')

//TODO:
// 1) Actualizacion para Form data
// END TODO:

const verifyToken = (req, res, next) => {
    //Recupero token
    
    let token = req.headers['access-token']
    //Si no existe o no es enviado
    if(!token) {
        return res.status(403).json({ auth: false, reason: "Token no encontrado"})
    }
    //Verificar si el token es valido
    jwt.verify(token, config.secret, (err, decoded) => {
        if(err) {
            return res.status(500).json({ auth: false, reason: err })
        }
        req.userId = decoded.id 
        
        next()
    })
}

const toLowerCase = (req, res, next ) => {
    if(req.body){
        
    }
    if(req.params){

    }
    if(req.query){

    }
}

module.exports = {
    verifyToken
}