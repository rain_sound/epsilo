const Sequelize = require('sequelize')
const BillModel             = require('./models/bill.model')
const BillServModel         = require('./models/bill_servi.model')
const ClientModel           = require('./models/client.model')
const EmployeeModel         = require('./models/employee.model')
const CategoryModel         = require('./models/category.model')
const MeterModel            = require('./models/meter.model')
const ServiceModel          = require('./models/services.model')
const ModelMeterModel       = require('./models/modelMeter.model')
const SectionModel          = require('./models/section.model')

const cf = require('../config')

//Configuracion inicial
const conn = new Sequelize(cf.db_name, cf.user_db, cf.password_db, {
    host: cf.host,
    dialect: cf.dialect,
    pool: {
        max: cf.max_conn,
        min: cf.min_conn
    }
})

//Iniciar modelos | ORM 
const Bill              = BillModel(conn, Sequelize)
const BillServ          = BillServModel(conn, Sequelize)
const Category          = CategoryModel(conn, Sequelize)
const Client            = ClientModel(conn, Sequelize)
const Meter             = MeterModel(conn, Sequelize)
const Service           = ServiceModel(conn, Sequelize)
const Employee          = EmployeeModel(conn, Sequelize)
const Section           = SectionModel(conn, Sequelize)
const ModelMeter        = ModelMeterModel(conn, Sequelize)

ModelMeter.hasMany(Meter)
Meter.belongsTo(ModelMeter)

Employee.belongsTo(Section)
Section.hasMany(Employee)

Meter.belongsTo(Client, { foreignKey: 'c_dni'} )
Client.hasMany(Meter, { foreignKey: 'c_dni'} )

Meter.hasMany(Bill)
Bill.belongsTo(Meter)

Category.hasMany(Meter)
Meter.belongsTo(Category)

Bill.belongsToMany(Service, {
    through: BillServ,
    foreignKey: 'bill_id'
})

Service.belongsToMany(Bill, {
    through: BillServ,
    foreignKey: 'service_id'
})


conn.sync({ force: false })
    .then(() => {
        console.log('Tablas y database creados')
    })
    .catch(err => {
        console.log(err);
    })

module.exports = {
    Bill,
    BillServ,
    Client,
    Category,
    Meter,
    Service,
    Employee,
    Section,
    ModelMeter,
    conn
}