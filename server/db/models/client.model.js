module.exports = (sequelize, type) => {
    return sequelize.define('client', {
        c_dni: {
            type: type.STRING(8),
            primaryKey: true,
        },
        c_name: {
            type: type.STRING(80),
        },
        c_lastname: {
            type: type.STRING(80)
        },
        c_email: {
            type: type.STRING(150),
            unique: true
        },
        c_daybirth: {
            type: type.DATEONLY
        },
        c_cellphone: {
            type: type.STRING(80)
        },
        c_password: type.STRING(255),
        passwordResetToken: type.STRING(60),
        passwordResetTokenExpire: type.DATE,

    })
}