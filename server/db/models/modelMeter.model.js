module.exports = (sequelize, type) => {
    return sequelize.define('model_meter', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        m_description: {
            type: type.STRING,
        }
    })
}