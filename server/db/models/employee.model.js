module.exports = (sequelize, type) => {
    return sequelize.define('employee', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        e_name: {
            type: type.STRING(80),
        },
        e_lastname: {
            type: type.STRING(80)
        },
        e_email: {
            type: type.STRING(150),
            unique:true
        },
        e_daybirth: {
            type: type.DATEONLY
        },
        e_cellphone: {
            type: type.STRING(9)
        },
        e_imgprofile: {
            type: type.STRING(255)
        },
        e_address: {
            type: type.STRING(255)
        },
        e_password: type.STRING(255),
        passwordResetToken: type.STRING(60),
        passwordResetTokenExpire: type.DATE,
    })
}