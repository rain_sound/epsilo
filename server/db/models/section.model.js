module.exports = (sequelize, type) => {
    return sequelize.define('section', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        s_description: {
            type: type.STRING,
        }
    })
}