
module.exports = (sequelize, type) => {
    return sequelize.define('meter', {
        c_conexion: {
            type: type.STRING(6),
            primaryKey: true
        },
        m_catastro_code: {
            type: type.STRING(80)
        },
        m_serie: {
            type: type.STRING(30),
            unique: true,
        },
        m_route: {
            type: type.STRING(150)
        },
        m_address:{
            type: type.STRING(255)
        },
        
    })
}