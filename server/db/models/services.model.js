module.exports = (sequelize, type) => {
    return sequelize.define('services', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        s_description: {
            type: type.STRING(255),
        },
        s_shortdesc: {
            type: type.STRING(80)
        },
        s_pricebase: {
            type: type.FLOAT
        }
    })
}