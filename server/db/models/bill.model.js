module.exports = (sequelize, type) => {
    return sequelize.define('bill', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        r_reading: {
            type: type.INTEGER,
        },
        r_date_expi: {
            type: type.DATEONLY
        },
        r_date_issue: {
            type: type.DATEONLY
        },
        r_type: {
            type: type.STRING(50),
        }
    })
}