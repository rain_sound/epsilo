module.exports = (sequelize, type) => {
    return sequelize.define('categories', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        ca_description: {
            type: type.STRING(80),
        },
        ca_activity: {
            type: type.STRING(80)
        },
        ca_cargo: {
            type: type.FLOAT
        }
    })
}