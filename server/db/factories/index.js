const bcrypt = require("bcryptjs")
const faker = require("faker");
const {
  Bill,
  BillServ,
  Client,
  Category,
  Meter,
  Service,
  Employee,
  ModelMeter
} = require("../../db");
for (y = 0; y < 10; y++) {
  var name = faker.name.firstName();
  Employee.create({
    e_name: name,
    e_password: bcrypt.hashSync('ss', 8), 
    e_lastname: faker.name.firstName(),
    e_email: faker.internet.email(),
    e_daybirth: faker.date.past(),
    e_imgprofile: 'http://lorempixel.com/400/200/',
  })

}

for(i = 0; i < 10; i++) {
  ModelMeter.create({
    m_description: faker.lorem.word()
  })
}

for(i = 0; i < 10; i++) {
  Category.create({
    ca_description: faker.lorem.word(),
    ca_activity: faker.lorem.word(),
    ca_cargo: faker.random.float,
  })
}

for(i = 0; i < 10; i++) {
  Service.create({
    s_description: faker.lorem.word(),
    s_shortdesc: faker.lorem.word(),
    s_pricebase: faker.random.float,
  })
}

