const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const router = require("./routes");
const config = require("./config");
const https = require('https');
const app = express();
const fs = require('fs');
//Middleware
app.use("/public", express.static(__dirname + "/public/uploads/images"));

app.use(bodyParser.urlencoded());
app.use(bodyParser.json());

app.use(cors());
app.use(router);

//Server
/*
app.listen(config.port, () => {
  console.log(`Server running on localhost:${config.port}`);
});

app.listen(35000, 'heldsoftware.com', () => { console.log('ingre')}).on('error', function(err) { console.log('error', err); });
*/
/*https.createServer({
    key: fs.readFileSync('./ssl/keyssl.key'),
    cert: fs.readFileSync('./ssl/cert.crt')
}, app)*/
app.listen(35000);
module.exports = app;
