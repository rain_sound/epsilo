# Estructura del proyecto
```server
        ─ config                    Configuraciones del proyecto (sequelize, config)
        ├── db                      Archivos relacionados a la db
        │   ├── migrations          Migraciones de la db
        │   └── models              Clases     
        ├── routes                  Carpeta contenedora de las rutas del api
        │   └── users               Carpeta con las rutas relacionadas a un modelo
        ├── services                Por ver (...)
        └── utilities               Alertas, archivos en comun
```
En el directorio del proyecto ejecutar para instalar las dependencias
### `npm install`

Para iniciar el servidor utilizar el comando
### `node app.js`

Documentos a revisar (dependencias)

* [Sequelize](http://docs.sequelizejs.com/) - ORM y migraciones
* [Express](http://expressjs.com/es/api.html) - Manejo de rutas - API REST

##Importante
Se debe de modificar el archivo de moficiacion (config/index.js)
```module.exports = {
    port: 3000,                     Puerto del servidor 
    db_name:        "innicia",      Nombre de la base de datos
    user_db:        "postgres",     Usuario 
    password_db:    "postgres",     Contrasenia
    host:           "localhost",    Host
    dialect:        "postgres",     Lenguaje
    max_conn:       5,              Maximo de conexiones en pool
    min_conn:       0               Minimo de conexiones en pool
}```